# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Unspsc',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('counter', models.IntegerField(null=True, blank=True)),
            ],
        ),
    ]
