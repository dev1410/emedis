from django.contrib import admin

from settings import ADMIN_SITE_HEADER, ADMIN_SITE_TITLE
from unspsc.family.admin import FamilyAdmin
from unspsc.family.classed.admin import ClassedAdmin
from unspsc.family.classed.models import Classed
from unspsc.family.models import Family

admin.site.site_header = ADMIN_SITE_HEADER
admin.site.site_title = ADMIN_SITE_TITLE

admin.site.register(Family, FamilyAdmin)
admin.site.register(Classed, ClassedAdmin)