from django.db import models


class Unspsc(models.Model):
    id = models.AutoField(primary_key=True)
    counter = models.IntegerField(
        null=True,
        blank=True
    )

