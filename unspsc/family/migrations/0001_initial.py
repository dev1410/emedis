# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import unspsc.family.models
from django.conf import settings
import unspsc.family.classed.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Classed',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(help_text=b'UNSPSC Class Name', unique=True, max_length=50)),
                ('class_code', models.IntegerField(help_text=b'UNSPSC Class Code', unique=True, validators=[unspsc.family.classed.models.validate_even])),
                ('is_active', models.BooleanField(default=False, help_text=b'UNSPSC Class Active Status')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='unspsc_class_created_user', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Classes',
            },
        ),
        migrations.CreateModel(
            name='Family',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(help_text=b'UNSPSC Family Name', unique=True, max_length=50)),
                ('family_code', models.IntegerField(help_text=b'UNSPSC Family Code', unique=True, validators=[unspsc.family.models.validate_even])),
                ('is_active', models.BooleanField(default=False, help_text=b'UNSPSC Family Active Status')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='unspsc_family_created_user', to=settings.AUTH_USER_MODEL)),
                ('updated_user', models.ForeignKey(related_name='unspsc_family_updated_user', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Families',
            },
        ),
        migrations.AddField(
            model_name='classed',
            name='family',
            field=models.ForeignKey(to='family.Family'),
        ),
        migrations.AddField(
            model_name='classed',
            name='updated_user',
            field=models.ForeignKey(related_name='unspsc_class_updated_user', to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
