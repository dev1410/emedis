from django.db import models
from django.contrib.auth.models import User
from django.db.models import DateTimeField, ForeignKey, BooleanField
from django.core.exceptions import ValidationError

from unspsc.family.models import Family


def validate_even(value):
    if value < 0 and value > 99:
        raise ValidationError(u'%s range 0 to 99' % value)


class Classed(models.Model):
    class Meta:
        verbose_name_plural = "Classes"

    id = models.AutoField(primary_key=True)
    family = models.ForeignKey(
        Family
    )
    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        unique=True,
        help_text='UNSPSC Class Name'
    )
    class_code = models.IntegerField(
        validators=[
            validate_even
        ],
        null=False,
        blank=False,
        unique=True,
        help_text='UNSPSC Class Code'
    )
    is_active = BooleanField(
        default=False,
        help_text='UNSPSC Class Active Status')
    created_date = DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='unspsc_class_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, related_name='unspsc_class_updated_user', null=True)