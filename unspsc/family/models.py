from django.db import models
from django.contrib.auth.models import User
from django.db.models import DateTimeField, ForeignKey, BooleanField
from django.core.exceptions import ValidationError


def validate_even(value):
    if value < 0 and value > 99:
        raise ValidationError(u'%s range 0 to 99' % value)

class Family(models.Model):
    class Meta:
        verbose_name_plural = "Families"


    id = models.AutoField(primary_key=True)
    name = models.CharField(
        max_length=50,
        null=False,
        blank=False,
        unique=True,
        help_text='UNSPSC Family Name'
    )
    family_code = models.IntegerField(
        validators=[
            validate_even
        ],
        null=False,
        blank=False,
        unique=True,
        help_text='UNSPSC Family Code'
    )
    is_active = BooleanField(
        default=False,
        help_text='UNSPSC Family Active Status')
    created_date = DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='unspsc_family_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, related_name='unspsc_family_updated_user', null=True)

    def __unicode__(self):
        return self.name
