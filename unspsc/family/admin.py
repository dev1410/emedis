from django.contrib import admin

from product.library.image import AdminImageWidget
from .models import Family

class FamilyAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'family_code', 'is_active',
        'created_date', 'updated_date')

    list_filter = ('created_date', 'updated_date')

    search_fields = ['name']

    fieldsets = [
        ('General Information',
         {'fields':
          ['name', 'family_code', 'is_active']
          })
    ]

    def save_model(self, request, obj, form, change):
        if change:
            obj.updated_user = request.user
            obj.save()
        else :
            obj.created_user = request.user
            obj.save()
