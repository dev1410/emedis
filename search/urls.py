from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^/$', views.index, name='suggest'),
    url(r'^/advance/$', views.advance, name='advance'),
    url(r'^/products/$', views.products, name='products'),
    url(r'^/products/(?P<size>[0-9]+)$', views.products, name='products'),
    url(r'^/products/sort/(?P<field>[\w-]+)/(?P<order>[\w-]+)$', views.products, name='products'),
    url(r'^/products/sort/(?P<field>[\w-]+)/(?P<order>[\w-]+)/(?P<size>[0-9]+)$', views.products, name='products'),
    url(r'^/product/(?P<id>[0-9]+)/$', views.product, name='products'),
    url(r'^/categories/$', views.categories, name='categories'),
    url(r'^/category/(?P<id>[0-9]+)/$', views.category, name='category'),
    url(r'^/category_search/$', views.category_search, name='category_search'),
    url(r'^/subcategories/$', views.subcategories, name='subcategories'),
    url(r'^/subcategory/(?P<id>[0-9]+)/$', views.subcategory, name='subcategory'),
    url(r'^/subcategory_search/$', views.subcategory_search, name='subcategory_search'),
]
