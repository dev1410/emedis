from settings import ELASTIC_CONNECTION
from common.utils.autovivification import AutoViviDict


def suggest(keyword):

    es = ELASTIC_CONNECTION
    suggestion = AutoViviDict()
    suggestion['suggestions']['text'] = keyword
    suggestion['suggestions']['completion']['field'] = 'suggest'

    return es.suggest(
            body=suggestion, index="product",
            params=None)


def search(keyword, offset=None, limit=None):

    body = AutoViviDict()
    body['fields'] = ['product_name', 'product_number', 'specification', 'function']
    product_name_body = body['query']['common']['product_name']
    product_name_body['query'] = keyword
    product_name_body['cutoff_frequency'] = 0.001
    product_name_body['minimum_should_match']['low_freq'] = 2
    product_name_body['minimum_should_match']['high_freq'] = 3

    return ELASTIC_CONNECTION.search(
        index='product',
        doc_type='product',
        body=body,
        _source=True,
        fields=[
            'product_name'],
        suggest_field='suggest',
        suggest_mode='missing',
        suggest_size=10,
        suggest_text=keyword,
        from_=offset, size=limit)


def multi_search(keyword, offset=None, limit=None):
    if keyword is not None:
        body = AutoViviDict()
        body_query = body['query']['multi_match']
        body_query['query'] = keyword
        body_query['type'] = 'best_fields'
        body_query['fields'] = ["product_name", "brand", "product_number"]
        body_query['cutoff_frequency'] = 0.001
        body_query['tie_breaker'] = 0.3

        return ELASTIC_CONNECTION.search(
            index='product',
            doc_type='product',
            body=body,
            from_=offset, size=limit)


def advance(**kwargs):

    query = AutoViviDict()

    query_filtered = query['query']['filtered']
    bool_must_list = query_filtered['query']['bool']['must'] = []
    filtered_filter = query_filtered['filter']
    filtered_filter['and'] = []

    nested = AutoViviDict()
    nested_1 = AutoViviDict()
    filtered = AutoViviDict()

    if 'product_name' not in kwargs:
        kwargs['product_name'] = None

    if 'category_' not in kwargs:
        kwargs['category_'] = None

    if 'sub_category' not in kwargs:
        kwargs['sub_category'] = None

    if 'offset' not in kwargs:
        kwargs['offset'] = 0

    if 'brand_id' not in kwargs:
        kwargs['brand_id'] = None

    if kwargs['product_name'] is None:
        if kwargs['category_'] is not None and kwargs['sub_category'] is not None:

            nested['nested']['path'] = 'category'
            nested['nested']['query']['bool']['must']['match']['category.name'] = kwargs['category_']
            bool_must_list.append(nested)

            nested_1['nested']['path'] = 'subcategory'
            nested_1['nested']['query']['bool']['must']['match']['subcategory.name'] = kwargs['sub_category']
            bool_must_list.append(nested_1)

        elif kwargs['category_'] is not None and kwargs['sub_category'] is None:

            nested['nested']['path'] = 'category'
            nested['nested']['query']['bool']['must']['match']['category.name'] = kwargs['category_']
            bool_must_list.append(nested)

        elif kwargs['brand_id'] is not None:
            nested['nested']['path'] = 'brand'
            nested['nested']['query']['bool']['must']['match']['brand.id'] = kwargs['brand_id']
            bool_must_list.append(nested)

    else:

        if kwargs['category_'] is not None and kwargs['sub_category'] is not None:

            nested['match']['product_name'] = kwargs['product_name']
            nested['nested']['path'] = 'category'
            nested['nested']['query']['bool']['must']['match']['category.name'] = kwargs['category_']
            bool_must_list.append(nested)

            nested_1['nested']['path'] = 'subcategory'
            nested_1['nested']['query']['bool']['must']['match']['subcategory.name'] = kwargs['sub_category']
            bool_must_list.append(nested_1)

        elif kwargs['category_'] is not None and kwargs['sub_category'] is None:

            nested['match']['product_name'] = kwargs['product_name']
            nested['nested']['path'] = 'category'
            nested['nested']['query']['bool']['must']['match']['category.name'] = kwargs['category_']
            bool_must_list.append(nested)

        elif kwargs['category_'] is None and kwargs['sub_category'] is None:

            nested['match']['product_name'] = kwargs['product_name']
            bool_must_list.append(nested)

    filtered['term']['is_active'] = 'true'
    filtered_filter['and'].append(filtered)

    return ELASTIC_CONNECTION.search(
        index='product',
        doc_type='product',
        body=query,
        from_=kwargs['offset'],
        size=kwargs['limit'])


def subcategory(keyword=None, _id=None):

    if _id is not None:
        return ELASTIC_CONNECTION.get_source(
            index='subcategory',
            doc_type='subcategory',
            _source=True,
            id=_id)

    query = AutoViviDict()
    query_filtered = query['query']['filtered']

    query_filtered_list = query_filtered['query']['bool']['must'] = []
    query_filtered_filter_and = query_filtered['filter']['and'] = []

    match = AutoViviDict()
    term = AutoViviDict()

    if keyword is None:
        match['match']['is_active'] = True
        query_filtered_list.append(match)

        term['term']['is_active'] = True
        query_filtered_filter_and.append(term)
    else:
        match['match']['name'] = keyword
        query_filtered_list.append(match)

        term['term']['is_active'] = True
        query_filtered_filter_and.append(term)

    return ELASTIC_CONNECTION.search(
        index='subcategory',
        doc_type='subcategory',
        _source=True,
        body=query)


def products(offset=None, limit=None, **kwargs):
    query = AutoViviDict()

    query_filtered = query['query']['filtered']
    query_filtered_list = query_filtered['query']['bool']['must'] = []
    query_filtered_filter_and = query_filtered['filter']['and'] = []

    match = AutoViviDict()
    term = AutoViviDict()

    match['match']['is_active'] = True
    query_filtered_list.append(match)

    term['term']['is_active'] = True
    query_filtered_filter_and.append(term)

    if len(kwargs) > 0:
        fieldsets = {'date': 'created_date'}

        orders = {
            'desc': 'desc', 'descending': 'desc',
            'asc': 'asc', 'ascending': 'asc'
        }

        if kwargs['order'] \
                and kwargs['order'] != '' \
                and kwargs['order'] is not None:

            query['sort'][fieldsets[kwargs['field']]] = {"order": orders[kwargs['order']]}

    return ELASTIC_CONNECTION.search(
        index='product',
        doc_type='product',
        from_=offset,
        size=limit,
        _source=True,
        fields=['product_name'],
        request_cache=True,
        body=query)


def product(product_id):
    return ELASTIC_CONNECTION.get(
        index='product',
        doc_type='product',
        id=product_id,
        _source=True,
        refresh=True)


def categories(offset=None, limit=None):
    query = AutoViviDict()
    query_filtered = query['query']['filtered']
    query_filtered_list = query_filtered['query']['bool']['must'] = []
    query_filtered_and = query_filtered['filter']['and'] = []

    match = AutoViviDict()
    term = AutoViviDict()

    match['match']['is_active'] = True
    query_filtered_list.append(match)

    term['term']['is_active'] = True
    query_filtered_and.append(term)

    return ELASTIC_CONNECTION.search(
        index='category',
        doc_type='category',
        from_=offset,
        size=limit,
        _source=True,
        fields=['name'],
        request_cache=True,
        body=query)


def category(category_id):
    return ELASTIC_CONNECTION.get(
        index='category',
        doc_type='category',
        id=category_id,
        _source=True,
        refresh=True)


def category_search(keyword=None, _id=None):

    if _id is not None:
        return ELASTIC_CONNECTION.get_source(
            index='category',
            doc_type='category',
            _source=True,
            id=_id
        )

    query = AutoViviDict()
    match = AutoViviDict()
    term = AutoViviDict()

    query_filtered = query['query']['filtered']
    query_filtered_list = query_filtered['query']['bool']['must'] = []
    query_filtered_and = query_filtered['filter']['and'] = []

    if keyword is None:
        match['match']['is_active'] = True
        query_filtered_list.append(match)

    else:
        match['match']['name'] = keyword
        query_filtered_list.append(match)

    term['term']['is_active'] = True
    query_filtered_and.append(term)

    return ELASTIC_CONNECTION.search(
        index='category',
        doc_type='category',
        _source=True,
        body=query)


def subcategories(offset=None, limit=None):
    query = AutoViviDict()
    match = AutoViviDict()
    term = AutoViviDict()

    query_filtered = query['query']['filtered']
    query_filtered_list = query_filtered['query']['bool']['must'] = []
    query_filtered_and = query_filtered['filter']['and'] = []

    match['match']['is_active'] = True
    query_filtered_list.append(match)

    term['term']['is_active'] = True
    query_filtered_and.append(term)

    return ELASTIC_CONNECTION.search(
        index='subcategory',
        doc_type='subcategory',
        from_=offset,
        size=limit,
        _source=True,
        fields=['name'],
        request_cache=True,
        body=query)


def subcategory_by_id(subcategory_id):
    return ELASTIC_CONNECTION.get(
        index='subcategory',
        doc_type='subcategory',
        id=subcategory_id,
        _source=True,
        refresh=True)
