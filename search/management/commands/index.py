from django.core.management.base import BaseCommand, CommandError
from settings import ELASTIC_CONNECTION
from search.engine import Mapper, Create


class Command(BaseCommand):
    help = '>>> Search Engine Management Command <<<'

    def add_arguments(self, parser):

        parser.add_argument(
            '--name',
            dest='index_name',
            default=False,
            help='Indexing specified index name for searching index')

        parser.add_argument(
            '--exists',
            dest='index_exists',
            default=False,
            help='Checking specified index name on searching index')

        parser.add_argument(
            '--delete',
            dest='index_delete',
            default=False,
            help='Deleting specified index of searching index')

        parser.add_argument(
            '-api',
            dest='manual_product_index',
            default=False,
            help='Manually add product index by product id')

    def handle(self, *args, **options):

        if options['index_name']:
            from search.engine import Index

            if options['index_name'] != 'product' and options['index_name'] != 'category' \
                    and options['index_name'] != 'subcategory':
                raise CommandError(
                    "Unknown index %s, choices are ['product','category','subcategory']" % options['index_name'])

            Index(index=options['index_name'])

        if options['index_exists']:

            result = ELASTIC_CONNECTION.indices.exists(index=options['index_exists'])

            if result is True:
                print ">>> %s" % result
            else:
                print ">>> No index named '%s' " % options['index_exists']

        if options['index_delete']:

            Mapper(index=options['index_delete']).delete

        if options['manual_product_index']:
            Create(
                index='product',
                doc_type='product',
                doc_id=options['manual_product_index'])
