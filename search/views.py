from django.http import HttpResponse
import search
import json


def index(request):
    """
    This main method contain 3 search method, which is can't be combined
    at the same time because it will returned redundant data.

    Usage:
    1. Suggestion
        1.1 URL: http://domain.com/search/?q=keyword be here
    2. Search
        2.1 URL: http://domain.com/search/?qs=keyword be here
    3. Multi Field Search
        3.1 URL: http://domain.com/search/?qms=keyword be here

    Let the space out there because the engine take care of it,
        - the keyword can contain as follow:
            a. Product name,
            b. Product number,
            c. e-catalogue registration number
            d. brand name
            e. function

    :param request:
    :return JSON Data Format:
    """
    suggest_query = request.GET.get('q', '')
    search_query = request.GET.get('qs', '')
    msearch_query = request.GET.get('qms', '')
    p = request.GET.get('page', '')
    limit = request.GET.get('size', '')

    size = limit if limit != '' else 10
    page = 0

    if p != '' and int(p) > 1:
        for length in xrange(int(p)):
            page += size

    if suggest_query:

        clean_query = suggest_query.replace(' ', '+')

        suggestion = search.suggest(clean_query)

        results = []

        no_result = {
            "error": {
                "code": "404",
                "message": "No data found!",
                "keyword": clean_query
            }
        }

        for result in suggestion['suggestions']:

            for option in result['options']:

                data = {
                    'keyword': "%s" % result['text'].replace('+', ' '),
                    'length': result['length'],
                    'output': option['text'],
                    'score': option['score'],
                    'id': option['payload']['id'],
                    'slug': option['payload']['slug']
                }

                results.append(data)

        return HttpResponse(
            json.dumps(results) if len(results) > 0 else json.dumps(no_result),
            content_type='application/json')

    if search_query:
        obj = search.search(search_query, offset=page, limit=size)

        return HttpResponse(
            json.dumps(obj)if len(obj) > 0 else 'Nothing',
            content_type='application/json')

    if msearch_query:

        query = search.multi_search(msearch_query, offset=page, limit=size)

        return HttpResponse(json.dumps(query), content_type='application/json')

    else:
        message = {
            "error": {
                "message": "Query match doesn't matter, Give me some clue !",
                "code": "500"
            }
        }
        return HttpResponse(json.dumps(message), content_type='application/json')


def advance(request):
    """
    Search by advance only supported category and subcategory attribute, which is
    this method can be more advanced search filter for the future.

    Usage:
    The source from the given URL parameters like so :
    - http://domain.com/search/?q=product-name&c=category-name&s=subcategory-name

    - Limit and offset function is not necessary unless you need pagination,
    default elastic size is 10
        - http://domain.com/search/?page=page-number&size=total-data-returned
        :parameter page;
        :parameter size

    and the result returned as JSON data format
    :param request:
    :return JSON Data Format:
    """

    q = request.GET.get('q', '')
    c = request.GET.get('c', '')
    s = request.GET.get('s', '')
    p = request.GET.get('page', '')
    l = request.GET.get('size', '')

    limit = int(l) if l != '' else 10
    page = 0
    result = None

    if p != '' and int(p) > 1:
        for length in range(int(p)):
            page += limit

    if q:
        if c != '' and s != '':
            result = search.advance(
                product_name=q.lower(),
                category_=c.lower(),
                sub_category=s.lower(),
                offset=page, limit=limit)

        elif c != '' and s == '':
            result = search.advance(
                product_name=q.lower(),
                category_=c.lower(),
                offset=page, limit=limit)

        elif c == '' and s == '':
            result = search.advance(
                product_name=q.lower(),
                offset=page, limit=limit)

    if q == '':
        if c != '' and s != '':
            result = search.advance(
                category_=c.lower(),
                sub_category=s.lower(),
                offset=page, limit=limit)

        elif c != '' and s == '':
            result = search.advance(
                category_=c.lower(),
                offset=page, limit=limit)

    return HttpResponse(json.dumps(result), content_type='application/json')


def products(request, size=None, field=None, order=None):
    l = request.GET.get('page', '')

    if size is None:
        size = 10
    offset = 0

    if l != '':
        if int(l) > 1:
            size = (size * int(l))
            offset += int(size)

    products_result = search.products(offset=offset, limit=size)

    if field is not None and order is not None:
        products_result = search.products(
            offset=offset, limit=size,
            field=field, order=order)

    return HttpResponse(json.dumps(products_result), content_type='application/json')


def product(request, product_id):

    products_result = search.product(product_id)

    return HttpResponse(json.dumps(products_result), content_type='application/json')


# Category State
def categories(request):
    l = request.GET.get('size', '')

    size = int(l) if l != '' else 10
    page = 0

    if size != '' and size != 10:
        page += int(size)

    categories_result = search.categories(offset=page, limit=size)

    return HttpResponse(json.dumps(categories_result), content_type='application/json')


def category(request, category_id):

    category_result = search.category(category_id)

    return HttpResponse(json.dumps(category_result), content_type='application/json')


def category_search(request):

    query = request.GET.get('q', '')
    cleaned_query = query.replace(' ', '+')

    _id = request.GET.get('s', '')
    result = None

    if query != '':
        result = search.category_search(keyword=cleaned_query)
    elif _id != '':
        if int(_id) != 0:
            result = search.category_search(_id=int(_id))
    else:
        result = search.category_search()['hits']

    return HttpResponse(json.dumps(result), content_type='application/json')


# Category State
def subcategories(request):
    l = request.GET.get('size', '')

    size = int(l) if l != '' else 10
    page = 0

    if size != '' and size != 10:
        page += int(size)

    subcategories_result = search.subcategories(offset=page, limit=size)

    return HttpResponse(json.dumps(subcategories_result), content_type='application/json')


def subcategory(request, subcategory_id):

    subcategory_result = search.subcategory_by_id(subcategory_id=subcategory_id)

    return HttpResponse(json.dumps(subcategory_result), content_type='application/json')


def subcategory_search(request):

    query = request.GET.get('q', '')
    cleaned_query = query.replace(' ', '+')

    _id = request.GET.get('s', '')
    result = None

    if query != '':
        result = search.subcategory(keyword=cleaned_query)
    elif _id != '':
        if int(_id) != 0:
            result = search.subcategory(_id=int(_id))
    else:
        result = search.subcategory()['hits']

    return HttpResponse(json.dumps(result), content_type='application/json')


def products_by_category(category_name=None, offset=0, limit=10):

    result = search.advance(category_=category_name,
                            offset=offset,
                            limit=limit)

    return result


def products_by_brand(brand_id, offset=0, limit=10):

    result = search.advance(brand_id=brand_id,
                            offset=offset,
                            limit=limit)
    return result

