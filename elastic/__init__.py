from settings import ELASTIC_CONNECTION as ES
import json
from django.http import HttpResponse


def suggest(keyword):

    suggestion = {
        "suggestions": {
            "text": keyword,
            "completion": {
                "field": "suggest"
            }
        }
    }

    return ES.suggest(
        body=suggestion,
        index="product",
        params=None)


def search(keyword, offset=None, limit=None):

    if keyword is not None:

        return ES.search(
            index='product',
            doc_type='product',
            body={
                "query": {
                    "query_string": {
                        'query': '*{0}*'.format(keyword, keyword),
                        'default_operator': 'OR',
                        "fields": ["product_with_brand_name", ],
                    }
                },
                "size": limit,
                "explain": False,
                'fields': ['product_name', 'metadata.slug', 'product_with_brand_name'],
            },
            _source_exclude=['suggest'],
            explain=False,
        )

    else:
        no_result = {
            "error": {
                "code": "404",
                "message": "No data found!",
                "keyword": keyword
            }
        }
        return no_result


def multi_search(keyword, offset=None, limit=None):
    if keyword is not None:
        return ES.search(
            index='product',
            doc_type='product',
            body={
                "query": {
                    "multi_match": {
                        "query": keyword,
                        "type": "best_fields",
                        "fields": ["product_name", "brand", "product_number"],
                        "cutoff_frequency": 0.001,
                        "tie_breaker": 0.3
                    }
                }
            },
            from_=offset, size=limit
        )


def advance(
    product_name=None, category=None,
        subcategory=None, offset=None, limit=None):

    if product_name is None:
        if category is not None and subcategory is not None:

            query = {
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"nested": {
                                        "path": "category",
                                        "query": {
                                            "bool": {"must": {"match": {"category.name": category}}}}}},
                                    {"nested": {
                                        "path": "subcategory",
                                        "query": {
                                            "bool": {
                                                "must": {"match": {"subcategory.name": subcategory}}}}}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": "true"}},
                            ]
                         }
                    }
                }
            }

        elif category is not None and subcategory is None:
            query = {
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"nested": {
                                        "path": "category",
                                        "query": {
                                            "bool": {"must": {"match": {"category.name": category}}}}}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": "true"}},
                            ]
                         }
                    }
                }
            }
    else:

        if category is not None and subcategory is not None:

            query = {
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"product_name": product_name}},
                                    {"nested": {
                                        "path": "category",
                                        "query": {
                                            "bool": {"must": {"match": {"category.name": category}}}}}},
                                    {"nested": {
                                        "path": "subcategory",
                                        "query": {
                                            "bool": {
                                                "must": {"match": {"subcategory.name": subcategory}}}}}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": "true"}},
                            ]
                         }
                    }
                }
            }

        elif category is not None and subcategory is None:
            query = {
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"product_name": product_name}},
                                    {"nested": {
                                        "path": "category",
                                        "query": {
                                            "bool": {"must": {"match": {"category.name": category}}}}}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": "true"}},
                            ]
                         }
                    }
                }
            }
        elif category is None and subcategory is None:
            query = {
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"product_name": product_name}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": "true"}},
                            ]
                         }
                    }
                }
            }

    return ES.search(
        index='product',
        doc_type='product',
        body=query,
        from_=offset if offset is not None else 0,
        size=limit)


def subcategory(keyword=None, _id=None):
    if keyword is None:

        result = ES.search(
            index='subcategory',
            doc_type='subcategory',
            _source=True,
            body={
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"is_active": True}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": True}},
                            ]
                        }
                    }
                }
            }
        )
    else:
        result = ES.search(
            index='subcategory',
            doc_type='subcategory',
            _source=True,
            body={
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"name": keyword}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": True}},
                            ]
                        }
                    }
                }
            }
        )

    if _id is not None:
        result = ES.get_source(
            index='subcategory',
            doc_type='subcategory',
            _source=True,
            id=_id
        )

    return result


# Product State
def products(offset=None, limit=None, **kwargs):
    result = ES.search(
        index='product',
        doc_type='product',
        from_=offset,
        size=limit,
        _source=True,
        fields=['product_name'],
        request_cache=True,
        body={
            "query": {
                "filtered": {
                    "query": {
                        "bool": {
                            "must": [
                                {"match": {"is_active": True}},
                            ],
                        },
                    },
                    "filter": {
                        "and": [
                            {"term": {"is_active": True}},
                        ]
                    }
                }
            }
        }
    )

    if len(kwargs) > 0:
        fieldsets = {
            'date': 'created_date'
        }

        orders = {
            'desc': 'desc', 'descending': 'desc',
            'asc': 'asc', 'ascending': 'asc'
        }
        if kwargs['order'] \
                and kwargs['order'] != '' \
                and kwargs['order'] is not None:
            result = ES.search(
                index='product',
                doc_type='product',
                from_=offset,
                size=limit,
                _source=True,
                fields=['product_name'],
                request_cache=True,
                body={
                    "query": {
                        "filtered": {
                            "query": {
                                "bool": {
                                    "must": [
                                        {"match": {"is_active": True}},
                                    ],
                                },
                            },
                            "filter": {
                                "and": [
                                    {"term": {"is_active": True}},
                                ]
                            }
                        }
                    },
                    "sort": {
                        fieldsets[kwargs['field']]: {
                            "order": orders[kwargs['order']]
                        }
                    }
                }
            )

    return result


def product(id):
    result = ES.get(
        index='product',
        doc_type='product',
        id=id,
        _source=True,
        refresh=True,
    )

    return result


# Category State
def categories(offset=None, limit=None):
    result = ES.search(
        index='category',
        doc_type='category',
        from_=offset,
        size=limit,
        _source=True,
        fields=['name'],
        request_cache=True,
        body={
            "query": {
                "filtered": {
                    "query": {
                        "bool": {
                            "must": [
                                {"match": {"is_active": True}},
                            ],
                        },
                    },
                    "filter": {
                        "and": [
                            {"term": {"is_active": True}},
                        ]
                    }
                }
            }
        }
    )

    return result


def category(id):
    result = ES.get(
        index='category',
        doc_type='category',
        id=id,
        _source=True,
        refresh=True,
    )

    return result


def category_search(keyword=None, _id=None):

    if keyword is None:

        result = ES.search(
            index='category',
            doc_type='category',
            _source=True,
            body={
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"is_active": True}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": True}},
                            ]
                        }
                    }
                }
            }
        )
    else:
        result = ES.search(
            index='category',
            doc_type='category',
            _source=True,
            body={
                "query": {
                    "filtered": {
                        "query": {
                            "bool": {
                                "must": [
                                    {"match": {"name": keyword}},
                                ],
                            },
                        },
                        "filter": {
                            "and": [
                                {"term": {"is_active": True}},
                            ]
                        }
                    }
                }
            }
        )

    if _id is not None:
        result = ES.get_source(
            index='category',
            doc_type='category',
            _source=True,
            id=_id
        )

    return result


# Subcategory State
def subcategories(offset=None, limit=None):
    return ES.search(
        index='subcategory',
        doc_type='subcategory',
        from_=offset,
        size=limit,
        _source=True,
        fields=['name'],
        request_cache=True,
        body={
            "query": {
                "filtered": {
                    "query": {
                        "bool": {
                            "must": [
                                {"match": {"is_active": True}},
                            ],
                        },
                    },
                    "filter": {
                        "and": [
                            {"term": {"is_active": True}},
                        ]
                    }
                }
            }
        }
    )


def subcategory(id):
    return ES.get(
        index='subcategory',
        doc_type='subcategory',
        id=id,
        _source=True,
        refresh=True,
    )
