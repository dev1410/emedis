from django.http import HttpResponse
from elastic import search
import json


def index(request):
    query = request.GET.get('q', '')
    p = request.GET.get('page', '')
    limit = request.GET.get('size', '')

    size = limit if limit != '' else 10

    if p:
        page = p if int(p) > 1 and p is not '' else 0
    else:
        page = 0

    results = search(keyword=query, offset=page, limit=size)

    # return HttpResponse(json.dumps(results), content_type='application/json')
    return results
