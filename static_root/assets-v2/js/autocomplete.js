(function($){
    "use-strict";

    $("input[name='search-input']").css('height', '40px');
    $("input[name='search-input']").autocomplete({
        serviceUrl: '/suggestion/',
        minChars: 3,
        dataType: 'json',
        type: 'GET',
        transformResult: function(response) {
            return {
                suggestions: $.map(response.suggestions, function(dataItem) {
                    return {
                        value: dataItem.value,
                        data: dataItem.data
                    };
                })
            };
        },
        width: 535,
        forceFixPosition: true,
        showNoSuggestionNotice: true,
        formatResult: function (suggestion, currentValue) {
            var words = currentValue.split(' ');
            var index = 0, formatted_value = '';
            var value = suggestion.value;
            var redundant = false;

            if (words.length > 1)
            {
                for (index = 0; index < words.length; index++)
                {
                    var index_of = value.toLowerCase().indexOf(words[index].toLowerCase());
                    var current_value_length = words[index].length;
                    var current_value = words[index];

                    for (var word_index = 0; word_index < words.length; word_index++)
                    {
                        if (words[word_index].search(
                                new RegExp(words[index], 'i')) > 0
                            || words[word_index] == words[index])
                        {
                            delete words[index];
                            redundant = true;
                        }
                    }

                    if (index_of >= 0 && redundant == false)
                    {
                        formatted_value = value.substring(index_of, (index_of + current_value_length));
                        value = value.replace(formatted_value, '<strong>' + formatted_value + '</strong>');
                    }
                }

                console.log("result is :");
                console.log(words);

                return '<a href="/product_detail/' + suggestion.data +'/">' +
                    '<div class="autocomplete">' +
                    value +
                    '</div>' +
                    '</a>';
            }
            else if (words.length == 1)
            {
                return '<a href="/product_detail/' + suggestion.data +'/">' +
                    '<div class="autocomplete">' +
                    $.Autocomplete.formatResult(suggestion, currentValue) +
                    '</div>' +
                    '</a>';
            }
        },
        noSuggestionNotice: '<div class="autocomplete-suggestion"> No results </div>',
        paramName: 'q',
        tabDisabled: true
    });
})(jQuery);