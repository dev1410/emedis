(function ($) {
    $(document).ready(function(){
        $('.inline-group').addClass("wide");
        $(this).custom_prepopulate(
            '#id_productmetadata-0-slug', '#id_product_name',
            'product_name', 256);

        var sequence_element = [
            [".product_id", '#producttag-group'],
            ["#productdetail-group","#productimage_set-group", '#productdocument_set-group'],
            ["#productspecificationandcertification-group", "#productmetadata-group"]
        ];

        $(".submit-row").replaceWith(
            '<div class="submit-row">' +
            '<input type="button" value="Back" id="prev" name="_prev">' +
            '<input type="button" value="Next" id="next" class="default" name="_next">' +
            '<input type="submit" value="Save" id="save" class="default" name="_save">' +
            '<input type="submit" value="Save and Continue Edit" id="save_edit" name="_continue">' +
            '<input type="submit" value="Save and Add Another" id="save_add" name="_addanother">' +
            '</div>');
        var button = {
            'next': {
                'type': 'button',
                'value': 'Next',
                'id': 'next',
                'class': 'default',
                'name': '_next'
            },
            'prev': {
                'type': 'button',
                'value': 'Back',
                'id': 'prev'
            },
            'submit': {
                'type': 'submit',
                'value': 'Save',
                'id': 'save',
                'name': '_save',
                'class': 'default'
            },
            'submit_edit': {
                'type': 'submit',
                'value': 'Save and Continue Edit',
                'id': 'save_edit',
                'name': '_continue'
            },
            'submit_add': {
                'type': 'submit',
                'value': 'Save and Add Another',
                'id': 'save_add',
                'name': '_addanother'
            },
            'parent': {
                'class': '.submit-row'
            }
        }

        $(this).sequence(
            sequence_element, button);
    });
})(django.jQuery);