(function ($) {
    $.fn.sequence = function(reference, button)
    {
        var component;
        var step;
        var pre_id = '#';

        component = [];

        if ($.isArray(reference))
        {
            for (var i=0; i<reference.length; i++)
            {
                component[i] = reference[i];
            }
        }

        $(pre_id+button.next.id).click(function()
        {
            if (step <= component.length)
            {
                step += 1;

                if (step+1 == component.length)
                {
                    $(pre_id+button.next.id).hide();
                    $(pre_id+button.submit.id).show();
                    $(pre_id+button.submit_edit.id).show();
                    $(pre_id+button.submit_add.id).show();
                }
                else if(step > 0)
                {
                    $(pre_id+button.prev.id).show();
                }

                if ($.isArray(component[step-1]))
                {
                    for (var i=0; i<component[step-1].length; i++)
                    {
                        $(component[step-1][i]).hide();
                    }
                } else {
                     $(component[step-1]).hide();
                }

                if ($.isArray(component[step]))
                {
                    for (var i=0; i<component[step].length; i++)
                    {
                        $(component[step][i]).fadeIn("slow");
                    }
                } else {
                    $(component[step]).fadeIn("slow");
                }
            }
        });

        $(pre_id+button.prev.id).click(function()
        {
            if (step > 0)
            {
                step -= 1;

                if (step+1 == component.length-1)
                {
                    $(pre_id+button.next.id).show();
                    $(pre_id+button.submit.id).hide();
                    $(pre_id+button.submit_edit.id).hide();
                    $(pre_id+button.submit_add.id).hide();
                }
                else if(step == 0)
                {
                    $(pre_id+button.prev.id).hide();
                }

                if ($.isArray(component[step+1]))
                {
                    for (var i=0; i<component.length; i++)
                    {
                        $(component[step+1][i]).hide();
                    }
                } else {
                    $(component[step+1]).hide();
                }

                if ($.isArray(component[step]))
                {
                    for (var i=0; i<component.length; i++)
                    {
                        $(component[step][i]).fadeIn();
                    }
                } else {
                    $(component[step]).fadeIn();
                }
            }
            else {alert("Min value reached");}
        });

        $(document).ready(function($){
            for (var i=1; i<component.length; i++)
            {
                step = 0;
                if ($.isArray(component[1]))
                {
                    for (var j=0; j<component[i].length; j++)
                    {
                        $(component[i][j]).hide();
                    }
                } else {
                    $(component[i]).hide();
                }
            }

            $(pre_id+button.prev.id).hide();
            $(pre_id+button.submit.id).hide();
            $(pre_id+button.submit_edit.id).hide();
            $(pre_id+button.submit_add.id).hide();
        });
    }
})(django.jQuery);