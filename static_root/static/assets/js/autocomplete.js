(function($){
    'use strict';

    var input = $("input[name='search-input']");

    input.css('height', '40px');
    input.autocomplete({
        serviceUrl: '/suggestion/',
        minChars: 3,
        dataType: 'json',
        type: 'GET',
        transformResult: function(response) {
            return {
                suggestions: $.map(response.suggestions, function(dataItem) {
                    return {
                        value: dataItem.value,
                        data: dataItem.data
                    };
                })
            };
        },
        width: 535,
        forceFixPosition: true,
        showNoSuggestionNotice: true,
        formatResult: function (suggestion, currentValue) {
            var words = currentValue.split(' ');
            var index = 0, formatted_value = '';
            var value = suggestion.value;

            if (words.length > 1)
            {
                for (index = 0; index < words.length; index++)
                {
                    var current_value = words[index];
                    var checkpoint = 0;

                    for (var word_index = 0; word_index < words.length; word_index++)
                    {
                        if (words[word_index].search(
                                new RegExp(current_value, 'i')) > 0)
                        {
                            words.splice(index, index+1);
                        }

                        if (current_value == words[word_index])
                        {
                            checkpoint += 1;

                            if (checkpoint > 1)
                            {
                                words.splice(index, index+1);
                            }
                        }
                    }

                    var index_of = value.toLowerCase().indexOf(words[index].toLowerCase());
                    var current_value_length = words[index].length;

                    if (index_of >= 0)
                    {
                        formatted_value = value.substring(index_of, (index_of + current_value_length));
                        value = value.replace(formatted_value, '<strong>' + formatted_value + '</strong>');
                    }
                }

                return '<a href="/product_detail/' + suggestion.data +'">' +
                    '<div class="autocomplete">' +
                    value +
                    '</div>' +
                    '</a>';
            }
            else if (words.length == 1)
            {
                return '<a href="/product_detail/' + suggestion.data +'">' +
                    '<div class="autocomplete">' +
                    $.Autocomplete.formatResult(suggestion, currentValue) +
                    '</div>' +
                    '</a>';
            }
        },
        noSuggestionNotice: '<div class="autocomplete-suggestion"> No Result. </div>',
        paramName: 'q',
        tabDisabled: true
    });
})(jQuery);