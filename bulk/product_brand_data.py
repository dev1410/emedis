import sys
from product.brand.models import Brand
from django.template.defaultfilters import slugify


class ProductBrandData(object):
    __product_brand_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_brand_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_brand_list(self):
        return self.__product_brand_list

    @product_brand_list.setter
    def product_brand_list(self, value):
        self.__product_brand_list = value

    def product_brand_data(self,
                           name=''):
        product_brand = Brand()
        product_brand.name = str(name).title()
        product_brand.brand_logo = ''
        product_brand.description = ''
        product_brand.is_active = True
        product_brand.slug = slugify(product_brand.name)
        # product_brand.created_date = ''
        product_brand.created_user = self.request.user
        # product_brand.updated_date = ''
        # product_brand.updated_user = ''

        product_brand.save()

        self.product_brand_populate_list()

    def product_brand_populate_list(self,):
        try:
            product_brands = Brand.objects.values_list('name', flat=True).all().order_by('name')
            new_list = map(str, product_brands)
            self.product_brand_list = map(self.__lower_strip_brand__, new_list)
            # self.product_brand_list = [product_brand.lower().strip() for product_brand in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Brand Populate List {0}'.format(sys.exc_info()[1])

        return self.product_brand_list

    def bulk(self):
        map(self.__row_creator__, self.data)

        # for row in self.data:
        #     manufacture = str(row['manufacture']).lower().strip()
        #     if manufacture not in self.product_brand_list:
        #         self.product_brand_data(name=manufacture)
                # print manufacture + ' inserted to manufacture list'

    def __row_creator__(self, row):
        manufacture = str(row['manufacture']).lower().strip()
        if manufacture not in self.product_brand_list:
            self.product_brand_data(name=manufacture)

    @staticmethod
    def __lower_strip_brand__(product_brand):
        return product_brand.lower().strip()
