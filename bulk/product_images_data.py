import csv
import os

import functools
from django.conf import settings
from django.core.files import File
from product.models import ProductImage, Product


class ImagesDataBase(object):

    def __init__(self, data, products):
        """
        :param data:
        :type data: :py:class:list
        :param products:
        :type products: :py:class:list
        """
        self.emedis_product_images_dir = 'images/product'
        self.root_dir_name = 'vendor_data'
        self.source_dir_name = 'images_data'

        self.data = data
        self.products = products

        self.root_dir = os.path.join(settings.BASE_DIR, self.root_dir_name)
        self.source_dir = os.path.join(self.root_dir, self.source_dir_name)
        self.csv_source_dir = os.path.join(self.source_dir, 'csv')

        self.destination_dir = os.path.join(settings.MEDIA_ROOT,
                                            self.emedis_product_images_dir)

        self.__dir_checker__()

    def __row_collections__(self):
        pass

    def __dir_checker__(self):
        dir_exists = os.path.isdir

        if not dir_exists(self.root_dir):
            os.mkdir(self.root_dir)

        if not dir_exists(self.source_dir):
            os.mkdir(self.source_dir)

        if not dir_exists(self.csv_source_dir):
            os.mkdir(self.csv_source_dir)

    def execute(self):
        pass

    def __repr__(self):
        return "<{class_name}, (root_dir='{rd}'), " \
               "(source_dir='{sd}'), " \
               "(csv_source_dir='{csv_sd}')>".format(class_name=self.__class__.__name__,
                                                     root_dir=self.root_dir,
                                                     sd=self.source_dir,
                                                     csv_sd=self.csv_source_dir)


class ImageDumper(ImagesDataBase):

    def __init__(self, data, products=None):
        super(ImageDumper, self).__init__(data=data, products=products)

    @staticmethod
    def __csv_finder__(filename):
        if filename.endswith('.csv'):
            return filename

    def __row_reader__(self, csv_file):
        return map(
            lambda col: [col[0], col[1]],
            csv.reader(
                open(os.path.join(self.csv_source_dir, csv_file)),
                delimiter='\t',
                dialect='excel-tab'
            )
        )

    def execute(self):
        products_is_empty = self.products.__len__() == 0

        if products_is_empty:
            raise IOError(
                'Empty products object list : \'%s\'' %
                self.csv_source_dir)
        else:
            list_data = self.data

        result = 0

        if not products_is_empty:
            map(
                functools.partial(
                    self.__image_object_creator__,
                    products=self.products,
                    data_list=self.data),
                self.data)

        else:

            for length in xrange(list_data.__len__()):
                product_obj = Product.objects.filter(id=list_data[length][0])[0]
                image_files = list_data[length][1].split(',')

                for index in xrange(image_files.__len__()):
                    image_obj = ProductImage()
                    image_obj.product = product_obj

                    path_to_file = os.path.join(self.source_dir, image_files[index])
                    if not os.path.exists(path_to_file):
                        pass
                    elif os.path.exists(
                            os.path.join(
                                self.destination_dir,
                                image_files[index])):
                        pass
                    else:
                        image_obj.image.save(
                            path_to_file,
                            File(open(path_to_file, 'r'))
                        )

                        result += 1

        return "Successfully dump %d image(s) of %d data" % (result, list_data.__len__())

    def __image_object_creator__(self, data, products, data_list):
        path_to_image = os.path.join(self.source_dir,
                                     str(data['image_name']).strip())

        if os.path.exists(os.path.join(settings.BASE_DIR, path_to_image)):
            try:
                image_object = ProductImage()
                image_object.product = products[data_list.index(data)]

                image_object.image.save(path_to_image,
                                        File(open(path_to_image, 'r')))
                return image_object
            except ValueError as e:
                print ":py:class:`bulk.product_images_data." \
                      "__image_object_creator__`: \r\n({err})".format(err=e.message)
