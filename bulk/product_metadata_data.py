import re
import sys
import functools
from django.template.defaultfilters import slugify
from product.models import Product
from product.models import ProductMetadata


class ProductMetadataData(object):
    __product_metadata_list = list()
    __data = None
    __request = None
    __forbiden_char = ['\/', '\&']

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_metadata_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_metadata_list(self):
        return self.__product_metadata_list

    @product_metadata_list.setter
    def product_metadata_list(self, value):
        self.__product_metadata_list = value

    def product_metadata_data(self,
                              product_name,):
        product_metadata = ProductMetadata()
        product = Product.objects.get(product_name=product_name)
        product_metadata.product = product
        product_metadata.slug = slugify(product_name)
        product_metadata.meta_description = product.get_product_spesification_and_certification().specification
        product_metadata.meta_keyword = product_name

        product_metadata.save()

        self.product_metadata_populate_list()

    def product_metadata_populate_list(self,):
        try:
            product_metadatas = ProductMetadata.objects.values_list('product__product_name', flat=True).all().order_by('product__product_name')
            new_list = map(str, product_metadatas)
            self.product_metadata_list = map(self.__lower_strip_metadata__, new_list)
            # self.product_metadata_list = [product_metadata.lower().strip() for product_metadata in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Metadata Populate List {0}'.format(sys.exc_info()[1])

        return self.product_metadata_list

    def bulk(self):
        row_count = len(self.data)
        current_row = 0

        map(
            functools.partial(
                self.__row_creator__,
                current_row=current_row),
            self.data)
        # for row in self.data:
        #     current_row += 1
        #     product_name = str(self.__repair_name(row['product_name'])).lower()
        #
        #     if product_name not in self.product_metadata_list:
        #         self.product_metadata_data(product_name=product_name,)
        # print '{0} of {1} {2} inserted to product metadata list'.format(current_row, row_count, product_name)

    def __repair_name(self, name):
        return re.sub(
            r'|'.join(self.__forbiden_char), ' ', ' '.join([word.strip() for word in name.split()])
        ).strip()

    def __row_creator__(self, data, current_row):
        current_row += 1
        product_name = str(self.__repair_name(data['product_name'])).lower()

        if product_name not in self.product_metadata_list:
            self.product_metadata_data(product_name=product_name,)

    @staticmethod
    def __lower_strip_metadata__(product_metadata):
        return product_metadata.lower().strip()
