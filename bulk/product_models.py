from category.models import Category

from product.models import ProductSupplier
from product.models import ProductSubSupplier
from product.models import ProductCurrency
from product.models import CountryOfOrigin


class ProductModels(object):
    __product_category_list = list()
    __product_supplier_list = list()
    __product_subsupplier_list = list()
    __product_currency_list = list()
    __product_country_of_origin_list = list()

    def __init__(self):
        pass

    @property
    def product_category_list(self):
        return self.__product_category_list

    @product_category_list.setter
    def product_category_list(self, value):
        self.__product_category_list = value

    @property
    def product_supplier_list(self):
        return self.__product_supplier_list

    @product_supplier_list.setter
    def product_supplier_list(self, value):
        self.__product_supplier_list = value

    @property
    def product_subsupplier_list(self):
        return self.__product_subsupplier_list

    @product_subsupplier_list.setter
    def product_subsupplier_list(self, value):
        self.__product_subsupplier_list = value

    @property
    def product_currency_list(self):
        return self.__product_currency_list

    @product_currency_list.setter
    def product_currency_list(self, value):
        self.__product_currency_list = value

    @property
    def product_country_of_origin_list(self):
        return self.__product_country_of_origin_list

    @product_country_of_origin_list.setter
    def product_country_of_origin_list(self, value):
        self.__product_country_of_origin_list = value



    def product_category_populate_list(self,):
        product_categories =  Category.objects.values_list('name', flat=True).all().order_by('name')
        new_list = map(str, product_categories)
        self.product_category_list = map(self.__lower_strip_category__, new_list)
        # self.product_category_list = [product_categories.lower().strip() for product_brand in new_list]
        del new_list

        return self.product_category_list

    def product_supplier_populate_list(self,):
        product_suppliers =  ProductSupplier.objects.values_list('name', flat=True).all().order_by('name')
        new_list = map(str, product_suppliers)
        self.product_supplier_list = map(self.__lower_strip_supplier__, new_list)
        # self.product_supplier_list = [product_suppliers.lower().strip() for product_supplier in new_list]
        del new_list

        return self.product_supplier_list

    def product_subsupplier_populate_list(self,):
        product_subsuppliers =  ProductSubSupplier.objects.values_list('name', flat=True).all().order_by('name')
        new_list = map(str, product_subsuppliers)
        self.product_subsupplier_list = map(self.__lower_strip_subsupplier__, new_list)
        # self.product_subsupplier_list = [product_subsuppliers.lower().strip() for product_subsupplier in new_list]
        del new_list

    def product_currency_populate_list(self):
        product_currencies =  ProductCurrency.objects.values_list('full_label', flat=True).all().order_by('full_label')
        new_list = map(str, product_currencies)
        self.product_currency_list = map(self.__lower_strip_currency__, new_list)
        self.product_currency_list = [product_currencies.lower().strip() for product_currency in new_list]
        del new_list

        return self.product_currency_list

    def product_country_of_origin_populate_list(self):
        product_country_of_origins =  CountryOfOrigin.objects.values_list('country_name', flat=True).all().order_by('country_name')
        new_list = map(str, product_country_of_origins)
        self.product_country_of_origin_list = map(self.__lower_strip_coo__, new_list)
        # self.product_country_of_origin_list = [product_country_of_origins.lower().strip() for product_country_of_origin in new_list]
        del new_list

        return self.product_country_of_origin_list

    @staticmethod
    def __lower_strip_category__(product_categories):
        return product_categories.lower().strip()

    @staticmethod
    def __lower_strip_supplier__(product_supplier):
        return product_supplier.lower().strip()

    @staticmethod
    def __lower_strip_subsupplier__(product_subsupplier):
        return product_subsupplier.lower().strip()

    @staticmethod
    def __lower_strip_currency__(product_currency):
        return product_currency.lower().strip()

    @staticmethod
    def __lower_strip_coo__(product_coo):
        return product_coo.lower().strip()
