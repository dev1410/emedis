import sys
from product.models import CountryOfOrigin


class ProductCountryOfOriginData(object):
    __product_country_of_origin_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_country_of_origin_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_country_of_origin_list(self):
        return self.__product_country_of_origin_list

    @product_country_of_origin_list.setter
    def product_country_of_origin_list(self, value):
        self.__product_country_of_origin_list = value

    def product_country_of_origin_data(self,
                                       country_name=''):
        product_country_of_origin = CountryOfOrigin()
        product_country_of_origin.country_name = str(country_name).title().strip()
        product_country_of_origin.alpha2_code = ''
        product_country_of_origin.alpha3_code = ''
        product_country_of_origin.numeric_code = ''
        product_country_of_origin.iso_code = ''
        product_country_of_origin.is_active = True
        # product_country_of_origin.created_date = ''
        product_country_of_origin.created_user = self.request.user
        # product_country_of_origin.updated_date = ''
        # product_country_of_origin.updated_user = ''

        product_country_of_origin.save()

        self.product_country_of_origin_populate_list()

    def product_country_of_origin_populate_list(self,):
        try:
            product_country_of_origins =  CountryOfOrigin.objects.values_list('country_name', flat=True).all().order_by('country_name')
            new_list = map(str, product_country_of_origins)
            self.product_country_of_origin_list = map(self.__lower_strip_coo__, new_list)
            # self.product_country_of_origin_list = [product_country_of_origin.lower().strip() for product_country_of_origin in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Country Of Origin Populate List {0}'.format(sys.exc_info()[1])

        return self.product_country_of_origin_list

    def bulk(self):
        map(self.__row_creator__, self.data)
        # for row in self.data:
        #     try:
        #         country_of_origin = str(row['country_of_origin']).lower().strip().replace('  ', ' ')
        #         if country_of_origin not in self.product_country_of_origin_list:
        #             self.product_country_of_origin_data(country_name=country_of_origin)
        #             # print country_of_origin + ' inserted to country_of_origin list'
        #     except:
        #         pass
                # print 'Country of Origin not found'

    def __row_creator__(self, row):
        try:
            country_of_origin = str(row['country_of_origin']).lower().strip().replace('  ', ' ')
            if country_of_origin not in self.product_country_of_origin_list:
                self.product_country_of_origin_data(country_name=country_of_origin)
                # print country_of_origin + ' inserted to country_of_origin list'
        except:
            pass
            # print 'Country of Origin not found'

    @staticmethod
    def __lower_strip_coo__(country_of_origin):
        return country_of_origin.lower().strip()
