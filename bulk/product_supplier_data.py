import sys
from product.models import ProductSupplier


class ProductSupplierData(object):
    __product_supplier_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_supplier_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_supplier_list(self):
        return self.__product_supplier_list

    @product_supplier_list.setter
    def product_supplier_list(self, value):
        self.__product_supplier_list = value

    def product_supplier_data(self,
                              name=''):
        product_supplier = ProductSupplier()
        product_supplier.name = str(name).title()
        # product_supplier.user_group = self.request.user.groups.all()
        product_supplier.is_active = True
        product_supplier.is_verified = True
        # product_supplier.created_date = ''
        product_supplier.created_user = self.request.user
        # product_supplier.updated_date = ''
        # product_supplier.updated_user = ''

        product_supplier.save()

        self.product_supplier_populate_list()

    def product_supplier_populate_list(self,):
        try:
            product_suppliers =  ProductSupplier.objects.values_list('name', flat=True).all().order_by('name')
            new_list = map(str, product_suppliers)
            self.product_supplier_list = map(self.__lower_strip_supplier__, new_list)
            # self.product_supplier_list = [product_supplier.lower().strip() for product_supplier in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Supplier Populate List {0}'.format(sys.exc_info()[1])

        return self.product_supplier_list

    def bulk(self):
        map(self.__row_creator__, self.data)
        # for row in self.data:
        #     supplier = str(row['supplier']).lower().strip()
        #     if supplier not in self.product_supplier_list:
        #         self.product_supplier_data(name=supplier)
                # print supplier + ' inserted to supplier list'

    def __row_creator__(self, row):
        supplier = str(row['supplier']).lower().strip()
        if supplier not in self.product_supplier_list:
            self.product_supplier_data(name=supplier)

    @staticmethod
    def __lower_strip_supplier__(supplier):
        return supplier.lower().strip()
