import re
import sys
import functools
from product.models import Product
from product.models import ProductSpecificationAndCertification


class ProductSpecificationAndCertificationData(object):
    __product_spesification_and_certification_list = list()
    __data = None
    __request = None
    __forbiden_char = ['\/', '\&']

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_spesification_and_certification_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_spesification_and_certification_list(self):
        return self.__product_spesification_and_certification_list

    @product_spesification_and_certification_list.setter
    def product_spesification_and_certification_list(self, value):
        self.__product_spesification_and_certification_list = value

    def product_spesification_and_certification_data(self,
                                                     product_name,
                                                     specification,
                                                     oem,
                                                     product_certification,
                                                     fda_clearance,
                                                     ce_mark,
                                                     iec_compliance,
                                                     locally_content,
                                                     green_features,
                                                     umdns_code,
                                                     additional_information,):
        product_spesification_and_certification = ProductSpecificationAndCertification()
        product_spesification_and_certification.product = Product.objects.filter(product_name=product_name)[0]
        product_spesification_and_certification.specification = specification
        product_spesification_and_certification.OEM = oem
        product_spesification_and_certification.product_certification = product_certification
        product_spesification_and_certification.FDA_clearance = fda_clearance
        product_spesification_and_certification.CE_mark = ce_mark
        product_spesification_and_certification.IEC_compliance = iec_compliance
        product_spesification_and_certification.locally_content = locally_content
        product_spesification_and_certification.green_features = green_features
        product_spesification_and_certification.UMDNS_code = umdns_code
        product_spesification_and_certification.additional_information = additional_information

        product_spesification_and_certification.save()

        self.product_spesification_and_certification_populate_list()

    def product_spesification_and_certification_populate_list(self,):
        try:
            product_spesification_and_certifications = ProductSpecificationAndCertification.objects.values_list('product__product_name', flat=True).all().order_by('product__product_name')
            new_list = map(str, product_spesification_and_certifications)
            self.product_spesification_and_certification_list = map(self.__lower_strip_sac__, new_list)
            # self.product_spesification_and_certification_list = [product_spesification_and_certification.lower().strip() for product_spesification_and_certification in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Spesification and Certification Populate List {0}'.format(sys.exc_info()[1])

        return self.product_spesification_and_certification_list

    def bulk(self):
        row_count = len(self.data)
        current_row = 0

        map(
            functools.partial(
                self.__row_creator__,
                current_row=current_row),
            self.data)

        # for row in self.data:
        #     current_row += 1
        #     product_name = str(self.__repair_name(row['product_name'])).lower()
        #
        #     if 'other_spesifications' in row:
        #         specification = str(row['function']).lower().strip()
        #     else:
        #         specification = ''
        #
        #     if 'oem' in row:
        #         oem = str(row['oem']).lower().strip()
        #     else:
        #         oem = ''
        #
        #     if 'product_certification' in row:
        #         product_certification = str(row['product_certification']).lower().strip()
        #     else:
        #         product_certification = ''
        #
        #     if 'fda_clearance' in row:
        #         fda_clearance = str(row['fda_clearance']).lower().strip()
        #     else:
        #         fda_clearance = ''
        #
        #     if 'ce_mark' in row:
        #         ce_mark = str(row['ce_mark']).lower().strip()
        #     else:
        #         ce_mark = ''
        #
        #     if 'iec_compliance' in row:
        #         iec_compliance = str(row['iec_compliance']).lower().strip()
        #     else:
        #         iec_compliance = ''
        #
        #     if 'locally_content' in row:
        #         locally_content = str(row['locally_content']).lower().strip()
        #     else:
        #         locally_content = ''
        #
        #     if 'green_features' in row:
        #         green_features = str(row['green_features']).lower().strip()
        #     else:
        #         green_features = ''
        #
        #     if 'umdns_code' in row:
        #         umdns_code = str(row['umdns_code']).lower().strip()
        #     else:
        #         umdns_code = ''
        #
        #     if 'additional_information' in row:
        #         additional_information = str(row['other_information']).lower().strip()
        #     else:
        #         additional_information = ''
        #
        #     if product_name not in self.product_spesification_and_certification_list:
        #         self.product_spesification_and_certification_data(product_name=product_name,
        #                                                           specification=specification,
        #                                                           oem=oem,
        #                                                           product_certification=product_certification,
        #                                                           fda_clearance=fda_clearance,
        #                                                           ce_mark=ce_mark,
        #                                                           iec_compliance=iec_compliance,
        #                                                           locally_content=locally_content,
        #                                                           green_features=green_features,
        #                                                           umdns_code=umdns_code,
        #                                                           additional_information=additional_information,)
                # print '{0} of {1} {2} inserted to product spesification and certification list'.format(current_row, row_count, product_name)

    def __repair_name(self, name):
        return re.sub(
            r'|'.join(self.__forbiden_char), ' ', ' '.join([word.strip() for word in name.split()])
        ).strip()

    def __row_creator__(self, data, current_row):
        current_row += 1
        product_name = str(self.__repair_name(data['product_name'])).lower()

        if product_name not in self.product_spesification_and_certification_list:
            specification = ''
            if 'other_spesifications' in data:
                specification = str(data['function']).lower().strip()

            oem = ''
            if 'oem' in data:
                oem = str(data['oem']).lower().strip()

            product_certification = ''
            if 'product_certification' in data:
                product_certification = str(data['product_certification']).lower().strip()

            fda_clearance = ''
            if 'fda_clearance' in data:
                fda_clearance = str(data['fda_clearance']).lower().strip()

            ce_mark = ''
            if 'ce_mark' in data:
                ce_mark = str(data['ce_mark']).lower().strip()

            iec_compliance = ''
            if 'iec_compliance' in data:
                iec_compliance = str(data['iec_compliance']).lower().strip()

            locally_content = ''
            if 'locally_content' in data:
                locally_content = str(data['locally_content']).lower().strip()

            green_features = ''
            if 'green_features' in data:
                green_features = str(data['green_features']).lower().strip()

            umdns_code = ''
            if 'umdns_code' in data:
                umdns_code = str(data['umdns_code']).lower().strip()

            additional_information = ''
            if 'additional_information' in data:
                additional_information = str(data['other_information']).lower().strip()

            self.product_spesification_and_certification_data(product_name=product_name,
                                                              specification=specification,
                                                              oem=oem,
                                                              product_certification=product_certification,
                                                              fda_clearance=fda_clearance,
                                                              ce_mark=ce_mark,
                                                              iec_compliance=iec_compliance,
                                                              locally_content=locally_content,
                                                              green_features=green_features,
                                                              umdns_code=umdns_code,
                                                              additional_information=additional_information,)

    @staticmethod
    def __lower_strip_sac__(sac):
        return sac.lower().strip()
