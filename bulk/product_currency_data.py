import sys
from product.models import ProductCurrency


class ProductCurrencyData(object):
    __product_currency_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_currency_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_currency_list(self):
        return self.__product_currency_list

    @product_currency_list.setter
    def product_currency_list(self, value):
        self.__product_currency_list = value

    def product_currency_data(self,
                           short_label=''):
        product_currency = ProductCurrency()
        product_currency.short_label = str(short_label).upper().strip()
        product_currency.full_label = ''
        # product_currency.kurs_to_rupiah = 0
        product_currency.is_active = True
        # product_currency.created_date = ''
        product_currency.created_user = self.request.user
        # product_currency.updated_date = ''
        # product_currency.updated_user = ''

        product_currency.save()

        self.product_currency_populate_list()

    def product_currency_populate_list(self,):
        try:
            product_currencies =  ProductCurrency.objects.values_list('short_label', flat=True).all().order_by('short_label')
            new_list = map(str, product_currencies)
            self.product_currency_list = map(self.__lower_strip_currency__, new_list)
            # self.product_currency_list = [product_currency.lower().strip() for product_currency in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Currency Populate List {0}'.format(sys.exc_info()[1])

        return self.product_currency_list

    def bulk(self):
        map(self.__row_creator__, self.data)
        # for row in self.data:
        #     currency = str(row['product_currency']).lower().strip()
        #     if currency not in self.product_currency_list:
        #         self.product_currency_data(short_label=currency)
                # print currency + ' inserted to currency list'

    def __row_creator__(self, row):
        currency = str(row['product_currency']).lower().strip()
        if currency not in self.product_currency_list:
            self.product_currency_data(short_label=currency)

    @staticmethod
    def __lower_strip_currency__(product_currency):
        return product_currency.lower().strip()
