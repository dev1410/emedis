from django.http import HttpResponse
from lkpp_dump_to_database import LKKPDumpToDatabase
from product.models import Product
import datetime


def bulk(request):
    start = datetime.datetime.now()
    response = ''

    client_address = str(request.META['REMOTE_ADDR'])

    if client_address == '114.199.104.23' or client_address == '36.71.177.63' or client_address == '127.0.0.1':
        response = client_address

        lkpp_dump_to_database = LKKPDumpToDatabase(session_name='a', request=request)
    else:
        response = ''
    print datetime.datetime.now() - start
    return HttpResponse(response)
