import re
import sys
import functools
from decimal import *
from product.models import ProductDetail
from product.models import ProductCurrency
from product.models import CountryOfOrigin
from product.models import Product
from datetime import datetime


class ProductDetailData(object):
    __product_detail_list = list()
    __data = None
    __request = None
    __forbiden_char = ['\/', '\&']

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_detail_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_detail_list(self):
        return self.__product_detail_list

    @product_detail_list.setter
    def product_detail_list(self, value):
        self.__product_detail_list = value

    def product_detail_data(self,
                            product_name='',
                            e_catalogue_price='',
                            private_price='',
                            e_catalogue_price_currency='',
                            private_price_currency='',
                            price_date='',
                            country_of_origin='',
                            url='',):
        product_detail = ProductDetail()
        product_detail.product = Product.objects.filter(product_name=product_name)[0]
        product_detail.e_catalogue_price = e_catalogue_price
        product_detail.private_price = private_price

        if e_catalogue_price_currency != '':
            product_detail.e_catalogue_price_currency = ProductCurrency.objects.get(short_label=e_catalogue_price_currency)

        product_detail.private_price_currency = ProductCurrency.objects.get(short_label=private_price_currency)
        product_detail.price_date = price_date
        product_detail.warranty_period = ''
        product_detail.estimated_delivery_time = ''
        product_detail.country_of_origin = CountryOfOrigin.objects.get(country_name=country_of_origin)
        product_detail.special_training = 0
        product_detail.url = url

        product_detail.save()

        self.product_detail_populate_list()

    def product_detail_populate_list(self,):
        try:
            product_details = ProductDetail.objects.values_list('product__product_name', flat=True).all().order_by('product__product_name')
            new_list = map(str, product_details)
            self.product_detail_list = map(self.__lower_strip_product_detail__, new_list)
            # self.product_detail_list = [product_detail.lower().strip() for product_detail in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Detail Populate List {0}'.format(sys.exc_info()[1])

        return self.product_detail_list

    def bulk(self):
        row_count = len(self.data)
        current_row = 0
        map(
            functools.partial(
                self.__row_creator__,
                current_row=current_row),
            self.data)

        # for row in self.data:
        #     current_row += 1
        #     product_name = str(self.__repair_name(row['product_name'])).lower()
        #
        #     try:
        #         e_catalogue_price = Decimal(str(row['product_price']).lower().strip())
        #     except:
        #         e_catalogue_price = Decimal(0)
        #
        #     try:
        #         private_price = Decimal(str(row['private_price']).lower().strip())
        #     except:
        #         private_price = Decimal(0)
        #
        #     try:
        #         currency = str(row['product_currency']).lower().strip()
        #     except:
        #         currency = ''
        #
        #     try:
        #         price_date = str(row['updated_price_date']).lower().strip()
        #     except:
        #         from datetime import datetime
        #         price_date = datetime.today()
        #
        #     try:
        #         country_of_origin = str(row['country_of_origin']).lower().strip().replace('  ', ' ')
        #     except:
        #         country_of_origin = ''
        #
        #     try:
        #         url = str(row['product_link']).lower().strip()
        #     except:
        #         url = ''
        #
        #     if product_name not in self.product_detail_list:
        #         self.product_detail_data(product_name=product_name,
        #                                  e_catalogue_price=e_catalogue_price,
        #                                  private_price=private_price,
        #                                  currency=currency,
        #                                  price_date=price_date,
        #                                  country_of_origin=country_of_origin,
        #                                  url=url)
                # print '{0} of {1} {2} inserted to product detail list'.format(current_row, row_count, product_name)

    def __repair_name(self, name):
        return re.sub(
            r'|'.join(self.__forbiden_char), ' ', ' '.join([word.strip() for word in name.split()])
        ).strip()

    def __row_creator__(self, data, current_row):
        current_row += 1

        product_name = self.__repair_name(data['product_name']).lower()

        e_catalogue_price = Decimal(0)
        if 'product_price' in data:
            if data['product_price'] != '':
                e_catalogue_price = Decimal(str(data['product_price']).lower().strip())

        private_price = Decimal(0)
        if 'private_price' in data:
            if data['private_price'] != '':
                private_price = Decimal(str(data['private_price']).lower().strip())

        e_catalogue_price_currency = ''
        if 'e_catalogue_price_currency' in data:
            if data['e_catalogue_price_currency'] != '':
                e_catalogue_price_currency = str(data['e_catalogue_price_currency']).lower().strip()

        private_price_currency = ''
        if 'private_price_currency' in data:
            if data['private_price_currency'] != '':
                private_price_currency = str(data['private_price_currency']).lower().strip()

        price_date = datetime.today().strftime('%Y-%m-%d')
        if 'updated_price_date' in data:
            if data['updated_price_date'] != '':
                price_date = str(data['updated_price_date']).lower().strip()

        country_of_origin = ''
        if 'country_of_origin' in data:
            country_of_origin = str(data['country_of_origin']).lower().strip().replace('  ', ' ')

        url = ''
        if 'product_link' in data:
            url = str(data['product_link']).lower().strip()

        if product_name not in self.product_detail_list:
            self.product_detail_data(product_name=product_name,
                                     e_catalogue_price=e_catalogue_price,
                                     private_price=private_price,
                                     e_catalogue_price_currency=e_catalogue_price_currency,
                                     private_price_currency=private_price_currency,
                                     price_date=price_date,
                                     country_of_origin=country_of_origin,
                                     url=url)

    @staticmethod
    def __lower_strip_product_detail__(product_detail):
        return product_detail.lower().strip()
