import sys
from category.subcategory.models import Subcategory
from category.models import Category
from django.template.defaultfilters import slugify


class ProductSubcategoryData(object):
    __product_subcategory_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_subcategory_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_subcategory_list(self):
        return self.__product_subcategory_list

    @product_subcategory_list.setter
    def product_subcategory_list(self, value):
        self.__product_subcategory_list = value

    def product_subcategory_data(self,
                                 category='',
                                 subcategory=''):
        product_subcategory = Subcategory()
        product_subcategory.category = Category.objects.get(name=category)
        product_subcategory.name = str(subcategory).title()
        # product_subcategory.sort_order = ''
        product_subcategory.description = ''
        # product_subcategory.subcategory_icon = ''
        product_subcategory.slug = slugify(product_subcategory.name)
        product_subcategory.is_active = True
        # product_subcategory.created_date = ''
        product_subcategory.created_user = self.request.user
        # product_subcategory.updated_date = ''
        product_subcategory.updated_user = self.request.user

        product_subcategory.save()

        self.product_subcategory_populate_list()

    def product_subcategory_populate_list(self,):
        try:
            product_subcategories = Subcategory.objects.values_list('name', flat=True).all().order_by('name')
            new_list = map(str, product_subcategories)
            self.product_subcategory_list = map(self.__lower_strip_subcategory__, new_list)
            # self.product_subcategory_list = [product_subcategory.lower().strip() for product_subcategory in new_list]
            del new_list
        except:
            print 'Data Bulk - Product SubCategory Populate List {0}'.format(sys.exc_info()[1])

        return self.product_subcategory_list

    def bulk(self):
        map(self.__row_creator__, self.data)
        # for row in self.data:
        #     category = str(row['category']).lower().strip()
        #     subcategory = str(row['sub_category']).lower().strip()
        #     if subcategory not in self.product_subcategory_list:
        #         self.product_subcategory_data(category=category, subcategory=subcategory)
                # print subcategory + ' inserted to subcategory list'

    def __row_creator__(self, row):
        category = str(row['category']).lower().strip()
        subcategory = str(row['sub_category']).lower().strip()
        if subcategory not in self.product_subcategory_list:
            self.product_subcategory_data(category=category, subcategory=subcategory)

    @staticmethod
    def __lower_strip_subcategory__(subcategory):
        return subcategory.lower().strip()
