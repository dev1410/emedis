import sys
from product.models import ProductSource


class ProductSourceData(object):
    __product_source_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_source_list(self):
        return self.__product_source_list

    @product_source_list.setter
    def product_source_list(self, value):
        self.__product_source_list = value

    def product_source_data(self,
                            source_images='',
                            source_attachments='',
                            source_attachment_titles=''):
        product_source = ProductSource()
        product_source.source_images = source_images
        product_source.source_attachments = source_attachments
        product_source.source_attachment_titles = source_attachment_titles

        product_source.created_user = self.request.user

        product_source.save()

    def bulk(self):
        map(self.__row_creator__, self.data)

    def __row_creator__(self, row):
        try:
            source_images = str(row['image_urls']).lower().strip()
            source_attachments = str(row['attachment_urls']).lower().strip()
            source_attachment_titles = str(row['attachment_titles']).lower().strip()

            self.product_source_data(source_images=source_images,
                                     source_attachments=source_attachments,
                                     source_attachment_titles=source_attachment_titles)
        except KeyError:
            pass

    @staticmethod
    def __lower_strip_source__(source):
        return source.lower().strip()
