import sys
import re
import functools
from product.models import Product
from product.brand.models import Brand
from category.models import Category
from category.subcategory.models import Subcategory
from product.models import ProductSupplier


class ProductData(object):
    __product_list = list()
    __data = None
    __request = None
    __forbiden_char = ['\/', '\&']

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_list(self):
        return self.__product_list

    @product_list.setter
    def product_list(self, value):
        self.__product_list = value

    def product_data(self,
                     product_name='',
                     product_number='',
                     product_type='',
                     product_registration='',
                     e_catalogue_registration='',
                     brand='',
                     category='',
                     subcategory='',
                     main_dealer='',):
        product = Product()
        product.product_name = str(product_name).title()
        product.product_number = product_number.upper()
        product.product_type = product_type.upper()
        product.product_registration = product_registration
        product.e_catalogue_registration = e_catalogue_registration.upper()
        product.brand = Brand.objects.get(name=brand.title())
        product.main_dealer = ProductSupplier.objects.get(name=main_dealer.title())
        # product.sub_dealer = ''
        product.is_active = True
        product.is_sale = False
        product.is_bestseller = False
        # product.created_date = ''
        product.created_user = self.request.user
        # product.updated_date = ''
        # product.updated_user = ''

        product.save()
        product.category.add(Category.objects.get(name=category))
        # product.category.add(Category.objects.get(name=category.title()))
        product.subcategory.add(Subcategory.objects.get(name=subcategory.title()))
        product.save()

        self.product_populate_list()

        return product

    def product_populate_list(self,):
        try:
            products = Product.objects.values_list('product_name', flat=True).all().order_by('product_name')
            new_list = map(str, products)
            self.product_list = map(self.__lower_strip_product__, new_list)
            # self.product_list = [product.lower().strip() for product in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Populate List {0}'.format(sys.exc_info()[1])

        return self.product_list

    def bulk(self):
        """
        :return products:
        :rtype products: :py:class:`product.models.Product`
        """
        row_count = len(self.data)
        current_row = 0

        products = map(
            functools.partial(
                self.__row_creator__,
                current_row=current_row),
            self.data)
        # for row in self.data:
        #     current_row += 1
        #     product_name = str(self.__repair_name(row['product_name'])).lower()
        #
        #     try:
        #         product_number = str(row['product_no']).lower().strip()
        #     except:
        #         product_number = ''
        #
        #     try:
        #         product_type = str(row['product_model']).lower().strip()
        #     except:
        #         pass
        #     try:
        #         e_catalogue_registration = str(row['product_registration']).lower().strip()
        #     except:
        #         pass
        #     brand = str(row['manufacture']).lower().strip()
        #     category = str(row['category']).lower().strip()
        #     subcategory = str(row['sub_category']).lower().strip()
        #     main_dealer = str(row['supplier']).lower().strip()
        #     if product_name not in self.product_list:
        #         self.product_data(product_name=product_name,
        #                           product_number=product_number,
        #                           product_type=product_type,
        #                           e_catalogue_registration=e_catalogue_registration,
        #                           brand=brand,
        #                           category=category,
        #                           subcategory=subcategory,
        #                           main_dealer=main_dealer,)
                # print '{0} of {1} {2} inserted to product list'.format(current_row, row_count, product_name)
        return products

    def __repair_name(self, name):
        return re.sub(
            r'|'.join(self.__forbiden_char), ' ', ' '.join([word.strip() for word in name.split()])
        ).strip()

    def __row_creator__(self, data, current_row):
        current_row += 1
        product_name = str(self.__repair_name(data['product_name'])).lower()

        if product_name not in self.product_list:

            product_number = ''
            if 'product_no' in data:
                product_number = str(data['product_no']).lower().strip()

            product_type = None
            if 'product_model' in data:
                product_type = str(data['product_model']).lower().strip()

            product_registration = None
            if 'product_registration' in data:
                product_registration = str(data['product_registration']).upper().strip()

            e_catalogue_registration = None
            if 'product_registration' in data:
                e_catalogue_registration = str(data['e_catalogue_registration']).lower().strip()

            brand = str(data['manufacture']).lower().strip()
            category = data['category'].lower().strip()
            subcategory = str(data['sub_category']).lower().strip()
            main_dealer = str(data['supplier']).lower().strip()

            return self.product_data(product_name=product_name,
                                     product_number=product_number,
                                     product_type=product_type,
                                     product_registration=product_registration,
                                     e_catalogue_registration=e_catalogue_registration,
                                     brand=brand,
                                     category=category,
                                     subcategory=subcategory,
                                     main_dealer=main_dealer,)

    @staticmethod
    def __lower_strip_product__(product):
        return product.lower().strip()
