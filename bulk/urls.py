from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^bulk/', views.bulk, name='bulk')
]
