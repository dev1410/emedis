import csv
import glob
import os
import shutil
import sys
import functools
from django.conf import settings

from bulk.product_images_data import ImageDumper
from product_brand_data import ProductBrandData
from product_supplier_data import ProductSupplierData
from product_currency_data import ProductCurrencyData
from product_country_of_origin_data import ProductCountryOfOriginData
from product_category_data import ProductCategoryData
from product_subcategory_data import ProductSubcategoryData
from product_data import ProductData
from product_detail_data import ProductDetailData
from product_spesification_and_certification_data import ProductSpecificationAndCertificationData
from product_metadata_data import ProductMetadataData
from product_source_data import ProductSourceData


class LKKPDumpToDatabase(object):
    __session_name = ''

    directory = os.path.join(settings.BASE_DIR, 'lkpp_data')
    csv_fullpath_filenames = list()
    backup_directory = os.path.join(directory, 'backup')
    data_directory = os.path.join(directory, 'data')
    rows = []
    append_row = rows.append
    new_list_lower = []
    __request = None

    def __init__(self, session_name, request):
        self.request = request
        try:
            if not os.path.exists(self.data_directory):
                os.makedirs(self.data_directory)
        except:
            pass

        try:
            if not os.path.exists(self.backup_directory):
                os.makedirs(self.backup_directory)
        except:
            pass

        new_list = []
        # products = Product.objects.values_list('product_name', flat=True).all().order_by('product_name')
        # new_list = map(str, products)
        # self.new_list_lower = [product_name.lower().strip() for product_name in new_list]
        # del new_list

        self.csv_fullpath_filenames = glob.glob(os.path.join(self.data_directory, '*.csv'))

        self.session_name = session_name

        self.dump_data_with_csv_headers()
        self.data_bulk()

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def session_name(self):
        return self.__session_name

    @session_name.setter
    def session_name(self, value):
        self.__session_name = value

    def move_complete_dumped_data(self, source, destination):
        try:
            shutil.move(source, destination)
        except:
            print sys.exc_info()[1]

    def dump_data(self):
        for csv_fullpath_filename in self.csv_fullpath_filenames:
            print csv_fullpath_filename
            csv_file = open(csv_fullpath_filename)
            csv_reader = csv.reader(csv_file,
                                    delimiter='\t',
                                    dialect='excel-tab',)

            for row in csv_reader:
                print row

            csv_file.close()

            # self.move_complete_dumped_data(csv_fullpath_filename, csv_fullpath_filename.replace(self.data_directory, self.backup_directory))

    def dump_data_with_csv_headers(self):
        map(self.__csv_reader__, self.csv_fullpath_filenames)
        # for csv_fullpath_filename in self.csv_fullpath_filenames:
        #     with open(csv_fullpath_filename, 'r') as csv_file:
        #         csv_dictreader = csv.DictReader(csv_file,delimiter='\t')
        #         csv_headers = csv_dictreader.fieldnames
        #
        #         for row in csv_dictreader:
        #             row_dictdata = dict()
        #             for csv_header in csv_headers:
        #                 row_dictdata[csv_header] = row[csv_header]
        #
        #             self.rows.append(row_dictdata)
        #
        #     csv_file.close()

        # import sys
        # sys.exit(0)

        # self.move_complete_dumped_data(csv_fullpath_filename, csv_fullpath_filename.replace(self.data_directory, self.backup_directory))

        return self.rows

    def data_bulk(self):
        product_brand_data = ProductBrandData(data=self.rows, request=self.request)
        product_brand_data.bulk()

        product_supplier_data = ProductSupplierData(data=self.rows, request=self.request)
        product_supplier_data.bulk()

        product_currency_data = ProductCurrencyData(data=self.rows, request=self.request)
        product_currency_data.bulk()

        product_country_of_origin_data = ProductCountryOfOriginData(data=self.rows, request=self.request)
        product_country_of_origin_data.bulk()

        product_category_data = ProductCategoryData(data=self.rows, request=self.request)
        product_category_data.bulk()

        product_subcategory_data = ProductSubcategoryData(data=self.rows, request=self.request)
        product_subcategory_data.bulk()

        product_data = ProductData(data=self.rows, request=self.request)
        products_data = product_data.bulk()

        product_detail_data = ProductDetailData(data=self.rows, request=self.request)
        product_detail_data.bulk()

        product_spesification_and_certification_data = ProductSpecificationAndCertificationData(data=self.rows, request=self.request)
        product_spesification_and_certification_data.bulk()

        product_metadata = ProductMetadataData(data=self.rows, request=self.request)
        product_metadata.bulk()

        product_source = ProductSourceData(data=self.rows, request=self.request)
        product_source.bulk()

        product_images_data = ImageDumper(data=self.rows, products=products_data)
        product_images_data.execute()

    def __csv_reader__(self, csv_file):
        with open(csv_file, 'r') as opened_csv_file:
            csv_dict_reader = csv.DictReader(opened_csv_file, delimiter='\t')
            csv_headers = csv_dict_reader.fieldnames

            map(
                functools.partial(
                    self.__row_reader__,
                    csv_headers=csv_headers),
                csv_dict_reader)

        opened_csv_file.close()

    def __row_reader__(self, csv_file, csv_headers):
        row_dict_data = dict()

        map(
            functools.partial(
                self.__pair_header__,
                row_dict_data=row_dict_data,
                csv_file=csv_file),
            csv_headers)

        self.append_row(row_dict_data)

    @staticmethod
    def __pair_header__(csv_header, row_dict_data, csv_file):
        row_dict_data[csv_header] = csv_file[csv_header]
