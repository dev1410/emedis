import sys
from category.models import Category
from django.template.defaultfilters import slugify


class ProductCategoryData(object):
    __product_category_list = list()
    __data = None
    __request = None

    def __init__(self, data=None, request=None):
        self.__data = data
        self.request = request

        self.product_category_populate_list()

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, value):
        self.__data = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @property
    def product_category_list(self):
        return self.__product_category_list

    @product_category_list.setter
    def product_category_list(self, value):
        self.__product_category_list = value

    def product_category_data(self,
                              category=''):
        product_category = Category()
        product_category.name = str(category).title()
        # product_category.sort_order = ''
        product_category.category_help_text = ''
        product_category.description = ''
        # product_category.category_icon = ''
        product_category.slug = slugify(product_category.name)
        product_category.is_active = True
        # product_category.created_date = ''
        product_category.created_user = self.request.user
        # product_category.updated_date = ''
        # product_category.updated_user = ''

        product_category.save()

        self.product_category_populate_list()

    def product_category_populate_list(self,):
        try:
            product_categories = Category.objects.values_list('name', flat=True).all().order_by('name')
            new_list = map(lambda string: string.encode('utf-8'), product_categories)
            print new_list
            self.product_category_list = map(self.__low_strip_product_category__, new_list)
            # self.product_category_list = [product_category.lower().strip() for product_category in new_list]
            del new_list
        except:
            print 'Data Bulk - Product Category Populate List {0}'.format(sys.exc_info()[1])

        return self.product_category_list

    def bulk(self):
        map(self.__row_creator__, self.data)
        # for row in self.data:
        #     category = str(row['category']).lower().strip()
        #     if category not in self.product_category_list:
        #         self.product_category_data(category=category)
                # print category + ' inserted to category list'

    def __row_creator__(self, row):
        category = str(row['category']).lower().strip()
        if category not in self.product_category_list:
            self.product_category_data(category=category)

    @staticmethod
    def __low_strip_product_category__(product_category):
        return product_category.lower().strip()
