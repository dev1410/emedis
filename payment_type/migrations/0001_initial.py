# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='PaymentType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(help_text=b'Payment Type Name', max_length=50)),
                ('api_ready', models.BooleanField(default=False, help_text=b'Have API Integration')),
                ('contact_person_name', models.CharField(help_text=b'Contact Person Name', max_length=50, null=True, blank=True)),
                ('contact_person_email', models.CharField(help_text=b'Contact Person Email', max_length=255, null=True, blank=True)),
                ('description', models.TextField(help_text=b'Payment Type Description', max_length=1024, null=True, blank=True)),
                ('starting_date', models.DateTimeField(help_text=b'Payment Type Start')),
                ('cash_support', models.BooleanField(default=False, help_text=b'Cash Payment Support')),
                ('credit_support', models.BooleanField(default=False, help_text=b'Credit Payment Support')),
                ('manual_payemnt_support', models.BooleanField(default=False, help_text=b'Credit  Payment Support')),
                ('status', models.BooleanField(default=False, help_text=b'Payment Type Status')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='payment_type_created_user', to=settings.AUTH_USER_MODEL)),
                ('updated_user', models.ForeignKey(related_name='payment_type_updated_user', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Payment Type',
                'verbose_name_plural': 'Payment Types',
            },
        ),
    ]
