from django.contrib.auth.models import User
from django.db.models import Model, BooleanField, CharField, DateTimeField, ForeignKey, TextField


class PaymentType(Model):
    class Meta:
        verbose_name = 'Payment Type'
        verbose_name_plural = 'Payment Types'

    name = CharField(max_length=50,
                     null=False,
                     blank=False,
                     help_text='Payment Type Name')
    api_ready = BooleanField(default=False,
                             help_text='Have API Integration')
    contact_person_name = CharField(max_length=50,
                                    help_text='Contact Person Name',
                                    blank=True,
                                    null=True,)
    contact_person_email = CharField(max_length=255,
                                     help_text='Contact Person Email',
                                     blank=True,
                                     null=True,)
    description = TextField(max_length=1024,
                            help_text='Payment Type Description',
                            blank=True,
                            null=True,)
    starting_date = DateTimeField(help_text='Payment Type Start',
                                  blank=False,
                                  null=False)
    cash_support = BooleanField(default=False,
                                help_text='Cash Payment Support')
    credit_support = BooleanField(default=False,
                                  help_text='Credit Payment Support')
    manual_payemnt_support = BooleanField(default=False,
                                          help_text='Credit  Payment Support')
    status = BooleanField(default=False,
                          help_text='Payment Type Status')
    created_date = DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='payment_type_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, related_name='payment_type_updated_user', null=True)

    def __unicode__(self):
        return self.name
