from django.contrib import admin
from payment_type.models import PaymentType


class PaymentTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'api_ready', 'contact_person_name', 'contact_person_email', 'description', 'starting_date', 'cash_support', 'credit_support', 'manual_payemnt_support', 'status',)

    list_filter = ('created_date', 'updated_date')

    search_fields = ['name']

    fieldsets = [('General Information',
                  {'fields':
                       ['name', 'api_ready', 'contact_person_name', 'contact_person_email', 'description', 'starting_date', 'cash_support', 'credit_support', 'manual_payemnt_support', 'status',]
                   }
                  )
                 ]

    def save_model(self, request, obj, form, change):
        if PaymentType.objects.filter(slug=request.POST['name']).exists():
            obj.updated_user = request.user
            obj.save()
        else :
            obj.created_user = request.user
            obj.save()

admin.site.register(PaymentType, PaymentTypeAdmin)
