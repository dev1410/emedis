"""emedis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
import settings
from mailer import views
import debug_toolbar
from django.conf.urls.static import static

if settings.DEBUG:
    website_url_prefix = '^'
else:
    website_url_prefix = ''

urlpatterns = [
    url(r'^admin123-123admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^mailer', include('mailer.urls')),
    url(r'^chaining/', include('smart_selects.urls')),
    url(r'^data', include('search.urls')),
    url(r'^subscribe/', views.subscriber),
    url(r'^documents/', include('documents.urls')),
    url(r'' + website_url_prefix + settings.ADDITIONAL_URL_PATH, include('website.urls')),
    url(r'', include('website.urls')),
    url(r'', include('cms.urls')),
    url(r'', include('email_template.urls')),
    url(r'^data_bulk/', include('bulk.urls')),
]

# with no worry, these code only works if DEBUG = True
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_ASSETS_URL, document_root=settings.STATIC_ASSETS_ROOT)
urlpatterns += static(settings.GOOGLE_APPS_VERIFY_URL, document_root=settings.GOOGLE_APPS_VERIFY_ROOT)

if settings.DEBUG:
    # django-debug-toolbar url
    urlpatterns += patterns(url(r'^__debug__/', include(debug_toolbar.urls)),)

admin.site.site_header = settings.ADMIN_SITE_HEADER
