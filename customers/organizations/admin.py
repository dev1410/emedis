from django.contrib import admin
from models import Organization


class OrganizationAdmin(admin.ModelAdmin):
    list_display = (
        'model', 'description', 'created_date', 'updated_date',)

    fieldsets = [
        ('General Information',
         {'fields':
          ['model', 'description',]
          })
    ]

    list_filter = ('created_date', 'updated_date')

    def save_model(self, request, obj, form, change):
        if Organization.objects.filter(model=request.POST['model']).exists():
            obj.updated_user = request.user
            obj.save()
        else:
            obj.created_user = request.user
            obj.save()
