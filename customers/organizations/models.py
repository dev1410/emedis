from django.db import models
from django.contrib.auth.models import User


class Organization(models.Model):
    model = models.CharField(max_length=30,
                             null=False,
                             blank=False,
                             unique=True,)
    description = models.TextField(null=False,
                                blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(User,
                                     related_name='originzation_admin_user')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(User, null=True)

    class Meta:
        verbose_name_plural = 'Organizations'

    def __unicode__(self):
        return self.model