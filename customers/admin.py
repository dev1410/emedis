from django.contrib import admin
from customers.models import Customer
from customers.organizations.models import Organization
from customers.organizations.admin import OrganizationAdmin
from customers.detail.models import CustomerDetail
import uuid


class CustomerDetailAdmin(admin.StackedInline):
    model = CustomerDetail


class CustomerAdmin(admin.ModelAdmin):
    list_display = (
        'pic_name',
        'email',
        'phone',)

    fieldsets = [
        ('General Information',
         {'fields': [
             'pic_name', 'email', 'phone', 'password', 'is_active', 'activation_code', ]
         },),
    ]

    inlines = [
        CustomerDetailAdmin,
    ]

    list_filter = ('pic_name', 'email', 'phone')

    # def save_model(self, request, obj, form, change):
    #     if obj.activation_code is None:
    #         obj.activation_code = str(uuid.uuid4()).replace('-', '')
    #         obj.save()

admin.site.register(Customer, CustomerAdmin)
admin.site.register(Organization, OrganizationAdmin)
