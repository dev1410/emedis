# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0011_district_village'),
        ('customers', '0002_auto_20160328_1540'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='customer',
            name='villages',
        ),
        migrations.AddField(
            model_name='customer',
            name='village',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'Village', related_name='village_district', default='', verbose_name=b'Kelurahan', to='geographicals.District', chained_field=b'village'),
            preserve_default=False,
        ),
    ]
