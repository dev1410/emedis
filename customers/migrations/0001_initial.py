# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('geographicals', '0011_district_village'),
    ]

    operations = [
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pic_name', models.CharField(max_length=35, verbose_name=b'Name (PIC)')),
                ('email', models.CharField(max_length=50, verbose_name=b'Email')),
                ('password', models.CharField(max_length=128, verbose_name=b'Password')),
                ('organization_name', models.CharField(max_length=50, verbose_name=b'Organization Name', blank=True)),
                ('position', models.CharField(max_length=30, verbose_name=b'Position')),
                ('address', models.CharField(max_length=50, verbose_name=b'Address')),
                ('address_2', models.CharField(max_length=50, null=True, verbose_name=b'Address 2', blank=True)),
                ('phone', models.CharField(max_length=20, null=True, verbose_name=b'Phone', blank=True)),
                ('mobile', models.CharField(max_length=20, verbose_name=b'Mobile')),
                ('fax', models.CharField(max_length=20, null=True, verbose_name=b'Fax', blank=True)),
                ('active_cart_id', models.IntegerField(null=True, verbose_name=b'Active Cart', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model', models.CharField(max_length=30)),
                ('description', models.TextField(blank=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='originzation_admin_user', to=settings.AUTH_USER_MODEL)),
                ('updated_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Organizations',
            },
        ),
        migrations.AddField(
            model_name='customer',
            name='organization',
            field=models.ForeignKey(to='customers.Organization'),
        ),
        migrations.AddField(
            model_name='customer',
            name='villages',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'District', related_name='village_district', chained_field=b'district', verbose_name=b'Kelurahan', to='geographicals.District'),
        ),
    ]
