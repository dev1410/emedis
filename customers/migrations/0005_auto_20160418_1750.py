# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0011_district_village'),
        ('customers', '0004_auto_20160328_1633'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('organization_name', models.CharField(max_length=50, verbose_name=b'Organization Name', blank=True)),
                ('position', models.CharField(max_length=30, verbose_name=b'Position')),
                ('address', models.CharField(max_length=50, verbose_name=b'Address')),
                ('address_2', models.CharField(max_length=50, null=True, verbose_name=b'Address 2', blank=True)),
                ('mobile', models.CharField(max_length=20, verbose_name=b'Mobile')),
                ('additional_contact', models.CharField(max_length=20, null=True, verbose_name=b'Fax', blank=True)),
                ('active_cart_id', models.IntegerField(null=True, verbose_name=b'Active Cart', blank=True)),
            ],
        ),
        migrations.RemoveField(
            model_name='customer',
            name='active_cart_id',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='address',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='address_2',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='fax',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='mobile',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='organization',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='organization_name',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='position',
        ),
        migrations.RemoveField(
            model_name='customer',
            name='village',
        ),
        migrations.AddField(
            model_name='customerdetail',
            name='customer',
            field=models.ForeignKey(to='customers.Customer'),
        ),
        migrations.AddField(
            model_name='customerdetail',
            name='organization',
            field=models.ForeignKey(to='customers.Organization'),
        ),
        migrations.AddField(
            model_name='customerdetail',
            name='village',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'Village', related_name='village_district', chained_field=b'village', verbose_name=b'Kelurahan', to='geographicals.Village'),
        ),
    ]
