# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0005_auto_20160418_1750'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='phone',
            field=models.CharField(default='', max_length=20, verbose_name=b'Phone'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='customerdetail',
            name='customer',
            field=models.OneToOneField(to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='customerdetail',
            name='mobile',
            field=models.CharField(max_length=20, null=True, verbose_name=b'Mobile', blank=True),
        ),
        migrations.AlterField(
            model_name='customerdetail',
            name='position',
            field=models.CharField(max_length=30, null=True, verbose_name=b'Position', blank=True),
        ),
    ]
