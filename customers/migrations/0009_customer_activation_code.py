# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0008_auto_20160907_1055'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='activation_code',
            field=models.CharField(default='', max_length=64, verbose_name=b'Registration activation email'),
            preserve_default=False,
        ),
    ]
