# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0011_auto_20161011_2210'),
    ]

    operations = [
        migrations.AddField(
            model_name='customerdetail',
            name='zip_code',
            field=models.CharField(default='', max_length=7, verbose_name=b'Zipcode'),
            preserve_default=False,
        ),
    ]
