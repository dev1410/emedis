# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0007_auto_20160419_1637'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='activation_date',
            field=models.DateTimeField(null=True, verbose_name=b'Customer activation datetime', blank=True),
        ),
    ]
