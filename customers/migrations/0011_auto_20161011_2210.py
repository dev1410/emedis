# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0010_customer_cart_session'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='cart_session',
            field=models.ForeignKey(blank=True, to='cart.Cart', null=True),
        ),
    ]
