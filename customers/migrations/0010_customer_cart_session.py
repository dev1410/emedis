# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0007_merge'),
        ('customers', '0009_customer_activation_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='cart_session',
            field=models.ForeignKey(default='', to='cart.Cart'),
            preserve_default=False,
        ),
    ]
