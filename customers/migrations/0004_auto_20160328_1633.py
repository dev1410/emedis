# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import smart_selects.db_fields


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20160328_1631'),
    ]

    operations = [
        migrations.AlterField(
            model_name='customer',
            name='village',
            field=smart_selects.db_fields.ChainedForeignKey(chained_model_field=b'Village', related_name='village_district', chained_field=b'village', verbose_name=b'Kelurahan', to='geographicals.Village'),
        ),
    ]
