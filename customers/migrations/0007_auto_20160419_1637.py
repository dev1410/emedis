# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0006_auto_20160418_1802'),
    ]

    operations = [
        migrations.AddField(
            model_name='customer',
            name='activation_date',
            field=models.BooleanField(default='', verbose_name=b'Customer activation datetime'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='customer',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'Customer Active'),
        ),
    ]
