from django.db import models
from ..organizations.models import Organization
from geographicals.villages.models import Village
from customers.models import Customer
from smart_selects.db_fields import ChainedForeignKey


class CustomerDetail(models.Model):
    customer = models.OneToOneField(Customer)
    organization = models.ForeignKey(Organization)
    organization_name = models.CharField(max_length=50,
                                         null=False,
                                         blank=True,
                                         verbose_name='Organization Name')
    position = models.CharField(max_length=30,
                                null=True,
                                blank=True,
                                verbose_name='Position')
    address = models.CharField(max_length=50,
                               null=False,
                               blank=False,
                               verbose_name='Address')
    address_2 = models.CharField(max_length=50,
                                 null=True,
                                 blank=True,
                                 verbose_name='Address 2')
    village = ChainedForeignKey(
        Village,
        chained_field='village',
        chained_model_field='Village',
        related_name='village_district',
        null=False,
        blank=False,
        verbose_name='Kelurahan',
    )
    mobile = models.CharField(max_length=20,
                              null=True,
                              blank=True,
                              verbose_name='Mobile')
    additional_contact = models.CharField(max_length=20,
                                          null=True,
                                          blank=True,
                                          verbose_name='Fax')
    active_cart_id = models.IntegerField(null=True,
                                         blank=True,
                                         verbose_name='Active Cart')
    zip_code = models.CharField(max_length=7,
                                null=False,
                                blank=False,
                                verbose_name='Zipcode')

    @staticmethod
    def authenticate(email, password):
        customer = None
        try:
            customer = Customer.objects.get(email=email, password=password)
        except Customer.DoesNotExist:
            pass

        return customer
