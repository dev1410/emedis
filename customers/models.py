from django.db import models
from django.db.models import BooleanField, CharField, DateTimeField, ForeignKey
from django.utils import timezone
from cart.models import Cart


class Customer(models.Model):
    pic_name = CharField(max_length=35,
                         null=False,
                         blank=False,
                         verbose_name='Name (PIC)')
    email = CharField(max_length=50,
                      null=False,
                      blank=False,
                      verbose_name='Email')
    phone = CharField(max_length=20,
                      null=False,
                      blank=False,
                      verbose_name='Phone')
    password = CharField(max_length=128,
                         null=False,
                         blank=False,
                         verbose_name='Password')
    is_active = BooleanField(default=False,
                             verbose_name='Customer Active')
    activation_date = DateTimeField(blank=True,
                                    null=True,
                                    verbose_name='Customer activation datetime')
    activation_code = CharField(max_length=64,
                                blank=False,
                                null=False,
                                verbose_name='Registration activation email')
    cart_session = ForeignKey(Cart,
                              blank=True,
                              null=True,)

    @staticmethod
    def account_activation(email, activation_code):
        result = False

        try:
            customer = Customer.objects.get(email=email,
                                            activation_code=activation_code)
            customer.is_active = True
            customer.activation_date = timezone.now()
            customer.save()

            result = True
        except Customer.DoesNotExist:
            pass

        return result

    @staticmethod
    def authenticate(email, password):
        customer = None
        try:
            customer = Customer.objects.get(email=email, password=password)
        except Customer.DoesNotExist:
            pass

        return customer

    @staticmethod
    def availability(email):
        result = False

        try:
            customer = Customer.objects.get(email=email)
            result = True
        except Customer.DoesNotExist:
            pass

        return result
