import os

from django.conf import settings
import time


def save(content, full_path_filename=None):
    """

    Save content to file

    :param content: string of content
    :param full_path_filename: full path

    Success:
        return file name
    Fail:
        return None
    """
    result = None

    if full_path_filename is None:
        filename = str(int(round(time.time() * 1000)))

        full_path_filename =  os.path.join(os.path.join(settings.BASE_DIR, settings.TMP_FOLDER), filename)
        print full_path_filename
    try:
        file = open(full_path_filename, "w")
        file.write(content)
        file.close()

        result = full_path_filename
    except:
        None

    return result


def delete(full_path_filename):
    result = False

    try:
        os.remove(full_path_filename)

        result = True
    except:
        None

    return result
