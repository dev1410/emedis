from customers.detail.models import Customer, CustomerDetail


class CustomerObject(object):

    def __init__(self, customer, verbose=False):

        if not verbose:
            customer = Customer.objects.get(pk=int(customer))

        customer_detail = CustomerDetail.objects.get(customer=customer)

        self.id = customer.id
        self.pic_name = customer.pic_name
        self.email = customer.email
        self.phone = customer.phone
        self.address = customer_detail.address
        self.address_2 = customer_detail.address_2
        self.village = customer_detail.village
        self.mobile = customer_detail.mobile
        self.additional_contact = customer_detail.additional_contact


class AutoAssignObject(object):
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
