@staticmethod
def try_parse(value=None, default_value=None):
    result = value

    if value is None:
        result = default_value

    return value


@staticmethod
def check_parse(value, condition, return_value):
    result = value

    if value == condition:
        result = return_value

    return result

