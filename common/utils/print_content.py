from PyQt4.QtGui import QApplication, QTextDocument, QPrinter
import sys

def output_to_pdf(content, filename):
    application = QApplication(sys.argv)

    document = QTextDocument()
    document.setHtml(content)

    printer = QPrinter()
    printer.setOutputFileName(filename)
    printer.setOutputFormat(QPrinter.PdfFormat)
    printer.setPageSize(QPrinter.A4)
    printer.setPageMargins(15, 15, 15, 15, QPrinter.Millimeter)

    document.print_(printer)