class AutoViviDict(dict):
    def __missing__(self, key):
        new_dict = self[key] = type(self)()
        return new_dict
