from django.conf import settings
from django.utils import timezone
from customers.models import Customer
from cart.models import Cart
from cart.cart_details.models import CartDetail
from product.models import Product
from authentication_sessions.models import AuthenticationSessions
from common.utils.guid import create_guid
import datetime
import hashlib


class Authentication(object):
    __session_key = ''
    __request = ''

    def __init__(self, request):
        self.session_key = request.session.session_key
        self.request = request

    @property
    def session_key(self):
        return self.__session_key

    @session_key.setter
    def session_key(self, value):
        self.__session_key = value

    @property
    def request(self):
        return self.__request

    @request.setter
    def request(self, value):
        self.__request = value

    @staticmethod
    def get_client_ip_address(request):
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        if x_forwarded_for:
            ip_address = x_forwarded_for.split(',')[0]
        else:
            ip_address = request.META.get('REMOTE_ADDR')

        return ip_address

    def login(self, email_address, password):
        customer = Customer.authenticate(email=email_address,
                                         password=Authentication.password_hash(password=password))

        if customer is not None:
            authentication_session_data = self.set_authentication_session(customer=customer,)
            self.request.session['authentication_session_data'] = authentication_session_data

        return customer

    def change_password(self, email_address, current_password, new_password):
        customer = None
        try:
            customer = Customer.authenticate(email=email_address, password=Authentication.password_hash(password=current_password))
            authentication_session = AuthenticationSessions.objects.get(session_key=self.session_key, ip_address=Authentication.get_client_ip_address(request=self.request),)
            customer.password = Authentication.password_hash(password=new_password)
            self.set_authentication_session(customer=customer,)

        except Customer.DoesNotExist:
            pass
        except AuthenticationSessions.DoesNotExist:
            pass

        return customer

    def forget_password(self, email_address, new_password):
        pass

    def set_authentication_session(self, customer):
        result = dict()
        session_data = dict()
        session_data['customer_id'] = customer.id

        now = timezone.now()

        try:
            authentication_sessions = AuthenticationSessions()
            authentication_sessions.session_key = self.get_current_session(self.request)
            authentication_sessions.ip_address = Authentication.get_client_ip_address(request=self.request)
            authentication_sessions.session_data = session_data
            authentication_sessions.expire_date = str(now + datetime.timedelta(days=14))
            authentication_sessions.last_login = str(now)
            authentication_sessions.save()

            result['session_key'] = self.get_current_session(self.request)
            result['ip_address'] = authentication_sessions.ip_address
            result['session_data'] = session_data
            result['session_date'] = str(now)
            result['expire_date'] = str(now + datetime.timedelta(days=14))
            result['last_login'] = str(now)
            try:
                cart = Cart.objects.get(id=customer.cart_session_id)
                result['cart_session_id'] = cart.id
                result['cart_session'] = cart.cart_session
            except Cart.DoesNotExist:
                xxx
                result['cart_session_id'] = 0
                result['cart_session'] = ''

            self.request.session['authentication_session_data'] = result
        except:
            pass

        return result
 
    @staticmethod
    def get_authentication_session(request):
        result = None

        if request.session.exists(request.session.session_key):
            try:
                result = AuthenticationSessions.objects.get(session_key=request.session.session_key,
                                                            ip_address=Authentication.get_client_ip_address(request=request),
                                                            expire_date__gte=timezone.now(),)
            except AuthenticationSessions.DoesNotExist:
                pass

        return result

    @staticmethod
    def get_current_session(request):
        if not request.session.exists(request.session.session_key):
            request.session.create()

        return request.session.session_key

    @staticmethod
    def password_hash(password):
        password_hash = hashlib.sha1(settings.SECRET_KEY + ' ' + password)
        hash_digest = password_hash.hexdigest()

        return hash_digest

    @staticmethod
    def get_cart_session(request):
        if 'cart_session' in request.COOKIES:
            cart_session = request.COOKIES.get('cart_session')
        else:
            try:
                customer = Customer.objects.get(id=Authentication.get_customer_id(request))
                cart_session = customer.cart_session.cart_session
            except Customer.DoesNotExist:
                cart_session = Authentication.get_new_cart_session()

        return cart_session

    @staticmethod
    def get_new_cart_session():
        return create_guid(prefix='ct')

    @staticmethod
    def get_current_customer_cart_session(request):
        try:
            customer = Customer.objects.get(id=Authentication.get_customer_id(request))
        except Customer.DoesNotExist:
            return None

        return customer.cart_session.cart_session

    @staticmethod
    def merge_cart(request):
        result = False

        try:
            customer_cart_session = str(Authentication.get_current_customer_cart_session(request) if Authentication.get_current_customer_cart_session(request) != None else '')
            cart_session = str(Authentication.get_cart_session(request))

            if customer_cart_session != '' and customer_cart_session != cart_session:
                cart = Cart.objects.get(cart_session=cart_session)
                cart_details = CartDetail.objects.filter(cart=cart)

                if cart_details.__len__() > 0:
                    customer_cart = Cart.objects.get(cart_session=customer_cart_session)
                    customer_cart_details = CartDetail.objects.filter(cart=customer_cart)

                    customer_cart_product_ids = list()
                    for customer_cart_detail in customer_cart_details:
                        customer_cart_product_ids.append(customer_cart_detail.product.id)

                    for cart_detail in cart_details:
                        if cart_detail.product.id not in customer_cart_product_ids:
                            cart_detail.cart = customer_cart
                            cart_detail.save()
                        else:
                            product = Product.objects.get(id=cart_detail.product.id)
                            customer_cart_detail = CartDetail.objects.get(cart=customer_cart, product=product)
                            customer_cart_detail.product_qty += cart_detail.product_qty
                            customer_cart_detail.save()
            elif customer_cart_session != '':
                cart = Cart.objects.get(cart_session=cart_session)

                customer = Customer.objects.get(id=Authentication.get_customer_id(request))
                customer.cart_session = cart.cart_session
                customer.save()

            result = True
        except:
            pass

        return result

    @staticmethod
    def signout_customer(request):
        if request.COOKIES.has_key('session'):
            del request.COOKIES['session']

        if request.session.has_key('session'):
            del request.session['session']

        if request.session.has_key('authentication_session_data'):
            del request.session['authentication_session_data']

        if request.COOKIES.has_key('cart_session'):
            del request.COOKIES['cart_session']

        cart_session = Authentication.get_new_cart_session()

        return cart_session

    @staticmethod
    def get_customer_id(request):
        if 'authentication_session_data' in request.session:
            print request.session['authentication_session_data']
            customer_id = request.session['authentication_session_data']['session_data']['customer_id']
        else:
            customer_id = 0

        return customer_id
