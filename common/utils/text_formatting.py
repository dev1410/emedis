class TextFormatting(object):
    def __init__(self):
        pass

    @staticmethod
    def text_ellipsis(text, max_length, ellipsis_chars):
        result = (text[:max_length - len(ellipsis_chars)] + '...') if len(text) > max_length else '{text:{max_length}}'.format(text=text, max_length=max_length).replace(' ', '&nbsp;')

        return result

    @staticmethod
    def price_formatting(price):
        result = dict()
        result['price'] = 'Request for quote'
        result['thousands'] = ''

        try:
            if price >= 1000:
                price, thousands = divmod(price, 1000)
                result['price'] = '{:,}'.format(int(price))
                result['thousands'] = '.' + str(format(int(thousands), '03'))
            elif 1000 > price > 0:
                result['price'] = price
        except:
            pass

        return result

    @staticmethod
    def int_price_formatting(price):
        result = ''

        try:
            result = '{:,}'.format(int(price))
        except:
            pass

        return result

