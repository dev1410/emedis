import uuid


def create_guid(prefix=None):
    guid_prefix = ''

    if prefix is not None:
        guid_prefix = prefix

    guid = guid_prefix + str(uuid.uuid4())

    return guid
