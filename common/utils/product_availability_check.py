from product.models import Product


class ProductAvailabilityCheck(object):
    __product_field_name = ''
    __product_list = []

    def __init__(self, product_field_name=None):
        """
        Product Availability Check

        :param product_field_name:
         Default value      product_name

        """
        if product_field_name is None:
            self.product_field_name = 'product_name'
        else:
            self.product_field_name = product_field_name

    @property
    def product_field_name(self):
        return self.__product_field_name

    @product_field_name.setter
    def product_field_name(self, value):
        self.__product_field_name = value

    @property
    def product_list(self):
        return self.__product_list

    @product_list.setter
    def product_list(self, value):
        self.__product_list = value
