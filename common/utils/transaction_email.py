from django.template import Template, Context
from email_template.models import Body
from django.conf import settings
from django.core.mail import EmailMessage


class TransactionEmail(object):
    __to = ''
    __email_template = ''

    def __init__(self, to, email_template):
        self.to = to
        self.email_template = email_template

    @property
    def to(self):
        return self.__to

    @to.setter
    def to(self, value):
        self.__to = value

    @property
    def email_template(self):
        return self.__email_template

    @email_template.setter
    def email_template(self, value):
        self.__email_template = value

    def send_email(self, context):
        result = False

        email_template = Body.get_body(slug=self.email_template)

        if email_template is not None:
            customer_object = context['customer']
            recipients = []
            recipients.append(customer_object.email)

            template = Template(email_template).render(Context(context))

            email_message = EmailMessage(subject=context['subject'],
                                         body=template,
                                         from_email=settings.DEFAULT_FROM_EMAIL,
                                         to=recipients,)
            email_message.content_subtype = 'html'
            email_message.send()

            result = True

        return result
