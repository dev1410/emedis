from django.conf import settings
from django.core.cache import cache
from django.template import loader
from htmlmin.minify import html_minify
import hashlib
import os


class TemplateLoader:
    def __init__(self):
        pass

    @staticmethod
    def get_simple_template(template):
        hashing = hashlib.md5()
        hashing.update(template)

        response = cache.get('tpl-' + hashing.hexdigest())

        if response is None:
            template_dirs = settings.TEMPLATES[0].get('DIRS')

            for template_dir in template_dirs:
                if os.path.exists(os.path.join(template_dir, template)):
                    print template_dir
                    try:
                        template_file = file(os.path.join(template_dir, template), mode='r')
                        response = html_minify(template_file.read(), ignore_comments=True)
                        print response
                        cache.set('tpl-' + hashing.hexdigest(), response, settings.CACHE_TIME)
                        break
                    except IOError:
                        pass

        return response

    @staticmethod
    def get_template(template):
        hashing = hashlib.md5()
        hashing.update(template)

        response = cache.get('tpl-' + hashing.hexdigest())

        if response is None:
            try:
                template_string = loader.g(template)
                print template_string
                # response = html_minify(template_string, ignore_comments=True)
                response = template_string
                cache.set('tpl-' + hashing.hexdigest(), response, settings.CACHE_TIME)
            except IOError:
                pass

        return response
