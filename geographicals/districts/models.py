from django.db import models
from ..city.models import City

class District(models.Model):
    class Meta:
        db_table = 'geographical_districts'
        verbose_name = 'District'
        verbose_name_plural = 'Districts'

    name = models.CharField(max_length=50,
                            null=False,
                            blank=False)
    city = models.ForeignKey(City)
    description = models.TextField(null=False,
                                   blank=True)

    def __str__(self):
        return "{0} - {1}".format(self.city, self.name)