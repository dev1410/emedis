from django.contrib import admin


class DistrictAdmin(admin.ModelAdmin):
    list_display = ('name', 'city', )

    fieldsets = (
        ('Name', {'fields': ('name', 'city')}),
        ('Description', {'fields': ('description',)}),
    )
