from django.db import models
from ..country.models import Country

class City(models.Model):
    class Meta:
        db_table = 'geographical_cities'
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    name = models.CharField(max_length=50,
                            null=False,
                            blank=False)
    iso_prefix = models.CharField(max_length=2,
                                  null=False,
                                  blank=False)
    country = models.ForeignKey(Country)
    description = models.TextField(null=False,
                                   blank=True)

    def __str__(self):
        return "{0} - {1}".format(self.iso_prefix, self.name)