from django.contrib import admin

class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'iso_prefix', 'country', )

    fieldsets = (
        ('Name', {'fields': ('name', 'iso_prefix', 'country')}),
        ('Description', {'fields': ('description',)}),
    )