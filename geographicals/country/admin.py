from django.contrib import admin

class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'iso_prefix', 'continent', )

    fieldsets = (
        ('Name', {'fields': ('name', 'iso_prefix', 'continent')}),
        ('Description', {'fields': ('description',)}),
    )