from django.db import models
from ..continent.models import Continent


class Country(models.Model):
    class Meta:
        db_table = 'geographical_countries'
        verbose_name = 'Country'
        verbose_name_plural = 'Countries'

    name = models.CharField(max_length=50,
                            null=False,
                            blank=False)
    iso_prefix = models.CharField(max_length=2,
                                  null=False,
                                  blank=False)
    continent = models.ForeignKey(Continent)
    description = models.TextField(null=False,
                                   blank=True)

    def __str__(self):
        return "{0} - {1}".format(self.iso_prefix, self.name)