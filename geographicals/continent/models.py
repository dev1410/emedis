from django.db import models


class Continent(models.Model):
    class Meta:
        db_table = 'geographical_continents'

    name = models.CharField(max_length=50,
                            null=False,
                            blank=False)
    description = models.TextField(null=False,
                                   blank=True)

    def __str__(self):
        return self.name
