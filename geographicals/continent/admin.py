from django.contrib import admin


class ContinentAdmin(admin.ModelAdmin):
    list_display = ('name',)

    fieldsets = (
        ('Name', {'fields': ('name',)}),
        ('Description', {'fields': ('description',)}),
    )
