from django.contrib import admin
from continent.models import Continent
from continent.admin import ContinentAdmin
from country.models import Country
from country.admin import CountryAdmin
from geographicals.city.admin import CityAdmin
from geographicals.city.models import City
from geographicals.districts.models import District
from geographicals.districts.admin import DistrictAdmin
from geographicals.villages.models import Village
from geographicals.villages.admin import VillageAdmin


admin.site.register(Continent, ContinentAdmin)
admin.site.register(Country, CountryAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(District, DistrictAdmin)
admin.site.register(Village, VillageAdmin)
