from django.db import models

class Geographical(models.Model):
    class Meta:
        db_table = 'geographicals'

    def __str__(self):
        return self.name