from django.contrib import admin


class VillageAdmin(admin.ModelAdmin):
    list_display = ('name', 'zip_code', 'district', )

    fieldsets = (
        ('Name', {'fields': ('name', 'zip_code', 'district')}),
        ('Description', {'fields': ('description',)}),
    )
