from django.db import models
from ..districts.models import District

class Village(models.Model):
    class Meta:
        db_table = 'geographical_villages'
        verbose_name = 'Village'
        verbose_name_plural = 'Villages'

    name = models.CharField(max_length=50,
                            null=False,
                            blank=False)
    zip_code = models.CharField(max_length=7,
                                null=False,
                                blank=False)
    district = models.ForeignKey(District)
    description = models.TextField(null=False,
                                   blank=True)

    def __str__(self):
        return "{0} - {1}".format(self.name, self.zip_code)