# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0009_auto_20160207_1915'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('iso_prefix', models.CharField(max_length=2)),
                ('description', models.TextField(blank=True)),
                ('country', models.ForeignKey(to='geographicals.Country')),
            ],
            options={
                'db_table': 'geographical_cities',
                'verbose_name': 'City',
                'verbose_name_plural': 'Cities',
            },
        ),
    ]
