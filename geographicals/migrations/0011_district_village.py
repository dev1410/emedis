# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0010_city'),
    ]

    operations = [
        migrations.CreateModel(
            name='District',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True)),
                ('city', models.ForeignKey(to='geographicals.City')),
            ],
            options={
                'db_table': 'geographical_districts',
                'verbose_name': 'District',
                'verbose_name_plural': 'Districts',
            },
        ),
        migrations.CreateModel(
            name='Village',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('zip_code', models.CharField(max_length=7)),
                ('description', models.TextField(blank=True)),
                ('district', models.ForeignKey(to='geographicals.District')),
            ],
            options={
                'db_table': 'geographical_villages',
                'verbose_name': 'Village',
                'verbose_name_plural': 'Villages',
            },
        ),
    ]
