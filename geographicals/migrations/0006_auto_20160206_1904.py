# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0005_auto_20160206_1902'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='continent',
            table='continents',
        ),
    ]
