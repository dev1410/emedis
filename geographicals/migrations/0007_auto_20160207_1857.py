# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0006_auto_20160206_1904'),
    ]

    operations = [
        migrations.CreateModel(
            name='Geographical',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
                'db_table': 'geographicals',
            },
        ),
        migrations.AlterModelTable(
            name='continent',
            table='geographical_continents',
        ),
    ]
