# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0008_country'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='country',
            options={'verbose_name': 'Country', 'verbose_name_plural': 'Countries'},
        ),
        migrations.AddField(
            model_name='country',
            name='iso_prefix',
            field=models.CharField(default='ID', max_length=2),
            preserve_default=False,
        ),
    ]
