# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geographicals', '0002_auto_20160206_1845'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='continent',
            table=None,
        ),
    ]
