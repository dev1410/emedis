from django.shortcuts import render
from models import Tag


def tag_bulk_upload(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source:
    :return:
    """
    import csv

    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0

    for row in reader:
        tag_data = Tag(
            name=row['tag'],
            is_active=row['is_active'],
            created_user=request.user)

        tag_data.save()

        total_uploaded += 1

    return total_uploaded

