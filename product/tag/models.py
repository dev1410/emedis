from django.db.models import Model, CharField, BooleanField, \
    DateTimeField, ForeignKey
from django.contrib.auth.models import User


class Tag(Model):
    name = CharField(max_length=128)
    is_active = BooleanField(default=True)
    created_date = DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='tag_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, null=True)

    def __unicode__(self):
        return self.name
