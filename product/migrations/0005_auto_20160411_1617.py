# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import product.brand.validators


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0004_auto_20160328_1545'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brand',
            name='brand_logo',
            field=models.ImageField(blank=True, null=True, upload_to=b'brand/logo', validators=[product.brand.validators.brand_image_size]),
        ),
    ]
