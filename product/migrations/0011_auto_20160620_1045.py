# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0010_auto_20160620_1010'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='e_catalogue_registration',
            field=models.CharField(max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='product_type',
            field=models.CharField(max_length=300, null=True, verbose_name=b'Type/Model', blank=True),
        ),
    ]
