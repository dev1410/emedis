# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('product', '0018_productdetail_private_price_currency'),
    ]

    operations = [
        migrations.AddField(
            model_name='productdocument',
            name='updated_user',
            field=models.ForeignKey(default=None, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='productdocument',
            name='updated_date',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
