# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0020_product_product_registration'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productdetail',
            name='e_catalogue_price_currency',
            field=models.ForeignKey(to='product.ProductCurrency', null=True),
        ),
    ]
