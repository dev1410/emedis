# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0017_auto_20160905_1224'),
    ]

    operations = [
        migrations.AddField(
            model_name='productdetail',
            name='private_price_currency',
            field=models.ForeignKey(related_name='private_price_currency', default=1, to='product.ProductCurrency'),
            preserve_default=False,
        ),
    ]
