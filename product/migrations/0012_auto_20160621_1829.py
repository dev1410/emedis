# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0011_auto_20160620_1045'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productspecificationandcertification',
            name='green_features',
            field=models.CharField(help_text=b'Optional data field', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='productspecificationandcertification',
            name='locally_content',
            field=models.CharField(help_text=b'Optional data field', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='productspecificationandcertification',
            name='product_certification',
            field=models.CharField(help_text=b'Optional data field', max_length=255, blank=True),
        ),
    ]
