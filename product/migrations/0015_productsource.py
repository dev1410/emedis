# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0014_merge'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductSource',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('source_images', models.CharField(max_length=1000)),
                ('source_attachments', models.CharField(max_length=1000)),
                ('product', models.ForeignKey(to='product.Product')),
            ],
        ),
    ]
