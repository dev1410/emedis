# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0015_productsource'),
    ]

    operations = [
        migrations.AddField(
            model_name='productsource',
            name='source_attachment_titles',
            field=models.CharField(default='', max_length=1024),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='productsource',
            name='source_attachments',
            field=models.CharField(max_length=1024),
        ),
        migrations.AlterField(
            model_name='productsource',
            name='source_images',
            field=models.CharField(max_length=1024),
        ),
    ]
