# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0016_auto_20160830_1350'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productdetail',
            old_name='currency',
            new_name='e_catalogue_price_currency',
        ),
        migrations.AlterField(
            model_name='product',
            name='product_type',
            field=models.CharField(max_length=512, null=True, verbose_name=b'Type/Model', blank=True),
        ),
        migrations.AlterField(
            model_name='productdetail',
            name='special_training',
            field=models.IntegerField(default=2, help_text=b'Optional data field', null=True, blank=True, choices=[(1, b'Yes'), (2, b'No')]),
        ),
    ]
