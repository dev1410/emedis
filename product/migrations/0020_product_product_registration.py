# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0019_auto_20160915_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_registration',
            field=models.CharField(max_length=24, blank=True),
        ),
    ]
