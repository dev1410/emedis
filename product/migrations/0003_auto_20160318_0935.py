# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_auto_20160317_1359'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productdocument',
            name='document',
            field=models.FileField(null=True, upload_to=b'documents/attachments/%Y/%m/%d/', blank=True),
        ),
    ]
