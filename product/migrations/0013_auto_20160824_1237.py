# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0012_auto_20160720_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='productdetail',
            name='country_of_origin',
            field=models.ForeignKey(verbose_name=b'Origin', blank=True, to='product.CountryOfOrigin', null=True),
        ),
    ]
