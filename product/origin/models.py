from django.db import models
from django.contrib.auth.models import User


class CountryOfOrigin(models.Model):
    country_name = models.CharField(max_length=60)

    alpha2_code = models.CharField(
        "Alpha-2 code",
        max_length=5,
        help_text='Country code Alpha-2 code',
        blank=True)

    alpha3_code = models.CharField(
        "Alpha-3 code",
        max_length=5,
        help_text='Country code Alpha-3 code',
        blank=True)

    numeric_code = models.CharField(
        max_length=5,
        help_text='Country code Alpha-2 code',
        blank=True)

    iso_code = models.CharField(
        "ISO 3166-2 Subdivision codes",
        max_length=13,
        blank=True)

    is_active = models.BooleanField(default=True)

    created_date = models.DateTimeField(auto_now_add=True)

    created_user = models.ForeignKey(
        User, related_name='cor_created_user')

    updated_date = models.DateTimeField(auto_now=True)

    updated_user = models.ForeignKey(
        User, null=True,
        related_name='cor_updated_user')

    def __unicode__(self):
        return self.country_name

    class Meta:
        verbose_name_plural = 'Origins'
