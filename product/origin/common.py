import csv
from .models import CountryOfOrigin


def country_bulk_upload(source, request):
    """

    :param source
    :param request:
    :return:
    """

    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0
    tmp = []
    append_tmp = tmp.append

    for row in reader:
        country_data = CountryOfOrigin(
            country_name=row['country_name'],
            alpha2_code=row['alpha_2_code'],
            alpha3_code=row['alpha_3_code'],
            numeric_code=row['numeric_code'],
            iso_code=row['iso_3166_2_subdivision_code'],
            is_active=row['is_active'],
            created_user=request.user)

        append_tmp(country_data)

        total_uploaded += 1

    CountryOfOrigin.objects.bulk_create(tmp)

    return total_uploaded
