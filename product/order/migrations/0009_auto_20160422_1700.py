# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0008_auto_20160420_1648'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='vendor_order_number',
            field=models.CharField(default='', max_length=84),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='temporder',
            name='vendor_order_number',
            field=models.CharField(default='', max_length=84),
            preserve_default=False,
        ),
    ]
