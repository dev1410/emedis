# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0005_auto_20160415_1351'),
    ]

    operations = [
        migrations.AlterField(
            model_name='temporderdetail',
            name='shipping_cost',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
