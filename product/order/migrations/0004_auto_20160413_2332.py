# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_auto_20160412_1657'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='temporder',
            options={'verbose_name_plural': 'new orders'},
        ),
        migrations.RemoveField(
            model_name='temporder',
            name='is_processed',
        ),
        migrations.AddField(
            model_name='order',
            name='is_processed',
            field=models.BooleanField(default=False, help_text=b'Check this if this order will be processed.'),
        ),
    ]
