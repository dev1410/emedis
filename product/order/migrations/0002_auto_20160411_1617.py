# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='created_user',
            field=models.ForeignKey(related_name='order_created_user', to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='created_user',
            field=models.ForeignKey(related_name='temporder_created_user', to='customers.Customer'),
        ),
    ]
