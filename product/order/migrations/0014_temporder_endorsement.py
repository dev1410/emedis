# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0013_order_endorsement'),
    ]

    operations = [
        migrations.AddField(
            model_name='temporder',
            name='endorsement',
            field=models.IntegerField(default=0),
        ),
    ]
