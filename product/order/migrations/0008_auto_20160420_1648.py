# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0007_auto_20160415_1822'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdetail',
            name='brand',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='product_image_url',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='temporderdetail',
            name='brand',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='temporderdetail',
            name='product_image_url',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
