# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0011_auto_20160824_1237'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='created_date',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='order',
            name='created_user',
            field=models.ForeignKey(related_name='order_created_user', verbose_name=b'buyer', to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='order',
            name='is_completed',
            field=models.BooleanField(help_text=b'Check this if this order has been completed.', verbose_name=b'order has been completed'),
        ),
        migrations.AlterField(
            model_name='order',
            name='is_processed',
            field=models.BooleanField(default=False, help_text=b'Check this if this order will be processed.', verbose_name=b'order is processed'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='created_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name=b'order date'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='created_user',
            field=models.ForeignKey(related_name='temporder_created_user', verbose_name=b'buyer', to='customers.Customer'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='is_credit',
            field=models.BooleanField(default=False, verbose_name=b'credit payment method'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='owner',
            field=models.ForeignKey(verbose_name=b'vendor', to='product.ProductSupplier'),
        ),
        migrations.AlterField(
            model_name='temporder',
            name='shipping_cost',
            field=models.IntegerField(default=0, help_text=b'e.g: 19000'),
        ),
    ]
