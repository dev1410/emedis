# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('product', '__first__'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_number', models.CharField(max_length=84)),
                ('is_completed', models.BooleanField(help_text=b'Check this if this order has been completed.')),
                ('is_credit', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='order_created_user', to=settings.AUTH_USER_MODEL)),
                ('owner', models.ForeignKey(to='product.ProductSupplier')),
            ],
            options={
                'verbose_name_plural': 'Processed Orders',
            },
        ),
        migrations.CreateModel(
            name='OrderDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=128)),
                ('product_number', models.CharField(max_length=34, blank=True)),
                ('product_price', models.IntegerField()),
                ('product_quantity', models.IntegerField()),
                ('delivery_time', models.CharField(max_length=84)),
                ('subtotal', models.IntegerField()),
                ('product_availability', models.BooleanField(default=False)),
                ('shipment_number', models.CharField(max_length=24, blank=True)),
                ('is_sent', models.BooleanField(default=False, help_text=b'Check if this product has been sent to the buyer.')),
                ('is_delivered', models.BooleanField(default=False, help_text=b'Check if this product has been received by the buyer.')),
                ('order', models.ForeignKey(to='order.Order')),
                ('product', models.ForeignKey(to='product.Product')),
            ],
        ),
        migrations.CreateModel(
            name='TempOrder',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('order_number', models.CharField(max_length=84)),
                ('is_processed', models.BooleanField(help_text=b'Check this if this order will be processed.')),
                ('is_credit', models.BooleanField(default=False)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='temporder_created_user', to=settings.AUTH_USER_MODEL)),
                ('owner', models.ForeignKey(to='product.ProductSupplier')),
                ('updated_user', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'New Orders',
            },
        ),
        migrations.CreateModel(
            name='TempOrderDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_name', models.CharField(max_length=128)),
                ('product_number', models.CharField(max_length=34, blank=True)),
                ('product_price', models.IntegerField()),
                ('product_quantity', models.IntegerField()),
                ('product_availability', models.BooleanField(help_text=b'Check this field if this product available to order.')),
                ('delivery_time', models.CharField(max_length=84)),
                ('subtotal', models.IntegerField()),
                ('product', models.ForeignKey(to='product.Product')),
                ('temporder', models.ForeignKey(to='order.TempOrder')),
            ],
        ),
    ]
