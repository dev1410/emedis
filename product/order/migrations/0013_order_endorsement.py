# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0012_auto_20160907_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='endorsement',
            field=models.IntegerField(default=0),
        ),
    ]
