# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20160413_2332'),
    ]

    operations = [
        migrations.AddField(
            model_name='temporderdetail',
            name='installation_fee',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='temporderdetail',
            name='shipping_cost',
            field=models.IntegerField(default=None),
        ),
    ]
