# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0010_auto_20160725_1057'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderdetail',
            name='shipping_cost',
        ),
        migrations.RemoveField(
            model_name='temporderdetail',
            name='shipping_cost',
        ),
        migrations.AddField(
            model_name='order',
            name='shipping_cost',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='temporder',
            name='shipping_cost',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='temporderdetail',
            name='delivery_time',
            field=models.CharField(default=b'14 Days', help_text=b'e.g: 7 days', max_length=84),
        ),
    ]
