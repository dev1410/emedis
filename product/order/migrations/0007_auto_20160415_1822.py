# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0006_auto_20160415_1743'),
    ]

    operations = [
        migrations.AddField(
            model_name='orderdetail',
            name='installation_fee',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='orderdetail',
            name='shipping_cost',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
