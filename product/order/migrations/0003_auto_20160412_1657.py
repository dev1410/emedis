# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0002_auto_20160411_1617'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='cart_created_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='order',
            name='cart_updated_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='temporder',
            name='cart_created_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='temporder',
            name='cart_updated_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
