# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0009_auto_20160422_1700'),
    ]

    operations = [
        migrations.AddField(
            model_name='temporder',
            name='expire_date',
            field=models.DateField(null=True),
        ),
        migrations.AddField(
            model_name='temporder',
            name='is_processed',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='temporderdetail',
            name='shipping_cost',
            field=models.IntegerField(help_text=b'Leave blank will raise zero value.', null=True, blank=True),
        ),
    ]
