from django.contrib import admin
from .models import TempOrder, TempOrderDetail as TempOrderDetail_, Order,\
    OrderDetail as OrderDetail_


class TempOrderDetail(admin.TabularInline):
    model = TempOrderDetail_
    readonly_fields = (
        'product_name', 'brand', 'product_number',
        'product_price', 'product_quantity', 'subtotal')
    extra = 0
    fields = (
        'product_name', 'brand', 'product_number', 'product_price',
        'product_quantity', 'subtotal', 'installation_fee', 'delivery_time',
        'product_availability',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        permission = False
        if request.user.is_superuser:
            if not hasattr(obj, 'updated_user'):
                permission = True

        return permission

    def get_readonly_fields(self, request, obj=None):
        readonly = super(TempOrderDetail, self).get_readonly_fields(request, obj)
        if obj.updated_user is not None:
            readonly += ('installation_fee', 'delivery_time', 'product_availability')
        return readonly


class TempOrderAdmin(admin.ModelAdmin):
    change_list_template = 'order/temp_order.html'
    readonly_fields = ('order_number', 'vendor_order_number', 'is_credit', 'owner', 'created_user')

    list_display = (
        ('order_number', 'vendor_order_number', 'owner',
         'shipping_cost', 'endorsement', 'created_user', 'created_date', 'updated_date')
    )

    list_filter = ('created_date', 'updated_date')

    search_fields = ['order_number']

    fieldsets = [
        ('New Order',
         {
             'classes': ('wide', 'extrapretty'),
             'fields': ['order_number', 'vendor_order_number',
                        'created_user', 'is_credit',
                        'shipping_cost', 'endorsement']
         }
         )
    ]

    inlines = [TempOrderDetail]

    class Media:
        js = ('assets/js/order.min.js', )

    def has_add_permission(self, request):
        return False

    def get_queryset(self, request):
        qs = super(TempOrderAdmin, self).get_queryset(request)

        if not request.user.is_superuser:
            from product.models import ProductSupplier

            try:
                order_owner = ProductSupplier.objects.get(user_group=request.user.groups.get())
            except ProductSupplier.DoesNotExist:
                from product.models import ProductSubSupplier
                supplier = ProductSubSupplier.objects.get(user_group=request.user.groups.get())
                order_owner = supplier.supplier

            return qs.filter(owner=order_owner, is_processed=False)

        return qs.filter(is_processed=False)

    def get_list_display(self, request):
        if not request.user.is_superuser:
            self.list_display = ('vendor_order_number', 'endorsement',
                                 'created_date', 'updated_date')

        return super(TempOrderAdmin, self).get_list_display(request)

    def get_fieldsets(self, request, obj=None):
        if not request.user.is_superuser:
            self.fieldsets = [
                ('New Order',
                 {
                     'classes': ('wide', 'extrapretty'),
                     'fields': ['vendor_order_number', 'is_credit',
                                'shipping_cost', 'endorsement']
                 })]
        return super(TempOrderAdmin, self).get_fieldsets(request, obj)

    def get_readonly_fields(self, request, obj=None):
        sp = super(TempOrderAdmin, self).get_readonly_fields(request, obj)
        if obj.updated_user is not None:
            sp += ('shipping_cost',)
            if obj.endorsement > 0:
                sp += ('endorsement',)

        return sp

    def get_actions(self, request):
        actions = super(TempOrderAdmin, self).get_actions(request)

        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def save_model(self, request, obj, form, change):
        user = request.user

        if hasattr(obj, 'created_user'):
            obj.updated_user = user
        else:
            obj.created_user = user

        super(TempOrderAdmin, self).save_model(request, obj, form, change)


class OrderDetailInline(admin.TabularInline):
    model = OrderDetail_
    extra = 0
    exclude = ('product_image_url', )
    fields = (
        'product_name', 'product_number', 'product_price', 'product_quantity',
        'delivery_time', 'subtotal',)
    readonly_fields = (
        'product_name', 'product_number', 'product_price', 'product_quantity',
        'delivery_time', 'subtotal',)

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_fields(self, request, obj=None):
        fields = super(OrderDetailInline, self).get_fields(request, obj)

        if obj.is_processed:
            fields += ('shipment_number', )

        return fields


class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'order_number', 'is_processed', 'is_completed',
        'created_date', 'updated_date')

    list_filter = (
        ('is_completed', admin.BooleanFieldListFilter),
        'created_date', 'updated_date')

    readonly_fields = (
        'order_number', 'vendor_order_number', 'is_credit',
        'created_date', 'created_user', 'shipping_cost', 'endorsement')

    search_fields = ['order_number']

    fieldsets = [
        ('Order Detail',
         {
             'classes': ('wide', 'extrapretty'),
             'fields': ['order_number', 'created_user',
                        'is_processed', 'is_completed',
                        'is_credit', 'shipping_cost',
                        'endorsement', 'created_date']
         }
         )
    ]

    class Media:
        js = ('assets/js/order.min.js', )

    inlines = [OrderDetailInline]

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        return False

    def get_actions(self, request):
        actions = super(OrderAdmin, self).get_actions(request)

        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def get_queryset(self, request):
        qs = super(OrderAdmin, self).get_queryset(request)

        if request.user.is_superuser:
            return qs
        else:
            from product.models import ProductSupplier
            try:
                order_owner = ProductSupplier.objects.get(user_group=request.user.groups.get())
            except ProductSupplier.DoesNotExist:
                from product.models import ProductSubSupplier
                supplier = ProductSubSupplier.objects.get(user_group=request.user.groups.get())
                order_owner = supplier.supplier

            return qs.filter(owner=order_owner)

    def get_list_display(self, request):
        if request.user.is_superuser:
            self.list_display = (
                ('order_number', 'owner', 'is_processed',
                 'is_completed', 'created_date',
                 'updated_date')
            )

        return super(OrderAdmin, self).get_list_display(request)

    def get_readonly_fields(self, request, obj=None):
        readonly = super(OrderAdmin, self).get_readonly_fields(request, obj)

        if not request.user.is_superuser:
            readonly += ('is_completed', 'is_processed')
        elif not obj.is_processed:
            readonly += ('is_completed',)

        return readonly

    def get_fieldsets(self, request, obj=None):
        if not request.user.is_superuser:
            self.fieldsets = [
                ('Order Detail',
                 {
                     'classes': ('wide', 'extrapretty'),
                     'fields': ['vendor_order_number', 'created_user',
                                'is_credit', 'is_processed',
                                'is_completed', 'endorsement',
                                'created_date']
                 })]

        return super(OrderAdmin, self).get_fieldsets(request, obj)


admin.site.register(TempOrder, TempOrderAdmin)
admin.site.register(Order, OrderAdmin)
