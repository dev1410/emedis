from django.db import models
from django.conf import settings
from django.template import Context

from product.models import Product, ProductImage, Brand
from customers.models import Customer
from customers.detail.models import CustomerDetail
from django.contrib.auth.models import User
from product.models import ProductSupplier
from django.utils.safestring import mark_safe
from product.library.pdfy import Pdfy
from product.library.date import get_next_day
from mailer.engine import Send
from common.utils.object import CustomerObject, AutoAssignObject

from payments.veritrans import request, vtweb, validators
from payments.veritrans.payment_types import VTWeb

import datetime
import functools


class TempOrder(models.Model):
    order_number = models.CharField(max_length=84)
    vendor_order_number = models.CharField(max_length=84)
    is_credit = models.BooleanField(default=False, verbose_name='credit payment method')
    is_processed = models.IntegerField(null=True)
    owner = models.ForeignKey(ProductSupplier, verbose_name='vendor')
    created_date = models.DateTimeField(auto_now_add=True, verbose_name='order date')
    updated_date = models.DateTimeField(auto_now=True)
    expire_date = models.DateField(null=True)
    created_user = models.ForeignKey(Customer, related_name='temporder_created_user', verbose_name='buyer')
    updated_user = models.ForeignKey(User, null=True)
    cart_created_date = models.DateTimeField(blank=True, null=True)
    cart_updated_date = models.DateTimeField(blank=True, null=True)
    shipping_cost = models.IntegerField(default=0, help_text='e.g: 19000')
    endorsement = models.IntegerField(default=0)

    def __unicode__(self):
        return self.order_number

    def save(self, *args, **kwargs):
        if self.expire_date is None:
            self.expire_date = get_next_day(2, weekday=True)
        super(TempOrder, self).save(*args, **kwargs)

        # below are used to send quotation email and update order
        # status as confirmed if all the same order number on the table
        # has been confirmed before the due date. (NOTE: the due date are 2 days after created).

        # get related orders objects after saving object to the database.
        related_orders = TempOrder.objects.filter(
            order_number=self.order_number)

        is_expire = False
        process_now = True

        # Do validate processed orders objects.
        for length in range(related_orders.__len__()):

            # if the object has been expired or already processed, break the process.
            if hasattr(related_orders[length], 'is_processed'):
                if related_orders[length].is_processed > 0:
                    is_expire = True
                    break

            # if the entire objects have some object that isn't
            # already confirmed but still not expired, break the process.
            if related_orders[length].updated_user is None:
                process_now = False
                break

        if not is_expire and process_now:
            # lazy import subprocess
            import subprocess

            # update TemporderObjects.expire_date to today to let the management command get it's value.
            related_orders.update(expire_date=datetime.date.today())

            # command to run emedis new order inspection manager. This will create
            # order objects from confirmed TempOrder object then send final quotation
            # email for both stakeholder.
            args = ['python', 'manage.py', 'order', '--inpect', 'new']

            # This isn't work if you use virtualenv
            try:
                subprocess.call(args=args)
            except ImportError as e:

                import platform

                args = ['which', 'python'] if platform.system() == 'Linux' else ['where', 'python']

                print('You are using virtualenv, this isn\'t work on virtualenv. \n\r'
                      'Error message : %s \n\r'
                      'Python location : %s' % (e.message, subprocess.call(args)))

    @staticmethod
    def send_order_notification_email(order_number):

        temp_order_objects = TempOrder.objects.filter(
            order_number=order_number)

        temp_order_detail_objects = map(
            TempOrder.__get_temp_order_details__,
            temp_order_objects)

        detail_list = []
        append_detail_list = detail_list.append

        for detail_objects in xrange(
                temp_order_detail_objects.__len__()):

            for detail_object in xrange(
                    temp_order_detail_objects[detail_objects].__len__()):

                append_detail_list(
                    temp_order_detail_objects[detail_objects][detail_object]
                )

        customer = CustomerObject(
            customer=temp_order_objects[0].created_user,
            verbose=True)

        order = AutoAssignObject()
        order.order_number = temp_order_objects[0].order_number
        order.created_date = temp_order_objects[0].created_date.strftime('%d/%m/%Y')
        order.total = sum(map(lambda item: item.subtotal, detail_list))

        data = dict()
        data['customer'] = customer
        data['order'] = order
        data['products'] = detail_list
        data['subject'] = 'eMedis - Purchase Request'

        Send.buyer_order_notification(data)

        for index in xrange(temp_order_objects.__len__()):
            owner = temp_order_objects[index].owner
            owner_group = User.objects.filter(groups=owner.user_group)
            owner_products = temp_order_detail_objects[index]

            order.order_number = temp_order_objects[index].vendor_order_number

            order.total = sum(map(lambda item: item.subtotal, owner_products))

            order.process_link = '{0}/admin/order/temporder/{1}/'.format(
                settings.DOMAIN_URL,
                temp_order_objects[index].id)

            data['products'] = owner_products
            data['recipient_list'] = tuple(
                owner_group[length].email for length in xrange(owner_group.__len__()))

            Send.order_notification(data)

    @staticmethod
    def send_email_quotation(order, verbose=False):
        """
        Sending quotation email and vendor recap for all
        processed order. order can be either a list or
        order number, change verbose=True if you
        give order as a list of orders. or give
        order as an order_number

        :param order:
        :param verbose:
        :return:
        """
        if verbose:
            orders = order
        else:
            orders = Order.objects.filter(order_number=order)

        order_details = []
        append_detail = order_details.append

        products = []
        append_product = products.append

        for x in xrange(orders.__len__()):

            per_owner_products = OrderDetail.objects.filter(
                order=orders[x],
                product_availability=True)
            append_detail(per_owner_products)

            for i in xrange(per_owner_products.__len__()):
                append_product(per_owner_products[i])

        customer = CustomerObject(
            customer=orders[0].created_user,
            verbose=True)

        order = AutoAssignObject()
        order.order_number = orders[0].order_number
        order.created_date = orders[0].created_date.strftime('%d/%m/%Y')

        order.subtotal = sum(
            map(lambda item: item.product_price * item.product_quantity,
                products)
        )

        order.installation_fee = sum(map(lambda item: item.installation_fee or 0, products))
        order.shipping_cost = sum(map(lambda item: item.shipping_cost, orders))
        order.endorsement = sum(map(lambda item: item.endorsement, orders))
        order.total = sum(map(lambda item: item.subtotal, products)) - order.endorsement

        # vt_web_link = TempOrder.veritrans_charge_request(buyer=customer,
        #                                                  order=order)
        # if isinstance(vt_web_link, object):
        #     order.payment_link = vt_web_link.redirect_url

        data = dict()
        data['customer'] = customer
        data['order'] = order
        data['products'] = products
        data['subject'] = 'Penawaran Akhir Pesanan #{0}'.format(order.order_number)

        attachment_for_buyer = Pdfy(
            context=Context(data),
            template='email_templates/order/buyer_quotation.html',
            save_to='temp/quotation',
            output_filename='Penawaran_Akhir_Pesanan_#{0}'.format(order.order_number)
        ).generate()

        data['attachment'] = attachment_for_buyer

        Send.quotation(data=data)

        for index in xrange(orders.__len__()):
            owner = orders[index].owner
            owner_group = User.objects.filter(groups=owner.user_group)
            owner_products = order_details[index]

            order.order_number = orders[index].vendor_order_number
            order.subtotal = sum(map(lambda item: item.subtotal, owner_products))

            order.installation_fee = sum(map(
                lambda item: item.installation_fee or 0,
                owner_products))

            order.shipping_cost = orders[index].shipping_cost

            order.endorsement = orders[index].endorsement

            order.total = sum(
                [order.subtotal,
                 order.installation_fee,
                 orders[index].shipping_cost]) - order.endorsement

            data['products'] = owner_products

            data['recipient_list'] = tuple(
                owner_group[length].email for length in xrange(
                    owner_group.__len__()))

            Send.quotation(data, is_customer=False)

    @staticmethod
    def move_confirmed(temp_orders, verbose=True):

        if not verbose:
            temp_orders = TempOrder.objects.filter(
                expire_date=datetime.date.today(),
                updated_user__isnull=False,
                is_processed=0)

        temp_order_detail_lists = map(
            TempOrder.__get_temp_order_details__,
            temp_orders)

        order_objects = map(
            TempOrder.__create_order_object__,
            temp_orders)

        order_detail_objects = []
        append_order_detail_objects = order_detail_objects.append

        for length in xrange(temp_order_detail_lists.__len__()):
            append_order_detail_objects(
                map(
                    functools.partial(
                        TempOrder.__create_order_detail_object__,
                        order_object=order_objects[length]),
                    temp_order_detail_lists[length]
                )
            )

        map(TempOrder.__save_order_details_object__,
            order_detail_objects)

        temp_orders.update(is_processed=1)

        return order_objects

    @staticmethod
    def __create_order_object__(temp_order):
        return Order.objects.create(
            order_number=temp_order.order_number,
            vendor_order_number=temp_order.vendor_order_number,
            owner=temp_order.owner,
            is_processed=False,
            is_completed=False,
            is_credit=temp_order.is_credit,
            shipping_cost=temp_order.shipping_cost,
            endorsement=temp_order.endorsement,
            created_date=temp_order.created_date,
            created_user=temp_order.created_user)

    @staticmethod
    def __get_temp_order_details__(temp_order):
        return TempOrderDetail.objects.filter(temporder=temp_order)

    @staticmethod
    def __create_order_detail_object__(temp_order_detail, order_object):
        return OrderDetail(
            order=order_object,
            product=temp_order_detail.product,
            product_name=temp_order_detail.product_name,
            product_number=temp_order_detail.product_number,
            product_image_url=temp_order_detail.product_image_url,
            product_price=temp_order_detail.product_price,
            product_quantity=temp_order_detail.product_quantity,
            installation_fee=temp_order_detail.installation_fee,
            delivery_time=temp_order_detail.delivery_time,
            subtotal=temp_order_detail.subtotal,
            product_availability=temp_order_detail.product_availability,
            brand=temp_order_detail.brand,
            is_sent=False,
            is_delivered=False)

    @staticmethod
    def __save_order_details_object__(order_details_object):
        OrderDetail.objects.bulk_create(order_details_object)

    @staticmethod
    def veritrans_charge_request(buyer, order, products=list()):
        """
        Returned response object from submitting charge via Veritrans VTWeb method.
        :param buyer:
        :type buyer: :py:class:`common.utils.object.CustomerObject`
        :param order:
        :type order: :py:class:`object`
        :param products:
        :type products: :py:class:`product.order.models.OrderDetail`
        :rtype: :py:class:`str`
        """

        if not isinstance(buyer, object):
            raise ValueError(
                'The given buyer parameter is not an object, "{buyer_type}"'.format(buyer_type=type(order)))

        if not isinstance(order, object):
            raise ValueError(
                'The given order parameter is not an object, "{order_type}"'.format(order_type=type(order)))

        gateway = vtweb.VTWeb(sandbox_mode=True)

        customer = request.CustomerDetails(
            first_name=buyer.pic_name,
            last_name=buyer.pic_name,
            email=buyer.email,
            phone=buyer.phone)

        transaction = request.TransactionDetails(
            order_id=order.order_number,
            gross_amount=order.total)

        payment_type = VTWeb()

        # items = map(gateway.create_item, products)

        charge_req = request.ChargeRequest(charge_type=payment_type,
                                           transaction_details=transaction,
                                           customer_details=customer)

        try:
            req = gateway.charge_request(charge_req)
            return req

        except validators.ValidationError as e:
            raise validators.ValidationError('Error #111. {e}'.format(e=e.message))

    class Meta:
        verbose_name_plural = 'new orders'


class TempOrderDetail(models.Model):
    temporder = models.ForeignKey(TempOrder)
    product = models.ForeignKey(Product)
    product_name = models.CharField(max_length=128)
    product_number = models.CharField(max_length=34, blank=True)
    product_image_url = models.CharField(max_length=100, blank=True, null=True)
    product_price = models.IntegerField()
    product_quantity = models.IntegerField()
    product_availability = models.BooleanField(help_text='Check this field if this product available to order.')
    brand = models.CharField(max_length=100, blank=True, null=True)
    installation_fee = models.IntegerField(blank=True, null=True)
    delivery_time = models.CharField(max_length=84, help_text='e.g: 7 days', default='14 Days')
    subtotal = models.IntegerField()

    def save(self, *args, **kwargs):

        self.subtotal = sum([
            self.product_price * self.product_quantity,
            self.installation_fee or 0])

        super(TempOrderDetail, self).save(*args, **kwargs)


class Order(models.Model):
    order_number = models.CharField(max_length=84)
    vendor_order_number = models.CharField(max_length=84)
    owner = models.ForeignKey(ProductSupplier)
    is_processed = models.BooleanField(help_text='Check this if this order will be processed.', default=False,
                                       verbose_name='order is processed')
    is_completed = models.BooleanField(help_text='Check this if this order has been completed.',
                                       verbose_name='order has been completed')
    is_credit = models.BooleanField(default=False)
    created_date = models.DateTimeField()
    updated_date = models.DateTimeField(auto_now=True)
    created_user = models.ForeignKey(Customer, related_name='order_created_user', verbose_name='buyer')
    cart_created_date = models.DateTimeField(blank=True, null=True)
    cart_updated_date = models.DateTimeField(blank=True, null=True)
    shipping_cost = models.IntegerField(default=0)
    endorsement = models.IntegerField(default=0)

    def __unicode__(self):
        return self.order_number

    def save(self, *args, **kwargs):

        try:
            before_save_processed = Order.objects.get(pk=self.pk).is_processed
        except Order.DoesNotExist:
            before_save_processed = False

        super(Order, self).save(*args, **kwargs)

        if before_save_processed != self.is_processed:
            self.send_email_request_order(order_number=self.order_number)

    def invoice(self):
        return mark_safe(
            u'<a target="_blank" href="%s">%s</a>' % (
                "/documents/sales_confirmation/" + str(self.order_number), "View"))

    class Meta:
        verbose_name_plural = 'Processed Orders'

    @staticmethod
    def mark_as_paid_orders(order_number):
        paid_orders = Order.objects.filter(order_number=order_number)

        if paid_orders.__len__() > 0:
            for index in xrange(paid_orders.__len__()):
                paid_orders[index].is_processed = True
                paid_orders[index].save()

    @staticmethod
    def send_email_request_order(order_number):

        orders = Order.objects.filter(order_number=order_number)

        order_details = []
        append_order = order_details.append

        customer = CustomerObject(customer=orders[0].created_user, verbose=True)

        products = []
        append_product = products.append

        for index in xrange(orders.__len__()):

            if not orders[index].is_processed:
                return

            detail = OrderDetail.objects.filter(
                order=orders[index])
            append_order(detail)

            for length in xrange(detail.__len__()):
                append_product(detail[length])

        order = AutoAssignObject()
        order.order_number = orders[0].order_number
        order.created_date = orders[0].created_date.strftime('%d/%m/%Y')
        order.installation_fee = sum(map(lambda item: item.installation_fee or 0, products))
        order.shipping_cost = sum(map(lambda item: item.shipping_cost, orders))
        order.endorsement = sum(map(lambda item: item.endorsement, orders))
        order.total = sum(map(lambda item: item.subtotal, products)) - order.endorsement

        data = dict()
        data['customer'] = customer
        data['products'] = products
        data['order'] = order

        invoice_attachment = Pdfy(
            context=data,
            template='email_templates/order/buyer_invoice.html',
            save_to='temp/invoice',
            output_filename='Purchase Invoice #{0}'.format(orders[0].order_number)
        ).generate()

        data['attachment'] = invoice_attachment

        Send.buyer_invoice_order(data)

        for index in xrange(orders.__len__()):
            owner = orders[index].owner
            owner_group = User.objects.filter(groups=owner.user_group)
            owner_products = order_details[index]

            order.order_number = orders[index].vendor_order_number

            order.subtotal = sum(map(lambda item: item.subtotal, owner_products))

            order.installation_fee = sum(map(lambda item: item.installation_fee or 0, owner_products))

            order.shipping_cost = orders[index].shipping_cost

            order.endorsement = orders[index].endorsement

            order.total = sum(
                [order.subtotal,
                 order.installation_fee,
                 order.shipping_cost]) - order.endorsement

            data['products'] = owner_products

            data['recipient_list'] = tuple(
                owner_group[length].email for length in xrange(
                    owner_group.__len__()))

            # delivery_order_attachment = Pdfy(
            #     context=data,
            #     template='email_templates/order/delivery_order.html',
            #     save_to='temp/delivery_order',
            #     output_filename='Delivery Order #{0}'.format(order.order_number)
            # ).generate()
            #
            # data['attachment'] = delivery_order_attachment
            #
            Send.request_order(data=data)

    @property
    def total(self):
        details = OrderDetail.objects.filter(order=self,
                                             product_availability=True)
        total_from_detail = sum(map(lambda detail: detail.subtotal, details))
        total = (total_from_detail + self.shipping_cost) - self.endorsement
        return total


class OrderDetail(models.Model):
    order = models.ForeignKey(Order)
    product = models.ForeignKey(Product)
    product_name = models.CharField(max_length=128)
    product_number = models.CharField(max_length=34, blank=True)
    product_image_url = models.CharField(max_length=100, blank=True, null=True)
    product_price = models.IntegerField()
    product_quantity = models.IntegerField()
    installation_fee = models.IntegerField(blank=True, null=True)
    delivery_time = models.CharField(max_length=84)
    subtotal = models.IntegerField()
    product_availability = models.BooleanField(default=False)
    brand = models.CharField(max_length=100, blank=True, null=True)
    shipment_number = models.CharField(max_length=24, blank=True)
    is_sent = models.BooleanField(default=False, help_text='Check if this product has been sent to the buyer.')
    is_delivered = models.BooleanField(default=False, help_text='Check if this product has been received by the buyer.')

    def image_tag(self):
        return u'<img src="%s" />' % self.product_image_url
    image_tag.allow_tags = True

    def save(self, *args, **kwargs):
        self.subtotal = sum(
            [self.product_price,
             self.product_quantity,
             self.installation_fee or 0])

        super(OrderDetail, self).save(*args, **kwargs)

    @property
    def image(self):
        return self.product.get_first_image()
