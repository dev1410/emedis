import re

from .models import Product, ProductTag, ProductMetadata, \
    ProductCurrency, ProductDetail, ProductSpecificationAndCertification, \
    ProductSupplier, CountryOfOrigin, Tag, ProductSubSupplier, ProductDocument, \
    ProductImage
from brand.models import Brand
from category.subcategory.models import Subcategory, Category
from library.urlify import downcoder
import os
from settings import BASE_DIR
from django.core.files import File


def save_image(image_url, filename):
    folder = os.path.join(BASE_DIR, 'ekatalog_images/')
    image_url = str(image_url).replace('&ts=50', '&ts=300')
    os.system('wget "{0}" -O {1}\\{2} --no-check-certificate 1> NUL 2>NUL'.format(image_url, folder, filename))

    return os.path.join(folder, filename)


def bulk_upload_handler(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source:
    :param request:
    :return:
    """
    import csv

    the_file = open(source)

    reader = csv.DictReader(the_file, delimiter='\t')

    total_uploaded = 0

    for row in reader:

        if 'sub_dealer' not in row:
            row['sub_dealer'] = ''

        if 'product_registration' not in row:
            row['product_registration'] = ''

        if 'e_catalogue_registration' not in row:
            row['e_catalogue_registration'] = row['product_registration']

        if 'product_no' not in row:
            row['product_no'] = ''

        if 'product_number' not in row:
            row['product_number'] = row['product_no']

        if 'manufacture' not in row:
            raise KeyError('Key "manufacture" not found in a key list.')

        if 'brand' not in row:
            row['brand'] = row['manufacture']

        if 'is_active' not in row:
            row['is_active'] = True

        if 'sub_category' not in row:
            row['sub_category'] = ''

        if 'subcategory' not in row:
            if row['sub_category'] != '':
                row['subcategory'] = row['sub_category']
            else:
                row['subcategory'] = ''

        if 'product_price' not in row:
            raise KeyError('Price not found for product %s' % row['product_name'])

        if 'product_currency' not in row:
            string_price = row['product_price'].strip()
            if string_price[:2].isdigit():
                if row['supplier'] == 'PT. Karya Pratama':
                    row['product_currency'] = 'IDR'
                else:
                    raise KeyError('Currency not specified for product %s' % row['product_name'])
            else:
                if row['supplier'] == 'PT. Karya Pratama':
                    row['product_currency'] = 'IDR'
                else:
                    raise KeyError('Currency not specified for product %s' % row['product_name'])

        if 'e_catalogue_price' not in row:
#            range_str = tuple(str(s) for s in range(1, 99))
#            joined_range_str = tuple(s[:0] + "." + s[:2] for s in range_str)
#
#            is_float = row['product_price'].strip().replace('Rp', '').endswith(joined_range_str)
#
#            numeric_value = re.findall('\d+', row['product_price'])
#
 #           if is_float:
                result = "".join(numeric_value)
                row['product_price'] = float(s[:len(result)-2] + "." + s[:2] for s in str(result))
            else:
                row['product_price'] = int("".join(numeric_value))

            row['e_catalogue_price'] = None

        if 'private_price' not in row:
            row['private_price'] = None

        if 'currency' not in row:
            row['currency'] = row['product_currency']

        if 'updated_price_date' not in row:
            row['updated_price_date'] = '1970-01-01'

        if 'price_date' not in row:
            row['price_date'] = '1970-01-01'

        if 'warranty_period' not in row:
            row['warranty_period'] = ''

        if 'estimated_delivery_time' not in row:
            row['estimated_delivery_time'] = ''

        if 'special_training' not in row:
            row['special_training'] = ''

        if 'description' not in row:
            row['description'] = ''

        if 'additional_information' not in row:
            row['additional_information'] = ''

        if 'meta_description' not in row:
            row['meta_description'] = ''

        if 'meta_keyword' not in row:
            row['meta_keyword'] = ''

        if 'product_tag' not in row:
            row['product_tag'] = ''

        if 'product_link' not in row:
            row['product_link'] = ''

        if 'url' not in row:
            row['url'] = row['product_link']

        if 'attachment_links' not in row:
            row['attachment_links'] = None

        if 'image_link' not in row:
            row['image_link'] = None

        if 'country_of_origin' not in row:
            row['country_of_origin'] = 'N/A'

        if 'oem' not in row:
            row['oem'] = ''

        if 'product_certification' not in row:
            row['product_certification'] = ''

        if 'fda_clearance' not in row:
            row['fda_clearance'] = ''

        if 'ce_mark' not in row:
            row['ce_mark'] = ''

        if 'iec_compliance' not in row:
            row['iec_compliance'] = ''

        if 'locally_content' not in row:
            row['locally_content'] = ''

        if 'green_features' not in row:
            row['green_features'] = ''

        if 'umdns_code' not in row:
            row['umdns_code'] = ''

        if 'function' not in row:
            row['function'] = ''

        if 'description' not in row:
            row['description'] = row['function']

        if 'product_model' not in row:
            row['product_model'] = ''

        try:
            brand = Brand.objects.get(name=row['brand'])
        except Brand.DoesNotExist:
            brand = Brand(
                name=row['brand'],
                is_active=True,
                slug=downcoder(row['brand']),
                created_user=request.user)
            brand.save()

        try:
            print row['product_model']
        except KeyError:
            row['product_model'] = ''

        try:
            supplier = ProductSupplier.objects.get(name=row['supplier'])

            if row['sub_dealer'] != '':
                try:
                    sub_supplier = ProductSubSupplier.objects.get(
                        name=row['sub_dealer'])
                except ProductSubSupplier.DoesNotExist:
                    sub_supplier = None

        except ProductSupplier.DoesNotExist:
            supplier = ProductSupplier.objects.create(
                name=row['supplier'],
                is_active=True,
                is_verified=True,
                created_user=request.user)

            if row['sub_dealer'] != '':
                try:
                    sub_supplier = ProductSubSupplier.objects.get(
                        name=row['sub_dealer'])
                except ProductSubSupplier.DoesNotExist:
                    sub_supplier = None

        if row['sub_dealer'] != '' and sub_supplier is not None:

            try:
                print "[>>> Getting into try 1 ... <<<]"
                if row['supplier'].strip() == 'PT. Karya Pratama':
                    product_data = ProductMetadata.objects.get(
                        slug=downcoder(row['product_name'] + " " + row['product_model'])).product
                else:
                    product_data = ProductMetadata.objects.get(
                        slug=downcoder(row['product_name'])).product

            except ProductMetadata.DoesNotExist:
                print "[>>> Getting into except 1 ... <<<]"
                if row['supplier'].strip() == 'PT. Karya Pratama':
                    product_name = row['product_name'] + " " + row['product_model']
                else:
                    product_name = row['product_name']

                product_data = Product.objects.create(
                    product_name=product_name,
                    product_number=row['product_number'],
                    product_type=row['product_model'],
                    e_catalogue_registration=row['e_catalogue_registration'],
                    brand=brand,
                    main_dealer=supplier,
                    sub_dealer=sub_supplier,
                    is_active=row['is_active'],
                    created_user=request.user)
                product_data.save()

        else:

            try:
                print "[>>> Try to get product data if exists... ]"
                if row['supplier'].strip() == 'PT. Karya Pratama':
                    product_metadata = ProductMetadata.objects.get(
                        slug=downcoder(row['product_name'] + " " + row['product_model']))
                    product_data = product_metadata.product
                else:
                    product_metadata = ProductMetadata.objects.get(
                        slug=downcoder(row['product_name']))
                    product_data = product_metadata.product

            except ProductMetadata.DoesNotExist:
                print "[>>> Get into ProductMetadata.DoesNotExists... ]"

                if row['supplier'].strip() == 'PT. Karya Pratama':
                    product_name = row['product_name'] + " " + row['product_model']
                else:
                    product_name = row['product_name']

                product_data = Product(
                    product_name=product_name,
                    product_number=row['product_number'],
                    product_type=row['product_model'],
                    e_catalogue_registration=row['e_catalogue_registration'],
                    brand=brand,
                    main_dealer=supplier,
                    is_active=row['is_active'],
                    created_user=request.user)

                product_data.save()

        try:
            try:
                print row['category']
            except KeyError:
                row['category'] = 'N/A'

            category = Category.objects.get(
                category_help_text='N/A')

        except Category.DoesNotExist:

            if row['category'] != '':
                category = Category(
                    name=row['category'],
                    slug=downcoder(row['category']),
                    category_help_text=row['category'],
                    is_active=True,
                    created_user=request.user,
                    updated_user=request.user)
                category.save()

            else:

                category = Category(
                    name="N/A",
                    slug="n-a",
                    category_help_text="N/A",
                    is_active=True,
                    created_user=request.user,
                    updated_user=request.user)
                category.save()

        product_data.category.add(category)

        try:
            product_data.subcategory.add(
                Subcategory.objects.get(
                    name__exact=row['subcategory'], category=category))
        except Subcategory.DoesNotExist:
            pass

        try:
            CountryOfOrigin.objects.get(
                country_name=row['country_of_origin']),

        except CountryOfOrigin.DoesNotExist:
            row['country_of_origin'] = 'N/A'

        product_id = product_data.id

        total_uploaded += 1

        try:
            detail_data = ProductDetail.objects.get(product=product_data)
            detail_data.e_catalogue_price = row['e_catalogue_price']
            detail_data.private_price = row['private_price'] if row['private_price'] != '' else None
            detail_data.currency = ProductCurrency.objects.get(short_label__exact=row['currency'])
            detail_data.price_date = row['price_date']
            detail_data.warranty_period = row['warranty_period']
            detail_data.estimated_delivery_time = row['estimated_delivery_time']
            detail_data.country_of_origin = CountryOfOrigin.objects.get(
                    country_name=row['country_of_origin'])
            detail_data.special_training = row['special_training'] if row['special_training'] != '' else 2
            detail_data.url = row['url']

        except ProductDetail.DoesNotExist:

            detail_data = ProductDetail(
                product=product_data,
                e_catalogue_price=row['e_catalogue_price'],
                private_price=row['private_price'] if row['private_price'] != '' else None,
                currency=ProductCurrency.objects.get(short_label__exact=row['currency']),
                price_date=row['price_date'],
                warranty_period=row['warranty_period'],
                estimated_delivery_time=row['estimated_delivery_time'],
                country_of_origin=CountryOfOrigin.objects.get(
                    country_name=row['country_of_origin']),
                special_training=row['special_training'] if row['special_training'] != '' else 2,
                url=row['url'])

        detail_data.save()

        try:
            spec_data = ProductSpecificationAndCertification.objects.get(product=product_data)
            spec_data.specification = row['description']
            spec_data.OEM = row['oem']
            spec_data.product_certification = row['product_certification']
            spec_data.FDA_clearance = row['fda_clearance']
            spec_data.CE_mark = row['ce_mark']
            spec_data.IEC_compliance = row['iec_compliance']
            spec_data.locally_content = row['locally_content']
            spec_data.green_features = row['green_features']
            spec_data.UMDNS_code = row['umdns_code']
            spec_data.additional_information = row['additional_information']

        except ProductSpecificationAndCertification.DoesNotExist:
            spec_data = ProductSpecificationAndCertification(
                product=product_data,
                specification=row['description'],
                OEM=row['oem'],
                product_certification=row['product_certification'],
                FDA_clearance=row['fda_clearance'],
                CE_mark=row['ce_mark'],
                IEC_compliance=row['iec_compliance'],
                locally_content=row['locally_content'],
                green_features=row['green_features'],
                UMDNS_code=row['umdns_code'],
                additional_information=row['additional_information'])

        spec_data.save()

        if row['supplier'].strip() == 'PT. Karya Pratama':
            product_slug = downcoder(row['product_name'] + " " + row['product_model'])
        else:
            product_slug = downcoder(row['product_name'])

        try:
            meta_data = ProductMetadata.objects.get(product=product_data)
            meta_data.slug=product_slug
            meta_data.meta_description = row['meta_description']
            meta_data.meta_keyword = row['meta_keyword']

        except ProductMetadata.DoesNotExist:
            meta_data = ProductMetadata(
                product=product_data,
                slug=product_slug,
                meta_description=row['meta_description'],
                meta_keyword=row['meta_keyword'])

        meta_data.save()

        if row['product_tag'] != '':
            product_tag = ProductTag(
                product=Product.objects.get(pk=product_id))
            product_tag.save()
            product_tag.tag.add(Tag.objects.get(name__exact=row['product_tag']))

        if row['attachment_links'] is not None:
            import ast

            obj = ast.literal_eval(row['attachment_links'])
            obj = [n for n in obj]
            doc = []
            for n in range(len(obj)):
                for key, value in obj[n].iteritems():
                    doc.append(ProductDocument(
                        product=product_data,
                        name=key, document=value,
                        created_user=request.user))
            ProductDocument.objects.bulk_create(doc)

#        if row['image_link'] is not None:
#            save_image(row['image_link'], downcoder(row['product_name']) + '.jpg')
#            image_instance = ProductImage()
#            image_instance.product = product_data
#            image_instance.is_source = True
#            image_instance.image.save(
#                downcoder(row['product_name']) + '.jpg',
#                File(open(os.path.join(BASE_DIR, 'ekatalog_images/' + downcoder(row['product_name']) + '.jpg'))))
#            image_instance.save()

    return total_uploaded


def currency_bulk_upload(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source:
    :param request:
    :return:
    """
    import csv

    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0

    for row in reader:
        currency_data = ProductCurrency(
            short_label=row['short_label'],
            full_label=row['full_label'],
            is_active=row['is_active'],
            created_user=request.user)

        currency_data.save()

        total_uploaded += 1

    return total_uploaded


def country_bulk_upload(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source
    :param request:
    :return:
    """
    import csv

    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0

    for row in reader:
        currency_data = CountryOfOrigin(
            country_name=row['country_name'],
            alpha2_code=row['alpha_2_code'],
            alpha3_code=row['alpha_3_code'],
            numeric_code=row['numeric_code'],
            iso_code=row['iso_3166_2_subdivision_code'],
            is_active=row['is_active'],
            created_user=request.user)

        currency_data.save()

        total_uploaded += 1

    return total_uploaded


def in_dict(row):
    if 'sub_dealer' not in row:
        row['sub_dealer'] = ''

    if 'product_registration' not in row:
        row['product_registration'] = ''

    if 'e_catalogue_registration' not in row:
        row['e_catalogue_registration'] = row['product_registration']

    if 'product_no' not in row:
        row['product_no'] = ''

    if 'product_number' not in row:
        row['product_number'] = row['product_no']

    if 'manufacture' not in row:
        raise KeyError('Key "manufacture" not found in a key list.')

    if 'brand' not in row:
        row['brand'] = row['manufacture']

    if 'is_active' not in row:
        row['is_active'] = True

    if 'sub_category' not in row:
        row['sub_category'] = ''

    if 'subcategory' not in row:
        if row['sub_category'] != '':
            row['subcategory'] = row['sub_category']
        else:
            row['subcategory'] = ''

    if 'product_price' not in row:
        raise KeyError('Price not found for product %s' % row['product_name'])

    if 'product_currency' not in row:
        string_price = row['product_price'].strip()
        if string_price[:2].isdigit():
            if row['supplier'] == 'PT. Karya Pratama':
                row['product_currency'] = 'IDR'
            else:
                raise KeyError('Currency not specified for product %s' % row['product_name'])

    if 'e_catalogue_price' not in row:
        range_str = tuple(str(s) for s in range(1, 99))
        joined_range_str = tuple(s[:0] + "." + s[:2] for s in range_str)

        is_float = row['product_price'].strip().replace('Rp', '').endswith(joined_range_str)

        numeric_value = re.findall('\d+', row['product_price'])

        if is_float:
            result = "".join(numeric_value)
            row['product_price'] = float(s[:len(result)-2] + "." + s[:2] for s in str(result))
        else:
            row['product_price'] = int("".join(numeric_value))

    if 'private_price' not in row:
        row['private_price'] = None

    if 'currency' not in row:
        row['currency'] = row['product_currency']

    if 'updated_price_date' not in row:
        row['updated_price_date'] = '1970-01-01'

    if 'price_date' not in row:
        row['price_date'] = row['updated_price_date']

    if 'warranty_period' not in row:
        row['warranty_period'] = ''

    if 'estimated_delivery_time' not in row:
        row['estimated_delivery_time'] = ''

    if 'special_training' not in row:
        row['special_training'] = ''

    if 'description' not in row:
        row['description'] = row['function']

    if 'additional_information' not in row:
        row['additional_information'] = ''

    if 'meta_description' not in row:
        row['meta_description'] = ''

    if 'meta_keyword' not in row:
        row['meta_keyword'] = ''

    if 'product_tag' not in row:
        row['product_tag'] = ''

    if 'product_link' not in row:
        row['product_link'] = ''

    if 'url' not in row:
        row['url'] = row['product_link']

    if 'attachment_links' not in row:
        row['attachment_links'] = None

    if 'image_link' not in row:
        row['image_link'] = None

    if 'country_of_origin' not in row:
        row['country_of_origin'] = 'N/A'

    if 'oem' not in row:
        row['oem'] = ''

    if 'product_certification' not in row:
        row['product_certification'] = ''

    if 'fda_clearance' not in row:
        row['fda_clearance'] = ''

    if 'ce_mark' not in row:
        row['ce_mark'] = ''

    if 'iec_compliance' not in row:
        row['iec_compliance'] = ''

    if 'locally_content' not in row:
        row['locally_content'] = ''

    if 'green_features' not in row:
        row['green_features'] = ''

    if 'umdns_code' not in row:
        row['umdns_code'] = ''

    if 'function' not in row:
        row['function'] = ''

    if 'description' not in row:
        row['description'] = row['function']

    if 'product_model' not in row:
        row['product_model'] = ''

    return row
