from category.subcategory.models import Subcategory, Category
import forms
from .models import Upload
from views import bulk_upload_handler

from django.conf import settings
from django.conf.urls import patterns

from django.contrib import admin, messages
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User

from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from product.brand.models import Brand
from product.currency.models import ProductCurrency
from product.library.image import AdminImageWidget
from product.models import Product, ProductSpecificationAndCertification,\
    ProductImage, ProductDetail, ProductMetadata, ProductTag,\
    Tag, ProductDocument
from product.supplier.models import ProductSupplier
from product.supplier.sub_supplier.models import ProductSubSupplier

from search.engine import Update

from currency.admin import ProductCurrencyAdmin
from product.brand.admin import BrandAdmin
from product.supplier.admin import SupplierAdmin
from product.supplier.sub_supplier.admin import SubSupplierAdmin
from product.tag.admin import TagAdmin
from product.origin.admin import CountryOfOriginAdmin


def product_index_update_map_loop_func(product_id):
    return Update(
                index='product',
                doc_type='product',
                doc_id=product_id)


class ProductImageInline(admin.TabularInline):
    model = ProductImage
    extra = 1
    exclude = ['is_source']

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'image':
            kwargs['widget'] = AdminImageWidget

            if 'request' in kwargs:
                del kwargs['request']

            return db_field.formfield(**kwargs)
        return super(
            ProductImageInline, self).formfield_for_dbfield(
            db_field, **kwargs)


class ProductDetailInline(admin.StackedInline):
    formset = forms.RequiredInlineForm
    model = ProductDetail
    extra = 1
    exclude = ("url",)


class ProductSpecificationAndCertificationInline(admin.StackedInline):
    model = ProductSpecificationAndCertification
    extra = 1


class ProductMetadataInline(admin.StackedInline):
    model = ProductMetadata
    extra = 1


class ProductDocumentInline(admin.TabularInline):
    model = ProductDocument
    extra = 1
    fieldsets = [
        ('Product Documents',
         {
             'classes': ('wide', 'extrapretty'),
             'fields': ['name', 'document']
          }
         ),
    ]


class ProductTagInline(admin.StackedInline):
    model = ProductTag
    extra = 1

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):

        if db_field.name == 'tag':
            kwargs['queryset'] = Tag.objects.filter(is_active=1)
            kwargs['widget'] = FilteredSelectMultiple('Tags', False)

        return super(
            ProductTagInline, self).formfield_for_manytomany(
                db_field, request, **kwargs)


class ProductAdmin(admin.ModelAdmin):
    # def __init__(self, *args, **kwargs):
    #     self.request = kwargs.pop('request', None)
    #     super(ProductAdmin, self).__init__(*args, **kwargs)

    change_list_template = 'admin/product/custom/change_list.html'
    actions = [
        'activate_selected_products',
        'deactivate_selected_products'
    ]

    list_display = (
        'product_name', 'product_number',
        'e_catalogue_registration', 'brand',
        'main_dealer', 'is_active', 'created_date', 'updated_date')

    list_filter = (
        ('is_active', admin.BooleanFieldListFilter),
        'created_date', 'updated_date')

    search_fields = [
        'product_name', 'brand__name',
        'main_dealer__name']

    fieldsets = [
        ('Product ID',
         {
             'classes': ('wide', 'extrapretty', 'product_id'),
             'fields': [
                'product_name', 'product_number', 'product_type', 'product_registration',
                'e_catalogue_registration', 'brand', 'main_dealer',
                'sub_dealer', 'category', 'subcategory', 'is_active', 'is_sale', 'is_bestseller']
          }
         ),
    ]

    inlines = [
        ProductTagInline, ProductDetailInline,
        ProductImageInline,
        ProductSpecificationAndCertificationInline,
        ProductDocumentInline, ProductMetadataInline
    ]

    class Media:
        js = (
            'admin/js/urlify.js',
            'admin/js/prepopulate.js',
            'assets/js/custom_prepopulated_fields.min.js',
            'assets/js/sequence.min.js',
            'assets/js/product.min.js',
        )

    """
        Create custom activate_products action.
    """
    def activate_selected_products(self, request, queryset):
        rows_updated = queryset.update(is_active=True)

        product_ids = queryset.values_list(
            'id', flat=True).order_by('id')

        map(product_index_update_map_loop_func, product_ids)

        # for product_id in product_ids:
        #
        #     Update(
        #         index='product',
        #         doc_type='product',
        #         doc_id=product_id)

        message_bit = "1 product was"

        if rows_updated > 1:
            message_bit = "%s products were " % rows_updated

        self.message_user(
            request, "%s successfully activated." % message_bit)

    """
        Create custom deactivate_products action.
    """
    def deactivate_selected_products(self, request, queryset):
        rows_updated = queryset.update(is_active=False)

        product_ids = queryset.values_list(
            'id', flat=True).order_by('id')

        for product_id in product_ids:
            Update(
                index='product',
                doc_type='product',
                doc_id=product_id)

        if rows_updated == 1:
            message_bit = "1 product was"
        else:
            message_bit = "%s products were " % rows_updated
        self.message_user(
            request, "%s successfully deactivated." % message_bit)

    """
        If the current logged in user is not superuser,
        disable delete_selected.
    """
    def get_actions(self, request):
        actions = super(ProductAdmin, self).get_actions(request)
        if not request.user.is_superuser:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    """
        Here we go! if the current logged in user is
        not superuser, hide the product that they don't
        have the credentials to see :) just show their
        own product on the list_display.
    """
    def get_queryset(self, request):
        user = request.user
        qs = super(ProductAdmin, self).get_queryset(request)

        if user.is_superuser:
            return qs

        """
            Check whether the logged_in user is main_dealer
            or sub_dealer
        """
        try:
            product_owner = ProductSupplier.objects.get(
                user_group=user.groups.get())

            return qs.filter(main_dealer=product_owner.pk)
        except ProductSupplier.DoesNotExist:
            product_owner = ProductSubSupplier.objects.get(
                user_group=user.groups.get())

            return qs.filter(
                main_dealer=product_owner.supplier,
                sub_dealer=product_owner.pk)

    """
        If the current logged in user is not superuser,
        we hide main_dealer fieldsets to automate the product
        owner bro, save_model() will declare the value because
        main_dealer field doesn't have to be null
    """
    def get_form(self, request, obj=None, **kwargs):

        if not request.user.is_superuser:
            self.fieldsets = [
                ('General Product Information',
                 {'fields': [
                     'product_name', 'product_number', 'product_type', 'product_registration',
                     'e_catalogue_registration', 'brand', 'main_dealer',
                     'sub_dealer', 'category', 'subcategory',
                     'is_active']}), ]

        return super(ProductAdmin, self).get_form(request, obj, **kwargs)

    """
        Here we used to hide inactive data from the selected fields.
        The code working in case of user add or edit data.
    """
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        # The code below are used to filter only
        # active data to show on the listed choices

        cur_user = request.user
        dbfield_name = db_field.name

        if dbfield_name == 'brand':
            kwargs['queryset'] = Brand.objects.filter(is_active=1)

        if not cur_user.is_superuser:
            # check whether the logged_in user is
            # main_dealer or sub_dealer
            is_sub_dealer = ProductSubSupplier.objects.filter(
                user_group=cur_user.groups.get())

            if is_sub_dealer is None:
                if dbfield_name == 'main_dealer':
                    # Only show their self in the
                    # main_dealer list
                    kwargs['queryset'] = ProductSupplier.objects.filter(
                        user_group=cur_user.groups.get())

                if dbfield_name == 'sub_dealer':
                    # Only show sub_dealer that parentheses
                    # to the current logged_in main_dealer
                    supplier = ProductSupplier.objects.get(
                        pk=cur_user.groups.get())
                    kwargs['queryset'] = ProductSubSupplier.objects.filter(
                        is_active=1,
                        supplier=supplier.pk)
            else:
                if dbfield_name == 'main_dealer':
                    # Only show their self main_dealer on the main_dealer list
                    kwargs['queryset'] = ProductSupplier.objects.filter(
                        pk=ProductSubSupplier.objects.get(
                            user_group=cur_user.groups.get()).pk)

                if dbfield_name == 'sub_dealer':
                    # Only show their self on the sub_dealer list
                    kwargs['queryset'] = ProductSubSupplier.objects.filter(
                        user_group=cur_user.groups.get())
        else:
            if dbfield_name == 'main_dealer':
                kwargs['queryset'] = ProductSupplier.objects.filter(is_active=1)

            if dbfield_name == 'sub_dealer':
                kwargs['queryset'] = ProductSubSupplier.objects.filter(is_active=1)

        if dbfield_name == 'currency':
            kwargs['queryset'] = ProductCurrency.objects.filter(is_active=1)

        if dbfield_name == 'product_owner':
            kwargs['queryset'] = User.objects.filter(is_active=1)

        return super(
            ProductAdmin, self).formfield_for_foreignkey(
                db_field, request, **kwargs)

    """
        Create FilteredSelectMultiple overriding for category
    """
    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if db_field.name == 'category':
            kwargs['queryset'] = Category.objects.filter(is_active=1)

        if db_field.name == 'subcategory':
            kwargs['queryset'] = Subcategory.objects.filter(is_active=1)

        return super(
            ProductAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)

    """
        We should do some automatic save for undisplayed fields
        on fieldsets.
    """
    def save_model(self, request, obj, form, change):
        cur_user = request.user

        if not cur_user.is_superuser:
            try:
                supplier_group = ProductSupplier.objects.get(
                    user_group=cur_user.groups.get())
                obj.main_dealer = supplier_group
            except ProductSupplier.DoesNotExist:
                sub_supplier_group = ProductSubSupplier.objects.get(
                    user_group=cur_user.groups.get())
                obj.main_dealer = sub_supplier_group.supplier
                obj.sub_dealer = sub_supplier_group

        if hasattr(obj, 'created_user'):
            obj.updated_user = cur_user
        else:
            obj.created_user = cur_user

        super(ProductAdmin, self).save_model(request, obj, form, change)

    """
        Getting the requested URl on the current apps ModelAdmin process
    """
    def get_urls(self):
        """
        Define the given url of the custom link given
        on the custom action button.
        :return:
        """

        # Let we define post-defined url request by custom method
        urls = super(ProductAdmin, self).get_urls()
        my_urls = patterns(
            '', (r'^bulk_upload/$', self.admin_site.admin_view(self.bulk_upload))
        )
        return my_urls + urls

    """
        Custom AdminView for ProductAdmin
    """
    def bulk_upload(self, request):
        if request.method == 'POST':
            form = forms.BulkForm(request.POST, request.FILES)

            if form.is_valid():
                file_uploaded = Upload(upload_file=request.FILES['file_upload'])
                file_uploaded.save()

                # Make sure that the file is the chosen file,
                # not Temp file in memory.
                # recent_uploaded = Upload.objects.get(
                #     pk=file_uploaded.id).upload_file

                """
                    If the uploaded file for bulk_upload format is CSV,
                    We only set CSV format allowed.
                """
                if request.FILES['file_upload'].name.endswith('.csv'):

                    # csv_file = settings.MEDIA_ROOT + '/' + recent_uploaded.name
                    total_uploaded = bulk_upload_handler("aaa", request)

                    # If the bulk upload is running well and csv data not empty
                    if total_uploaded is not 0:
                        messages.success(
                            request,
                            'You have successfully upload %d of products bulk upload.' % total_uploaded)
                        return HttpResponseRedirect('..')

                    messages.warning(
                        request,
                        'Would you like to re-check your file \
                        ? \n I think some incorrect \
                        information there causing this failed to be processed.')
                    return HttpResponseRedirect('')

                messages.warning(
                    request,
                    'Please use only CSV file format to use products bulk upload.')
                return HttpResponseRedirect('')

        form = forms.BulkForm()

        return render_to_response(
            'admin/product/bulk_upload.html',
            {
                'form': form,
                'title': 'Bulk Upload',
                'site_header': settings.ADMIN_SITE_HEADER,
                'opts': self.model._meta,
                'change': False,
                'is_popup': False,
                'save_as': False,
                'has_delete_permission': False,
                'has_add_permission': False,
                'has_change_permission': False,
                'bulk_upload': True
             },
            context_instance=RequestContext(request)
        )

admin.site.site_header = settings.ADMIN_SITE_HEADER
admin.site.site_title = settings.ADMIN_SITE_TITLE
admin.site.register(Product, ProductAdmin)
