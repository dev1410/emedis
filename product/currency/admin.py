from django.http import HttpResponseRedirect
from django.contrib import admin, messages
from django.conf import settings
from django.conf.urls import patterns
from django.shortcuts import render_to_response
from django.template import RequestContext

from .models import ProductCurrency
from common import currency_bulk_upload
from product.forms import BulkForm
from product.models import Upload


class ProductCurrencyAdmin(admin.ModelAdmin):
    list_display = (
        'full_label', 'short_label', 'kurs_to_rupiah',
        'is_active', 'created_date', 'updated_date')

    list_display_links = ['short_label']

    list_filter = (
        'created_date', 'updated_date',
        ('is_active', admin.BooleanFieldListFilter))

    fieldsets = [
        ('General Information',
         {'fields': ['short_label', 'full_label', 'kurs_to_rupiah', 'is_active']}
         ),
    ]

    search_fields = ['short_label', 'full_label', ]

    def save_model(self, request, obj, form, change):
        cur_user = request.user
        if hasattr(obj, 'created_user'):
            obj.updated_user = cur_user
        else:
            obj.created_user = cur_user
        return super(ProductCurrencyAdmin, self).save_model(request, obj, form, change)

    def get_urls(self):
        """
        Define the given url of the custom link given
        on the custom action button.
        :return:
        """

        # Let we define post-defined url request by custom method
        urls = super(ProductCurrencyAdmin, self).get_urls()
        my_urls = patterns(
            '', (r'^bulk_upload/$', self.admin_site.admin_view(self.bulk_upload))
        )
        return my_urls + urls

    def bulk_upload(self, request):

        if request.method == 'POST':

            form = BulkForm(request.POST, request.FILES)

            if form.is_valid():
                file_uploaded = Upload(upload_file=request.FILES['file_upload'])
                file_uploaded.save()

                # Make sure that the file is the chosen file,
                # not Temp file in memory.
                recent_uploaded = Upload.objects.get(
                    pk=file_uploaded.id).upload_file

                """
                    If the uploaded file for bulk_upload format is CSV,
                    We only set CSV format allowed.
                """
                if request.FILES['file_upload'].name.endswith('.csv'):

                    csv_file = '{0}/{1}'.format(settings.MEDIA_ROOT, recent_uploaded.name)
                    total_uploaded = currency_bulk_upload(csv_file, request)

                    # If the bulk upload is running well and csv data not empty
                    if total_uploaded > 0:
                        messages.success(
                            request,
                            'You have successfully upload %d of currency bulk upload.' % total_uploaded)
                        return HttpResponseRedirect('..')

                    messages.warning(
                        request,
                        'Would you like to re-check your file \
                        ? \n I think some incorrect \
                        information there causing this failed to be processed.')
                    return HttpResponseRedirect('')

                messages.warning(
                    request,
                    'Please use only CSV file format to use currency bulk upload.')
                return HttpResponseRedirect('')

        form = BulkForm()

        return render_to_response(
            'admin/product/bulk_upload.html',
            {
                'form': form,
                'title': 'Bulk Upload',
                'site_header': settings.ADMIN_SITE_HEADER,
                'opts': self.model._meta,
                'change': False,
                'is_popup': False,
                'save_as': False,
                'has_delete_permission': False,
                'has_add_permission': False,
                'has_change_permission': False,
                'bulk_upload': True
             },
            context_instance=RequestContext(request)
        )

admin.site.register(ProductCurrency, ProductCurrencyAdmin)
