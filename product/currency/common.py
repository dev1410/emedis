import csv
from product.currency.models import ProductCurrency


def currency_bulk_upload(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source:
    :param request:
    :return:
    """
    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0
    temp = []
    append_temp = temp.append

    for row in reader:
        currency_data = ProductCurrency(
            short_label=row['short_label'],
            full_label=row['full_label'],
            is_active=row['is_active'],
            created_user=request.user)
        append_temp(currency_data)

        total_uploaded += 1
    ProductCurrency.objects.bulk_create(temp)

    return total_uploaded
