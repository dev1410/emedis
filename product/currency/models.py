from django.db import models
from django.contrib.auth.models import User


class ProductCurrency(models.Model):
    short_label = models.CharField(max_length=5)
    full_label = models.CharField(
        max_length=60, blank=True,
        help_text='Optional field data')
    kurs_to_rupiah = models.IntegerField(
        null=True, blank=True,
        help_text="don't input any symbol, just numeric please.")
    is_active = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(
        User, related_name='currency_created_user')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(
        User, null=True, related_name='currency_updated_user')

    def __unicode__(self):
        return self.short_label

    class Meta:
        verbose_name_plural = 'Currencies'
