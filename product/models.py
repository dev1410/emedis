import settings

from django.db import models
from django.contrib.auth.models import User

from ckeditor.fields import RichTextField
from brand.models import Brand
from category.models import Category
from category.subcategory.models import Subcategory
from smart_selects.db_fields import ChainedManyToManyField, ChainedForeignKey

from product.currency.models import ProductCurrency
from product.origin.models import CountryOfOrigin
from product.supplier.models import ProductSupplier
from product.supplier.sub_supplier.models import ProductSubSupplier
from product.tag.models import Tag
from django.db.models.signals import post_save
from django.dispatch import receiver
import os

default_choices = (
    (1, 'Yes'),
    (2, 'No'),
)


class Product(models.Model):
    product_name = models.CharField(max_length=255, db_index=True)

    product_number = models.CharField(max_length=50, blank=True, db_index=True)

    product_type = models.CharField(
        max_length=512,
        blank=True, null=True,
        verbose_name='Type/Model')

    product_registration = models.CharField(max_length=24, blank=True)

    e_catalogue_registration = models.CharField(max_length=255, blank=True)

    brand = models.ForeignKey(Brand)

    category = models.ManyToManyField(Category, blank=True)

    subcategory = ChainedManyToManyField(
        Subcategory,
        help_text="Subcategory",
        chained_field="category",
        chained_model_field="category", blank=True)

    main_dealer = models.ForeignKey(
        ProductSupplier, related_name='product_main_dealer',
        verbose_name='Supplier')

    sub_dealer = ChainedForeignKey(
        ProductSubSupplier,
        chained_field="main_dealer",
        chained_model_field="supplier",
        related_name='product_sub_dealer',
        blank=True, null=True,
        verbose_name='Supplier-Sub')

    is_active = models.BooleanField(default=False, verbose_name='publish')

    is_sale = models.BooleanField(default=False, verbose_name='sale')

    is_bestseller = models.BooleanField(default=False, verbose_name='best seller')

    created_date = models.DateTimeField(auto_now_add=True)

    created_user = models.ForeignKey(
        User, related_name='product_created_user')

    updated_date = models.DateTimeField(auto_now=True)

    updated_user = models.ForeignKey(
        User, related_name='product_updated_user',
        null=True)

    def __unicode__(self):
        return unicode(self.product_name)

    def delete(self, *args, **kwargs):

        super(Product, self).delete(*args, **kwargs)

        # Elastic Integrate
        from search.engine import Delete
        Delete(index='product', doc_type='product', doc_id=self.id)

    def get_product_image(self):
        product_images = ProductImage.objects.filter(product=self)
        return product_images[0] if product_images.__len__() > 0 else None

    def get_first_image(self):
        first_image = os.path.join(
            settings.STATIC_URL,
            settings.DEFAULT_NO_IMAGE_PRODUCT_URL)

        images = ProductImage.objects.filter(product=self).order_by('sort_order')

        path = os.path.join(settings.BASE_DIR, settings.MEDIA_URL.strip('/'))

        if images.__len__() > 0 and os.path.isfile(os.path.join(path, images[0].image.name)):
            first_image = os.path.join(settings.MEDIA_URL, images[0].image.name)
        else:
            brand_image = self.brand.brand_logo or None
            if brand_image is not None:
                if os.path.isfile(os.path.join(path, brand_image.name)):
                    first_image = brand_image.url

        return first_image

    def get_product_detail(self):
        return ProductDetail.objects.get(product=self)

    def get_product_metadata(self):
        return ProductMetadata.objects.get(product=self)

    def get_product_spesification_and_certification(self):
        return ProductSpecificationAndCertification.objects.get(product=self)


class ProductDetail(models.Model):
    product = models.OneToOneField(Product)

    e_catalogue_price = models.DecimalField(
        max_digits=20,
        decimal_places=2,
        blank=True, null=True)

    private_price = models.DecimalField(
        max_digits=20,
        decimal_places=2,
        blank=True, null=True)

    e_catalogue_price_currency = models.ForeignKey(ProductCurrency, null=True)

    private_price_currency = models.ForeignKey(ProductCurrency, related_name='private_price_currency')

    price_date = models.DateField(blank=True, null=True)

    warranty_period = models.CharField(
        max_length=12, blank=True,
        help_text='In month')

    estimated_delivery_time = models.CharField(
        max_length=50, blank=True,
        help_text='Optional data field')

    country_of_origin = models.ForeignKey(
        CountryOfOrigin,
        verbose_name='Origin', blank=True, null=True)

    special_training = models.IntegerField(
        choices=default_choices, blank=True, null=True,
        default=2,
        help_text='Optional data field')

    url = models.CharField(max_length=256, blank=True, null=True)


class ProductSpecificationAndCertification(models.Model):
    product = models.OneToOneField(Product)

    specification = RichTextField(
        config_name='full', blank=True,
        verbose_name='Description')

    OEM = models.CharField(
        max_length=128, blank=True,
        help_text='Optional data field')

    product_certification = models.CharField(
        max_length=255, blank=True,
        help_text='Optional data field')

    FDA_clearance = models.CharField(
        max_length=128, blank=True,
        help_text='Optional data field')

    CE_mark = models.CharField(
        max_length=128, blank=True,
        help_text='Optional data field')

    IEC_compliance = models.CharField(
        max_length=128, blank=True,
        help_text='Optional data field')

    locally_content = models.CharField(
        max_length=255, blank=True,
        help_text='Optional data field')

    green_features = models.CharField(
        max_length=255, blank=True,
        help_text='Optional data field')

    UMDNS_code = models.CharField(
        max_length=30, blank=True,
        help_text='Optional data field')

    additional_information = RichTextField(
        config_name='default', blank=True,
        help_text='Optional data field')


class ProductImage(models.Model):
    product = models.ForeignKey(Product)
    image = models.ImageField(upload_to='images/product', blank=True)
    sort_order = models.IntegerField(
        null=True, default=0,
        help_text="Sort order used to display which image "
                  "will appear for the first time from the "
                  "list of product images.")
    is_source = models.BooleanField(default=False)

    def image_media_url(self):
        return '{0}{1}'.format(settings.MEDIA_URL, self.image)

    def __unicode__(self):
        return self.image.name


class ProductDocument(models.Model):
    product = models.ForeignKey(Product)
    name = models.CharField(max_length=34, null=True, blank=True)
    document = models.FileField(
        upload_to='documents/attachments/%Y/%m/%d/', null=True, blank=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    created_user = models.ForeignKey(
        User, related_name='product_document_created_user')
    updated_user = models.ForeignKey(User, null=True, default=None)

    def __unicode__(self):
        return '%r-%r' % (self.product, self.document.name)


class ProductMetadata(models.Model):
    product = models.OneToOneField(Product)
    slug = models.SlugField(max_length=256)
    meta_description = models.TextField(
        blank=True, help_text='Optional data field')
    meta_keyword = models.TextField(
        blank=True,
        help_text='Optional data field')


class ProductTag(models.Model):
    product = models.OneToOneField(Product)
    tag = models.ManyToManyField(Tag, blank=True)


class Upload(models.Model):
    upload_file = models.FileField(upload_to='documents/%Y/%m/%d')


class ProductSource(models.Model):
    product = models.ForeignKey(Product)
    source_images = models.CharField(max_length=1024)
    source_attachments = models.CharField(max_length=1024)
    source_attachment_titles = models.CharField(max_length=1024)

    def __unicode__(self):
        return self.product.product_name


@receiver(post_save, sender=ProductMetadata)
def product_post_save(sender, instance, **kwargs):
    # Elastic Integrate
    from search.engine import Update, Create

    index_exists = settings.ELASTIC_CONNECTION.exists(
        index='product',
        doc_type='product',
        id=instance.product.id)

    if index_exists is True:
        Update(
            index='product',
            doc_type='product',
            doc_id=instance.product.id)
    else:
        Create(
            index='product',
            doc_type='product',
            doc_id=instance.product.id)
