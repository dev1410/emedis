from django import forms


class BulkForm(forms.Form):
    file_upload = forms.FileField(
        label='Please select the file',
        help_text='Only allowed for csv format files',)


class RequiredInlineForm(forms.models.BaseInlineFormSet):

    def _construct_form(self, i, **kwargs):
        form = super(RequiredInlineForm, self)._construct_form(i, **kwargs)
        form.empty_permitted = False
        return form
