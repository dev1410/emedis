from django.core.management.base import BaseCommand
from product.order import models
import datetime


class Command(BaseCommand):
    help = '>>> Order Management <<<'
    can_import_settings = True

    def add_arguments(self, parser):

        parser.add_argument(
            '--inspect',
            dest='inspect',
            default=False,
            help='Inspecting new order whether to '
                 'send it to the customer or mark it '
                 'as expired order.')

    def handle(self, *args, **options):
        today = datetime.date.today()

        if options['inspect'] == 'new':
	    print "---------------{0}---------------".format(str(datetime.datetime.now()))
            self.stdout.write("message: Getting expired orders...")
            expired_orders = models.TempOrder.objects.filter(
                expire_date=today,
                updated_user__isnull=True)

            if expired_orders.count() > 0:
                # Update unprocessed order to be expired flags (2)
                self.stdout.write(
                    "message: Updating expired order flag...")

                expired_orders.update(is_processed=2)

                self.stdout.write(
                    "message: %d order has been marked as expired order(s)" % expired_orders.__len__())
            else:
                self.stdout.write("message: No expired order are found!")

            self.stdout.write("message: Getting processed orders...")
            processed_orders = models.TempOrder.objects.filter(
                expire_date=today,
                updated_user__isnull=False,
                is_processed=0)

            if processed_orders.__len__() > 0:

                self.stdout.write("message: Moving processed order into processed order table...")

                # move the processed new_order(temp_order) into processed_order
                order_objects = models.TempOrder.move_confirmed(
                    temp_orders=processed_orders)

                self.stdout.write("message: %d has been moved." % order_objects.__len__())

                self.stdout.write("message: sending final quotation to both vendors and buyers")
                # Send the quotations and recap
                models.TempOrder.send_email_quotation(
                    order=order_objects, verbose=True)
            else:
                self.stdout.write("message: No processed order are found!")

            print '------------------------------'
