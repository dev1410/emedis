import os

from django.core.management.base import BaseCommand
from django.conf import settings
from product.brand.models import Brand
from django.core.files import File
from product.library.urlify import downcoder


class Command(BaseCommand):
    help = '>>> Brand images generator and slug repair management. <<<'
    can_import_settings = True

    def add_arguments(self, parser):

        parser.add_argument(
            '--generate',
            dest='generate',
            default=False,
            help='Generating brand logos from related images filename into specific directory. \r\n '
                 'type \'python manage.py generate logo')

        parser.add_argument(
            '--repair',
            dest='repair',
            default=False,
            help='Repairing brand field regarding to the given field name. Available \r\n option [slug]')

    def handle(self, *args, **options):
        all_brand = Brand.objects.values_list('slug')
        all_brand_list = [brand[0] for brand in all_brand]
        index_of = 0

        if options['generate'] == 'logo':
            brand_logos_source = os.path.join(settings.BASE_DIR, 'brand_logos')
            destination_folder = os.path.join(settings.MEDIA_ROOT, 'brand/logo')
            brand_logos_source_generated = os.path.join(brand_logos_source, 'generated')

            if not os.path.exists(brand_logos_source):
                os.makedirs(brand_logos_source)

            if not os.path.exists(destination_folder):
                os.makedirs(destination_folder)

            if not os.path.exists(brand_logos_source_generated):
                os.makedirs(brand_logos_source_generated)

            all_logo_list = os.listdir(brand_logos_source)
            all_logo_list_total = len(all_logo_list)

            if all_logo_list_total == 0:
                self.stdout.write('There\'s no brand logo on the source dir! \r')
            else:
                self.stdout.write("The brand logo source are located here : %s \r" % brand_logos_source)
                self.stdout.write("There are %d logo(s) in the directory \r" % all_logo_list_total)

                for new_logo in all_logo_list:
                    name, ext = os.path.splitext(new_logo)
                    path_to_file = os.path.join(brand_logos_source, new_logo)
                    path_to_file_generated = os.path.join(brand_logos_source_generated, new_logo)

                    if name in all_brand_list:
                        self.stdout.write('%d.Trying to generate logo for : %s \r\n' % (index_of, name))
                        index_of += 1

                        brand = Brand.objects.get(slug=name)

                        if brand.brand_logo:
                            try:
                                os.remove(os.path.join(destination_folder, new_logo))
                            except OSError as e:
                                self.stdout.write("%s \r\n" % e.strerror)
                                self.stdout.write("No image already exists on the destination folder!")
                        try:
                            brand.brand_logo.save(
                                path_to_file,
                                File(open(path_to_file, 'r')))

                            os.rename(path_to_file, path_to_file_generated)

                            self.stdout.write('> %s successfully generated !\r' % name)
                        except IOError as e:
                            brand.brand_logo = None
                            brand.save()
                            self.stdout.write("%s \r\n" % e.strerror)
                            self.stdout.write('%s \r\n' % path_to_file)

        if options['repair'] == 'slug':
            brand_objects = Brand.objects.all()

            self.stdout.write('Repairing for slug...')

            map(self.__brand_repair_slug__, brand_objects)

            self.stdout.write(
                '%d of total slug has been updated' % brand_objects.__len__())

    @staticmethod
    def __brand_repair_slug__(brand_object):
        brand_object.slug = downcoder(brand_object.name)
        return brand_object.save()
