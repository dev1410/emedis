from django.core.management.base import BaseCommand
from product.models import ProductImage
from PIL import Image
import os
import settings
import datetime


class Command(BaseCommand):
    help = 'Check utility for the entire module'
    can_import_settings = True

    def add_arguments(self, parser):
        parser.add_argument(
            '-pie',
            dest='product_image_existence',
            default=False,
            help='Checking entire product images existence with '
                 'open one by one if got error, will purge the '
                 'images data on the database to avoid error. '
                 'Usage : python manage.py inspect -pie verbose'
        )

    def handle(self, *args, **options):

        if options['product_image_existence'] == 'verbose':
            self.stdout.write(
                'start checking all the entire product image...\r')

            start_time = datetime.datetime.now()

            all_images = ProductImage.objects.all()
            non_exist_image = map(self.executor, all_images)

            fixed_problematic_images = []
            append_problematic_image = fixed_problematic_images.append

            for image_index in xrange(non_exist_image.__len__()):
                image = non_exist_image[image_index]
                if image is not None:
                    append_problematic_image(image)

            problematic_images_length = fixed_problematic_images.__len__()

            if problematic_images_length > 0:
                self.stdout.write(
                    'Found %d image existence issue(s)\r\n' % problematic_images_length)

                self.stdout.write('Deleting problematic image(s)...\r\n')

                map(
                    lambda image_obj: image_obj.delete(),
                    fixed_problematic_images)

            if problematic_images_length == 0:
                self.stdout.write('No problematic image(s) found.\r\n')

            self.stdout.write('Finish inspecting entire images in {0}.\r\n'.format(
                datetime.datetime.now() - start_time))

    def executor(self, image):
        path_to_media = os.path.join(settings.BASE_DIR, settings.MEDIA_URL)
        try:
            Image.open(os.path.join(path_to_media, image.image.path))
        except IOError:
            self.stdout.write(
                'Image check failed to check. IOError : %s, id=%d \r\n' % (
                    image.image.name, image.id))
            return image
