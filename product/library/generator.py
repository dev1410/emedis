"""
Python Generator library
"""
import datetime
from product.order.models import TempOrder, Order


class Generator(object):

    def __init__(self):
        pass

    @classmethod
    def order_number(cls):
        """
        Format : EMyy00000mmdd
        EM = Emedis prefix
        yy = 2 digits year
        00000 = order number start from 0
        mm = month
        dd = day
        :return: string
        """
        try:
            order_object = TempOrder.objects.filter(created_date__gte=datetime.datetime.now().date()).latest('id')
        except TempOrder.DoesNotExist:
            try:
                order_object = Order.objects.filter(created_date__gte=datetime.datetime.now().date()).latest('id')
            except Order.DoesNotExist:
                order_object = None

        last_order_number = int(order_object.order_number[4:9]
                                if hasattr(order_object, 'order_number') else 0) + 1

        number = "{0:05d}".format(last_order_number)

        now = datetime.datetime.now()

        prefix = 'EM'

        return "{0}{1}{2}{3}{4}".format(prefix, now.strftime("%y"), number, now.strftime("%d"), now.strftime("%m"))

    @classmethod
    def invoice_number(cls, data_dict):
        # INV/25/XVI/II/A/0001

        """
        this method require dictionary that all str data type
        {
            'day': '25',
            'year': '16',
            'month': '02 or 2 is ok',
            'order_group': 'A',
            'order_number: '0001'
        }
        :param data_dict:
        :return:
        """

        romanian = {
            1: 'I', 2: 'II', 3: 'III', 4: 'IV', 5: 'V',
            6: 'VI', 7: 'VII', 8: 'VIII', 9: 'IX', 10: 'X'}

        if int(data_dict['month']) > 10:
            number_list = map(int, list(data_dict['month']))
            romanian_list = [romanian[number_list[0] * 10], romanian[number_list[1]]]

            month = ''.join(str(x) for x in romanian_list)
        else:
            month = romanian[int(data_dict['month'])]

        if int(data_dict['year']) > 10:
            number_list = map(int, list(data_dict['year']))
            romanian_list = [romanian[number_list[0] * 10], romanian[number_list[1]]]

            year = ''.join(str(x) for x in romanian_list)
        else:
            year = data_dict['year']

        return "{0}/{1}/{2}/{3}/{4}/{5}".format(
            'INV', data_dict['day'], year, month,
            data_dict['order_group'], data_dict['order_number'])

    @classmethod
    def sku_number(cls, product_id):
        return "SKU-{0}{1}".format(
            datetime.date.today().strftime('%y'),
            str(product_id).zfill(6))
