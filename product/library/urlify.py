def downcoder(slug):
    import re

    char_map = [
        '\(', '\)', '\#', '!', '@', '\$', '%', '\^', '\*', '\+',
        '=', '\"', '\[', '\]', '\{', '\}', '\|', '~', '\:', ';',
        '\?', '<', '>', '&', '\s', '\`']

    special_char_map = ['\.', '\'', '\/', '\,']

    result = slug.split()

    clear_word = []

    for word in result:
        string = re.sub(r'|'.join(char_map), '', word)
        string = re.sub(r'|'.join(special_char_map), '-', string)
        string = string.strip().strip('-')
        if len(string) > 0:
            clear_word.append(string)

    return "-".join(clear_word).lower()
