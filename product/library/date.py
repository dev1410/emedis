import datetime


def is_weekend(date):
    return date.weekday() > 4


def get_next_day(value, weekday=False):
    today = datetime.date.today()

    while value > 0:

        tomorrow = today + datetime.timedelta(1)

        value -= 1

        if weekday:
            if is_weekend(tomorrow):
                value += 1

        today = tomorrow

    return today
