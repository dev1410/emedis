from __future__ import division
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe
from PIL import Image
import os
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _


class AdminImageWidget(AdminFileWidget):
    def render(self, name, value, attrs=None):
        output = []
        
        if value and getattr(value, 'url', None):
            
            # defining size gambarnya bro
            size = '100x100'
            standard_size = '1024x1024'
            x, y = [int(x) for x in size.split('x')]
            w, h = [int(w) for w in standard_size.split('x')]
            
            # Split the original file for the image re-create requirements
            filehead, filetail = os.path.split(value.path)
            basename, format = os.path.splitext(filetail)

            # rename original filename which contain or add the size of the file
            thumbnail = basename + '_' + size + format
            standard = basename + '_' + standard_size + format

            # Define original file path
            filename = value.path

            # Re-create image filename
            thumbnail_filename = os.path.join(filehead, thumbnail)
            standard_filename = os.path.join(filehead, standard)

            # Defining url path to the new images created (thumbnail and standard)
            filehead, filetail = os.path.split(value.url)
            thumbnail_url = filehead + '/' + thumbnail
            standard_url = filehead + '/' + standard
            
            ''' make sure bahwa thumbnail didapat dari the 
            current gambar original yang di resize '''

            # Check if the thumbnail is already exists
            # TODO: Don't overwrite the existing thumbnail.
            if os.path.exists(thumbnail_filename) \
                and os.path.getmtime(filename) > \
                    os.path.getmtime(thumbnail_filename):
                os.unlink(thumbnail_filename)

            # Check if the standard is already exists
            # TODO: Don't overwrite the existing standard.
            if os.path.exists(standard_filename) \
                and os.path.getmtime(filename) > \
                    os.path.getmtime(standard_filename):
                os.unlink(standard_filename)
            
            # jika gambar belum di resize, do resizing, this process return new image
            # Check the thumbnail if not exists
            # TODO: Create a new thumbnail if not exists.
            if not os.path.exists(thumbnail_filename):
                image = Image.open(filename)
                image.thumbnail([x, y], Image.ANTIALIAS)
                
                try:
                    image.save(
                        thumbnail_filename,
                        image.format,
                        quality=100,
                        optimize=1
                    )
                except:
                    image.save(
                        thumbnail_filename,
                        image.format,
                        quality=100
                    )
            output.append(
                u'<div><a href="%s" target="_blank"><img src="%s" alt="%s"/></a></div>' %
                (thumbnail_url, thumbnail_url, thumbnail_filename))

            # Check the standard if not exists
            # TODO: Create a new standard if not exists.
            if not os.path.exists(standard_filename):
                image = Image.open(filename)
                image.thumbnail([w, h], Image.ANTIALIAS)

                try:
                    image.save(
                        standard_filename,
                        image.format,
                        quality=100,
                        optimize=1
                    )
                except:
                    image.save(
                        standard_filename,
                        image.format,
                        quality=100
                    )

        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))


class Resizing(object):

    def __init__(
            self, width, height, filename,
            output_filename=None, save_to=None,
            replace=False):

        self._width = width
        self._height = height
        self._filename = filename
        self._output_filename = output_filename
        self._save_to = save_to
        self._replace = replace
        self._image = Image.open(self._filename)
        self._origin_x, self._origin_y = Image.open(self._filename).size
        self._errors = []

    def size_check(self):

        # If original width is less than the given width
        if self._origin_x < self._width:

            error = ValidationError(
                _(
                    'Original width of your image "{0}" is '
                    'less than our standard width "{1}"'.format(
                        self._origin_x, self._width)),
                code='invalid_width')

            self._errors.append(error)

        # If original height is less than the given height
        elif self._origin_y < self._height:
            error = ValidationError(
                _('Original height of your image "{0}" is '
                  'less than our standard height "{1}"'.format(
                    self._origin_y, self._height)),
                code='invalid_height')

            self._errors.append(error)

        # if any errors raise ValueError
        if len(self._errors) > 0:
            raise ValidationError(self._errors)

    def resize(self):

        head, tail = os.path.split(self._filename.path)
        name, ext = os.path.splitext(tail)

        if self._output_filename is None:
            if self._replace:
                result_name = name + ext
            else:
                result_name = name + '_' + "{0}x{1}".format(self._width, self._height) + ext
        else:
            if os.path.exists(
                    os.path.join(head, self._output_filename)) \
                    and self._save_to is None:
                raise IOError('The filename already exists on "{0}".'
                              'Either you give me another output_filename'
                              'or some other destination to deal with this error.\n'
                              'Current destination is "{1}".'.format(os.path.join(head, tail), head))
            result_name = self._output_filename

        if self._save_to is None:
            destination_path = os.path.join(head, result_name)
        else:
            if os.path.exists(os.path.join(self._save_to, result_name)):
                raise IOError(
                    'The filename already exists on the '
                    'destination "{0}". Either you change '
                    'the destination or give some output_filename'
                    'to deal with this error'.format(os.path.join(self._save_to, result_name)))

            destination_path = os.path.join(self._save_to, result_name)

        x, y = self._image.size

        self._image = self._image.resize((self._width, int((y / x) * self._width)), Image.ANTIALIAS)

        try:
            self._image.save(
                destination_path,
                self._image.format,
                quality=100,
                optimize=1)
        except IOError:
            self._image.save(
                destination_path,
                self._image.format,
                quality=100)

        return self._image
