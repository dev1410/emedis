import os

from django.template import loader
from django.conf import settings
from wkhtmltopdf.utils import wkhtmltopdf


class Pdfy(object):
    def __init__(
            self, context, template, save_to, output_filename):
        self._context = context
        self._template = template
        self._save_to = os.path.join(settings.MEDIA_ROOT, save_to)
        self._filename = output_filename + '.html'
        self._path = os.path.join(
            os.path.join(settings.BASE_DIR, self._save_to),
            self._filename)
	
    @property
    def renderer(self):
        template = loader.get_template(self._template)
        return template.render(self._context)

    def __html_writer(self):
        try:
            with open(self._path, "w") as the_file:
                the_file.write(self.renderer)
                the_file.close()
        except IOError as e:
            raise Exception("I/O error({0}): {1} at {2}".format(e.errno, e.strerror, self._path))

    def generate(self):

        obj = self._path

        self.__html_writer()

	print obj

        wkhtmltopdf(
            pages=[obj],
            output=obj.replace('.html', '.pdf'))

        os.remove(obj)
        self._path = obj.replace('.html', '.pdf')

        return self._path

    def download(self):
        from django.http import HttpResponse
        with open(self._path, 'rb') as pdf:
            response = HttpResponse(pdf.read(), content_type='application/pdf')
            response['Content-Disposition'] = 'filename=' + self._path

            return response
