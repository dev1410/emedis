from product.library.image import Resizing


def brand_image_size(value):
        Resizing(
            width=260, height=220,
            filename=value
        ).size_check()
