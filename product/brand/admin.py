from django.contrib import admin
from .models import Brand
from imaging import BrandImageWidget


class BrandAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'is_active',
        'created_date', 'updated_date')
    list_filter = (
        'created_date', 'updated_date',
        ('is_active', admin.BooleanFieldListFilter))
    
    search_fields = ['name', ]
    
    fieldsets = [
        ('General Information',
            {'fields':
             ['name', 'brand_logo', 'description', 'is_active', 'slug']
             }
         )
    ]
    
    prepopulated_fields = {'slug': ('name',)}
    
    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'brand_logo':
            kwargs['widget'] = BrandImageWidget
            
            try:
                del kwargs['request']
            except KeyError:
                pass
            
            return db_field.formfield(**kwargs)
        return super(BrandAdmin, self).formfield_for_dbfield(db_field, **kwargs)
    
    def save_model(self, request, obj, form, change):
        if Brand.objects.filter(slug=request.POST['slug']).exists():
            obj.updated_user = request.user
            obj.save()
        else:
            obj.created_user = request.user
            obj.save()

    def get_urls(self):
        """
        Define the given url of the custom link given
        on the custom action button.
        :return:
        """
        from django.conf.urls import patterns

        # Let we define post-defined url request by custom method
        urls = super(BrandAdmin, self).get_urls()
        my_urls = patterns(
            '', (r'^bulk_upload/$', self.admin_site.admin_view(self.bulk_upload))
        )
        return my_urls + urls

    def bulk_upload(self, request):
        from .forms import BulkForm
        from django.conf import settings
        from django.template import RequestContext
        from django.shortcuts import render_to_response

        if request.method == 'POST':
            from django.http import HttpResponseRedirect
            from django.contrib import messages
            from product.models import Upload
            from views import bulk_upload

            form = BulkForm(request.POST, request.FILES)

            if form.is_valid():
                file_uploaded = Upload(upload_file=request.FILES['file_upload'])
                file_uploaded.save()

                # Make sure that the file is the chosen file, not Temp file in memory.
                recent_uploaded = Upload.objects.get(pk=file_uploaded.id).upload_file

                """
                    If the uploaded file for bulk_upload format is CSV,
                    We only set CSV format allowed.
                """
                if request.FILES['file_upload'].name.endswith('.csv'):

                    csv_file = settings.MEDIA_ROOT + '/' + recent_uploaded.name
                    total_uploaded = bulk_upload(csv_file, request)

                    # If the bulk upload is running well and csv data not empty
                    if total_uploaded is not 0:
                        messages.success(
                            request,
                            'You have successfully upload %d of brand bulk upload.' % total_uploaded)
                        return HttpResponseRedirect('..')
                    else:
                        messages.warning(
                            request,
                            'Would you like to re-check your file \
                            ? \n I think some incorrect \
                            information there causing this failed to be processed.')
                        return HttpResponseRedirect('')

                else:
                    messages.warning(
                        request,
                        'Please use only CSV file format to use brand bulk upload.')
                    return HttpResponseRedirect('')
        else:
            form = BulkForm()

        return render_to_response(
            'admin/product/bulk_upload.html',
            {
                'form': form,
                'title': 'Bulk Upload',
                'site_header': settings.ADMIN_SITE_HEADER,
                'opts': self.model._meta,
                'change': False,
                'is_popup': False,
                'save_as': False,
                'has_delete_permission': False,
                'has_add_permission': False,
                'has_change_permission': False,
                'bulk_upload': True
             },
            context_instance=RequestContext(request)
        )

admin.site.register(Brand, BrandAdmin)
