from django import forms


class BulkForm(forms.Form):
    file_upload = forms.FileField(label='Please select the file', help_text='Only allowed for csv format files')
