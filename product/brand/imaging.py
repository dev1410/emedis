from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe


class BrandImageWidget(AdminFileWidget):

    def render(self, name, value, attrs=None):
        output = []

        if value and getattr(value, 'url', None):

            output.append(
                u'<div><a href="%s" target="_blank"><img src="%s" alt="%s"/></a></div>' %
                (value.url, value.url, value))

        output.append(super(AdminFileWidget, self).render(name, value, attrs))
        return mark_safe(u''.join(output))
