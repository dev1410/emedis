from django.shortcuts import render
from settings import MEDIA_ROOT

from .models import Brand
from product.library.urlify import downcoder


def bulk_upload(source, request):
    """
    This function is used in ProductAdmin::bulk_upload
    to generate uploaded products csv file
    :param source:
    :return:
    """
    import csv

    the_file = open(source)

    reader = csv.DictReader(the_file)

    total_uploaded = 0

    for row in reader:
        brand_data = Brand(
            name=row['name'],
            description=row['description'],
            is_active=row['is_active'],
            slug=downcoder(row['name']),
            created_user=request.user)

        brand_data.save()

        total_uploaded += 1

    return total_uploaded


