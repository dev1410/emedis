from django.db import models
from django.contrib.auth.models import User, Group
from product.supplier.models import ProductSupplier


class ProductSubSupplier(models.Model):
    supplier = models.ForeignKey(ProductSupplier)
    name = models.CharField(max_length=128)
    user_group = models.OneToOneField(
        Group,
        related_name='product_subsupplier_user_group',
        null=True, blank=True)
    is_active = models.NullBooleanField(default=False)
    is_verified = models.NullBooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(
        User, related_name='subsupplier_name')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(User, null=True)

    class Meta:
        verbose_name_plural = 'Suppliers-Sub'

    def __unicode__(self):
        return self.name
