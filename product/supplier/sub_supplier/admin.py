from django.contrib import admin
from .models import ProductSubSupplier


class SubSupplierAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'user_group', 'is_verified',
        'is_active', 'created_date')

    list_filter = (
        'created_date', 'updated_date',
        ('is_active', admin.BooleanFieldListFilter),
        ('is_verified', admin.BooleanFieldListFilter),
    )

    fieldsets = [
        ('General Information',
         {'fields': ['name', 'supplier', 'user_group', 'is_active', 'is_verified']
          }
         )
    ]

    search_fields = ['name', ]

    def save_model(self, request, obj, form, change):

        cur_user = request.user

        if hasattr(obj, 'created_user'):
            obj.updated_user = cur_user
        else:
            obj.created_user = cur_user

        super(SubSupplierAdmin, self).save_model(request, obj, form, change)

admin.site.register(ProductSubSupplier, SubSupplierAdmin)
