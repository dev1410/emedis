from django.db import models
from django.contrib.auth.models import User, Group


class ProductSupplier(models.Model):
    name = models.CharField(max_length=128)
    user_group = models.OneToOneField(
        Group,
        related_name='product_supplier_user_group',
        null=True, blank=True)
    is_active = models.NullBooleanField(default=False)
    is_verified = models.NullBooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(
        User, related_name='supplier_name')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(User, null=True)

    class Meta:
        verbose_name_plural = 'Suppliers'

    def __unicode__(self):
        return self.name
