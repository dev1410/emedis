from django.db import models
from django.contrib.auth.models import User
from django.core.cache import cache
from django.conf import settings


class Body(models.Model):
    class Meta:
        verbose_name_plural = 'Email Bodies'
        verbose_name = 'Email Body'

    title = models.CharField(max_length=55,
                             blank=False,
                             null=False,
                             help_text='Content Title')
    template = models.TextField(blank=False,
                                null=False,
                                help_text='Template')
    slug = models.SlugField(unique=True)
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(User, related_name='email_body_creator')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(User, related_name='email_body_updater', null=True)

    def __unicode__(self):
        return self.template

    def save(self, *args, **kwargs):
        cache.set('email-'.format(self.slug), self, 3600 * 24 * 30)

        super(Body, self).save(*args, **kwargs)

    @staticmethod
    def get_body(slug):
        """
        from email_template.models import Body
        from django.template import Template, Context

        def <method>(slug):
            context = Context({'name': 'Hello World',
                               'content': '<b>Testing</b>',})

            template = Template(Body.get_body('<template-slug>'))
            http_response = template.render(context)

            return HttpResponse(http_response)

        :param slug:
        :return:
        """

        email_body = cache.get(slug)

        try:
            if email_body is None:
                email_body = Body.objects.get(slug=slug)
                cache.set('email-'.format(slug), email_body, settings.CACHE_TIME)
        except Body.DoesNotExist:
            email_body = None

        return email_body
