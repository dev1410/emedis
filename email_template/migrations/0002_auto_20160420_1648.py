# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('email_template', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='body',
            options={'verbose_name': 'Email Body', 'verbose_name_plural': 'Email Bodies'},
        ),
    ]
