# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Body',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(help_text=b'Content Title', max_length=55)),
                ('template', models.TextField(help_text=b'Template')),
                ('slug', models.SlugField(unique=True)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='email_body_creator', to=settings.AUTH_USER_MODEL)),
                ('updated_user', models.ForeignKey(related_name='email_body_updater', to=settings.AUTH_USER_MODEL, null=True)),
            ],
        ),
    ]
