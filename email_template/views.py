from __future__ import unicode_literals

import settings
from models import Body
from customers.models import Customer
from django.template import Template, Context
from django.http import HttpResponse


def body(request, slug):
    email_body = None

    try:
        email_body = Body.get_body(slug=slug)
        data = Customer.objects.get(id=1)
    except Body.DoesNotExist:
        pass

    context = Context({'email_body': email_body,
                       'data': data, })

    template = Template(email_body)
    http_response = template.render(context)

    return HttpResponse(http_response)


def request_order(data):
    email_body = Body.get_body(slug='request-order')

    context = Context({
        'customer': data['customer'],
        'order': data['order'],
        'products': data['products'],
        'domain_url': settings.DOMAIN_URL
    })

    return Template(email_body).render(context)


def buyer_invoice(data):
    email_body = Body.get_body(slug='buyer-invoice')

    context = Context({
        'customer': data['customer'],
        'order': data['order'],
        'products': data['products'],
        'domain_url': settings.DOMAIN_URL
    })

    return Template(email_body).render(context)


def purchase_request(data, is_customer=True):
    email_body = None

    try:
        email_body = Body.get_body(slug='seller-purchase-request')

        if is_customer:
            email_body = Body.get_body(slug='buyer-purchase-request')

    except Body.DoesNotExist:
        pass

    context = Context({
        'customer': data['customer'],
        'order': data['order'],
        'products': data['products'],
        'vendor': data['vendor'] if 'vendor' in data else None,
        'domain_url': settings.DOMAIN_URL
    })

    template = Template(email_body)
    http_response = template.render(context)

    return http_response


def quotation(data, is_customer=True):
    email_body = None
    try:

        email_body = Body.get_body(slug='buyer-quotation')

        if not is_customer:
            email_body = Body.get_body(slug='seller-quotation')

    except Body.DoesNotExist:
        pass

    context = Context({
        'customer': data['customer'],
        'order': data['order'],
        'products': data['products'],
        'vendor': data['vendor'] if 'vendor' in data else None,
        'domain_url': settings.DOMAIN_URL
    })

    return Template(email_body).render(context)
