from django.contrib import admin
from models import Body


class BodyAdmin(admin.ModelAdmin):
    list_display = ('title',
                    'template',
                    'slug',)
    fieldsets = (
        ('Content', {'fields': ('title', 'template',)}),
        ('Additional', {'fields': ('slug',)})
    )
    prepopulated_fields = {'slug': ('title',)}

    def save_model(self, request, obj, form, change):
        if Body.objects.filter(slug=request.POST['slug']).exists():
            obj.updated_user = request.user
            obj.save()
        else :
            obj.created_user = request.user
            obj.save()

admin.site.register(Body, BodyAdmin)
