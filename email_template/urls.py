from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^email/(?P<slug>[\w-]+)/$', views.body, name='email_template'),
]

