    #!/usr/bin/env python
import os
import sys

if __name__ == "__main__":

    base = os.path.dirname(os.path.dirname(__file__))
    base_parent = os.path.dirname(base)

    sys.path.append(base)
    sys.path.append(base_parent)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
