-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 09 Mar 2016 pada 02.10
-- Versi Server: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `emedis_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_category`
--

DROP TABLE IF EXISTS `category_category`;
CREATE TABLE IF NOT EXISTS `category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(84) NOT NULL,
  `family_id` int(11) DEFAULT NULL,
  `sort_order` smallint(6) NOT NULL DEFAULT '1',
  `category_help_text` varchar(100) DEFAULT NULL,
  `description` longtext NOT NULL,
  `category_icon` varchar(100) DEFAULT NULL,
  `slug` varchar(256) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data untuk tabel `category_category`
--

INSERT INTO `category_category` (`id`, `name`, `family_id`, `sort_order`, `category_help_text`, `description`, `category_icon`, `slug`, `is_active`, `created_date`, `created_user_id`, `updated_date`, `updated_user_id`) VALUES
(1, 'Anastesi', NULL, 5, 'Peralatan Anastesi', '<p>Peralatan Anastesi</p>\r\n', '', 'anastesi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-08 11:09:09.633000', 1),
(2, 'Sediaan Untuk Mencuci', NULL, 18, 'Sediaan Untuk Mencuci', '<p>Sediaan Untuk Mencuci</p>\r\n', '', 'sediaan-untuk-mencuci', 1, '2016-03-07 13:38:02.954000', 1, '2016-03-08 11:09:32.329000', 1),
(3, 'Antiseptika dan Desinfektan', NULL, 12, 'Antiseptika dan Desinfektan', '<p>Antiseptika dan Desinfektan</p>\r\n', '', 'antiseptika-dan-desinfektan', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-08 11:09:56.785000', 1),
(4, 'Pewangi', NULL, 22, 'Pewangi', '<p>Pewangi</p>\r\n', '', 'pewangi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:03:18.300000', 1),
(5, 'Neurologi', NULL, 15, 'Peralatan Neurologi', '<p>Peralatan Neurologi</p>\r\n', '', 'neurologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:03:56.695000', 1),
(6, 'Telinga, Hidung & Tenggorokan', NULL, 16, 'Peralatan Telinga, Hidung dan Tenggorokan (THT)', '<p>Peralatan Telinga, Hidung dan Tenggorokan (THT)</p>\r\n', '', 'telinga-hidung-dan-tenggorokan', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:04:08.676000', 1),
(7, 'RS Umum & Perorangan', NULL, 1, 'Peralatan Rumah Sakit Umum dan Perorangan (RSU & P)', '<p>Peralatan Rumah Sakit Umum dan Perorangan (RSU &amp; P)</p>\r\n', '', 'rs-umum-dan-perorangan', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:04:20.285000', 1),
(8, 'Ortopedi', NULL, 3, 'Peralatan Ortopedi', '<p>Peralatan Ortopedi</p>\r\n', '', 'ortopedi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:04:42.797000', 1),
(9, 'Alat Perawatan Bayi', NULL, 19, 'Alat Perawatan Bayi', '<p>Alat Perawatan Bayi</p>\r\n', '', 'alat-perawatan-bayi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:04:53.604000', 1),
(10, 'Radiologi', NULL, 9, 'Peralatan Radiologi', '<p>Peralatan Radiologi</p>\r\n', '', 'radiologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:03.352000', 1),
(11, 'Gigi', NULL, 11, 'Peralatan Gigi', '<p>Peralatan Gigi</p>\r\n', '', 'gigi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:11.800000', 1),
(12, 'Hematologi & Patologi', NULL, 10, 'Peralatan Hematologi dan Patologi', '<p>Peralatan Hematologi dan Patologi</p>\r\n', '', 'hematologi-dan-patologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:24.093000', 1),
(13, 'Tissue & Kapas', NULL, 20, 'Tissue dan Kapas', '<p>Tissue dan Kapas</p>\r\n', '', 'tissue-dan-kapas', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:32.898000', 1),
(14, 'Pembersih', NULL, 21, 'Pembersih', '<p>Pembersih</p>\r\n', '', 'pembersih', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:46.055000', 1),
(15, 'Imunologi & Mikrobiologi', NULL, 13, 'Peralatan Imunologi dan Mikrobiologi', '<p>Peralatan Imunologi dan Mikrobiologi</p>\r\n', '', 'imunologi-dan-mikrobiologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:05:57.805000', 1),
(16, 'Bedah Umum & Plastik', NULL, 2, 'Peralatan Bedah Umum dan Bedah Plastik', '<p>Peralatan Bedah Umum dan Bedah Plastik</p>\r\n', '', 'bedah-umum-dan-plastik', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:07.854000', 1),
(17, 'Obstetrik & Ginekologi', NULL, 7, 'Peralatan Obstetrik dan Ginekologi (OG)', '<p>Peralatan Obstetrik dan Ginekologi (OG)</p>\r\n', '', 'obstetrik-dan-ginekologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:18.597000', 1),
(18, 'Kardiologi', NULL, 6, 'Peralatan Kardiologi', '<p>Peralatan Kardiologi</p>\r\n', '', 'kardiologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:27.328000', 1),
(19, 'Gastroenterologi-Urologi', NULL, 8, 'Peralatan Gastroenterologi-Urologi (GU)', '<p>Peralatan Gastroenterologi-Urologi (GU)</p>\r\n', '', 'gastroenterologi-urologi', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:35.219000', 1),
(20, 'Mata', NULL, 17, 'Peralatan Mata', '<p>Peralatan Mata</p>\r\n', '', 'mata', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:42.663000', 1),
(21, 'Pestisida Rumah Tangga', NULL, 23, 'Pestisida Rumah Tangga', '<p>Pestisida Rumah Tangga</p>\r\n', '', 'pestisida-rumah-tangga', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:06:50.667000', 1),
(22, 'Lainnya', NULL, 24, 'Lainnya', '<p>Lainnya</p>\r\n', '', 'lainnya', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:07:35.946000', 1),
(23, 'Kimia & Toksikologi Klinik', NULL, 4, 'Peralatan Kimia Klinik dan Toksikologi Klinik', '<p>Peralatan Kimia Klinik dan Toksikologi Klinik</p>\r\n', '', 'kimia-dan-toksikologi-klinik', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:07:54.043000', 1),
(24, 'Kesehatan Fisik', NULL, 14, 'Peralatan Kesehatan Fisik', '<p>Peralatan Kesehatan Fisik</p>\r\n', '', 'kesehatan-fisik', 1, '2016-03-07 12:36:44.917000', 1, '2016-03-09 01:08:05.850000', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_subcategory`
--

DROP TABLE IF EXISTS `category_subcategory`;
CREATE TABLE IF NOT EXISTS `category_subcategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `name` varchar(84) NOT NULL,
  `subcategory_icon` varchar(100) DEFAULT NULL,
  `description` longtext NOT NULL,
  `slug` varchar(256) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created_date` datetime(6) NOT NULL,
  `created_user_id` int(11) NOT NULL,
  `updated_date` datetime(6) NOT NULL,
  `updated_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

--
-- Dumping data untuk tabel `category_subcategory`
--

INSERT INTO `category_subcategory` (`id`, `category_id`, `name`, `subcategory_icon`, `description`, `slug`, `is_active`, `created_date`, `created_user_id`, `updated_date`, `updated_user_id`) VALUES
(1, 1, 'Pembersih peralatan dapur', NULL, '', 'pembersih-peralatan-dapur', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(2, 1, 'Pembersih kaca', NULL, '', 'pembersih-kaca', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(3, 1, 'Pembersih lantai', NULL, '', 'pembersih-lantai', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(4, 1, 'Pembersih porselen', NULL, '', 'pembersih-porselen', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(5, 1, 'Pembersih kloset', NULL, '', 'pembersih-kloset', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(6, 1, 'Pembersih mebel', NULL, '', 'pembersih-mebel', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(7, 1, 'Pembersih karpet', NULL, '', 'pembersih-karpet', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(8, 1, 'Pembersih mobil', NULL, '', 'pembersih-mobil', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(9, 1, 'Pembersih sepatu', NULL, '', 'pembersih-sepatu', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(10, 1, 'Penjernih air', NULL, '', 'penjernih-air', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(11, 1, 'Pembersih Iainnya', NULL, '', 'pembersih-iainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(12, 2, 'Sabun cuci', NULL, '', 'sabun-cuci', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(13, 2, 'Deterjen', NULL, '', 'deterjen', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(14, 2, 'Pelembut cucian', NULL, '', 'pelembut-cucian', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(15, 2, 'Pemutih', NULL, '', 'pemutih', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(16, 2, 'Enzim pencuci', NULL, '', 'enzim-pencuci', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(17, 2, 'Pewangi pakaian', NULL, '', 'pewangi-pakaian', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(18, 2, 'Sabun cuci tangan', NULL, '', 'sabun-cuci-tangan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(19, 2, 'Sediaan untuk mencuci lainnya', NULL, '', 'sediaan-untuk-mencuci-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(20, 3, 'Antiseptika', NULL, '', 'antiseptika', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(21, 3, 'Disinfektan', NULL, '', 'disinfektan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(22, 3, 'Antiseptika dan disinfektan Iainnya', NULL, '', 'antiseptika-dan-disinfektan-iainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(23, 4, 'Pewangi ruangan', NULL, '', 'pewangi-ruangan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(24, 4, 'Pewangi telepon', NULL, '', 'pewangi-telepon', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(25, 4, 'Pewangi mobil', NULL, '', 'pewangi-mobil', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(26, 4, 'Pewangi kulkas', NULL, '', 'pewangi-kulkas', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(27, 4, 'Pewangi lainnya', NULL, '', 'pewangi-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(28, 5, 'Peratatan Neurologi Diagnostik', NULL, '', 'peratatan-neurologi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(29, 5, 'Peralatan Neurologi Bedah', NULL, '', 'peralatan-neurologi-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(30, 5, 'Peralatan Neurotogi Terapetik', NULL, '', 'peralatan-neurotogi-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(31, 6, 'Peralatan THT Diagnostik', NULL, '', 'peralatan-tht-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(32, 6, 'Peralatan THT Prostetik', NULL, '', 'peralatan-tht-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(33, 6, 'Peralatan THT Bedah', NULL, '', 'peralatan-tht-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(34, 6, 'Peralatan THT Terapetik', NULL, '', 'peralatan-tht-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(35, 7, 'Peralatan RSU & P Pemantauan', NULL, '', 'peralatan-rsu-&-p-pemantauan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(36, 7, 'Peralatan RSU & P Terapetik', NULL, '', 'peralatan-rsu-&-p-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(37, 7, 'Peralatan RSU & P Lainnya', NULL, '', 'peralatan-rsu-&-p-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(38, 8, 'Peralatan Ortopedi Diagnostik', NULL, '', 'peralatan-ortopedi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(39, 8, 'Peralatan Ortopedi Prostetik', NULL, '', 'peralatan-ortopedi-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(40, 8, 'Peralatan Ortopedi Bedah', NULL, '', 'peralatan-ortopedi-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(41, 9, 'Dot dan sejenisnya', NULL, '', 'dot-dan-sejenisnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(42, 9, 'Popok bayi', NULL, '', 'popok-bayi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(43, 9, 'Botol susu', NULL, '', 'botol-susu', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(44, 9, 'AIat perawatan bayi lainnya', NULL, '', 'aiat-perawatan-bayi-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(45, 10, 'Peralatan Radiologi Diagnostik', NULL, '', 'peralatan-radiologi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(46, 10, 'Peralatan Radiologi Terapetik', NULL, '', 'peralatan-radiologi-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(47, 10, 'Peralatan Radiologi Lainnya', NULL, '', 'peralatan-radiologi-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(48, 11, 'Peralatan Gigi Diagnostik', NULL, '', 'peralatan-gigi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(49, 11, 'Peralatan Gigi Prostetik', NULL, '', 'peralatan-gigi-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(50, 11, 'Peralatan Gigi Bedah', NULL, '', 'peralatan-gigi-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(51, 11, 'Peralatan Gigi Terapetik', NULL, '', 'peralatan-gigi-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(52, 11, 'Peralatan Gigi Lainnya', NULL, '', 'peralatan-gigi-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(53, 12, 'Pewarna Biological', NULL, '', 'pewarna-biological', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(54, 12, 'Produk Kultur Sel dan Jaringan', NULL, '', 'produk-kultur-sel-dan-jaringan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(55, 12, 'Peralatan dan Asesori Patologi', NULL, '', 'peralatan-dan-asesori-patologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(56, 12, 'Pereaksi Penyedia Specimen', NULL, '', 'pereaksi-penyedia-specimen', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(57, 12, 'Peralatan Hematologi Otomatis dan Semi Otomatis', NULL, '', 'peralatan-hematologi-otomatis-dan-semi-otomatis', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(58, 12, 'Peralatan Hematologi Manual', NULL, '', 'peralatan-hematologi-manual', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(59, 12, 'Paket dan Kit hematologi', NULL, '', 'paket-dan-kit-hematologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(60, 12, 'Pereaksi Hematologi', NULL, '', 'pereaksi-hematologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(61, 12, 'Produk yang digunakan dalam pembuatan sediaan darah dan sediaan berasal dan darah', NULL, '', 'produk-yang-digunakan-dalam-pembuatan-sediaan-darah-dan-sediaan-berasal-dan-darah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(62, 13, 'Kapas kecantikan', NULL, '', 'kapas-kecantikan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(63, 13, 'Facial tissue', NULL, '', 'facial-tissue', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(64, 13, 'Toilet tissue', NULL, '', 'toilet-tissue', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(65, 13, 'Tissue basah', NULL, '', 'tissue-basah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(66, 13, 'Tissue makan', NULL, '', 'tissue-makan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(67, 13, 'Cotton bud', NULL, '', 'cotton-bud', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(68, 13, 'Paper towel', NULL, '', 'paper-towel', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(69, 13, 'Tissue dan kapas lainnya', NULL, '', 'tissue-dan-kapas-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(70, 1, '', NULL, '', '', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(71, 15, 'Peralatan Diagnostika', NULL, '', 'peralatan-diagnostika', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(72, 15, 'Peralatan Mikrobiologi', NULL, '', 'peralatan-mikrobiologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(73, 15, 'Pereaksi Serologi', NULL, '', 'pereaksi-serologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(74, 15, 'Perlengkapan dan Pereaksi Laboratorium Imunologi', NULL, '', 'perlengkapan-dan-pereaksi-laboratorium-imunologi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(75, 15, 'Sistem Tes Imunologikal', NULL, '', 'sistem-tes-imunologikal', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(76, 15, 'Sistem Tes Imunologikal Antigen Tumor', NULL, '', 'sistem-tes-imunologikal-antigen-tumor', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(77, 16, 'Peralatan Bedah Diagnostik', NULL, '', 'peralatan-bedah-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(78, 16, 'Peratatan Bedah Prostetik', NULL, '', 'peratatan-bedah-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(79, 16, 'Peralatan Bedah', NULL, '', 'peralatan-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(80, 16, 'Peratatan Bedah Terapetik', NULL, '', 'peratatan-bedah-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(81, 17, 'Peralatan OG Diagnostik', NULL, '', 'peralatan-og-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(82, 17, 'Peralatan OG Pemantauan', NULL, '', 'peralatan-og-pemantauan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(83, 17, 'Peralatan OG Prostetik', NULL, '', 'peralatan-og-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(84, 17, 'Peralatan OG Bedah', NULL, '', 'peralatan-og-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(85, 17, 'Peralatan OG Terapetik', NULL, '', 'peralatan-og-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(86, 17, 'Peralatan Bantu Reproduksi', NULL, '', 'peralatan-bantu-reproduksi', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(87, 18, 'Peralatan Kardiologi Diagnostik', NULL, '', 'peralatan-kardiologi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(88, 18, 'Peralatan Kardiotogi Pemantauan', NULL, '', 'peralatan-kardiotogi-pemantauan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(89, 18, 'Peralatan Kardiologi Prostetik', NULL, '', 'peralatan-kardiologi-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(90, 18, 'Peralatan Kardiologi Bedah', NULL, '', 'peralatan-kardiologi-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(91, 18, 'Peratatan Kardiologi Terapetik', NULL, '', 'peratatan-kardiologi-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(92, 19, 'Peralatan GU Diagnostik', NULL, '', 'peralatan-gu-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(93, 19, 'Peralatan GU Pemantauan', NULL, '', 'peralatan-gu-pemantauan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(94, 19, 'Peralatan GU Prostetik', NULL, '', 'peralatan-gu-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(95, 19, 'Peralatan GU Bedah', NULL, '', 'peralatan-gu-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(96, 19, 'Peralatan GU Terapetik', NULL, '', 'peralatan-gu-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(97, 20, 'Peralatan Mata Diagnostik', NULL, '', 'peralatan-mata-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(98, 20, 'Peralatan Mata Prostetik', NULL, '', 'peralatan-mata-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(99, 20, 'Peralatan Mata Bedah', NULL, '', 'peralatan-mata-bedah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(100, 20, 'Peralatan Mata Terapetik', NULL, '', 'peralatan-mata-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(101, 21, 'Pengendali serangga', NULL, '', 'pengendali-serangga', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(102, 21, 'Pencegah serangga', NULL, '', 'pencegah-serangga', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(103, 21, 'Pengendali kutu rambut', NULL, '', 'pengendali-kutu-rambut', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(104, 21, 'Pengendali kutu binatang peliharaan (bukan ternak)', NULL, '', 'pengendali-kutu-binatang-peliharaan-(bukan-ternak)', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(105, 21, 'Pengendali tikus rumah', NULL, '', 'pengendali-tikus-rumah', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(106, 21, 'Pestisida rumah tangga Iainnya', NULL, '', 'pestisida-rumah-tangga-iainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(107, 22, 'Peralatan Anestesi Diagnostik', NULL, '', 'peralatan-anestesi-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(108, 22, 'Peralatan Anestesi Pemantauan', NULL, '', 'peralatan-anestesi-pemantauan', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(109, 22, 'Peralatan Anestesi Terapetik', NULL, '', 'peralatan-anestesi-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(110, 22, 'Peralatan Anestesi Lainnya', NULL, '', 'peralatan-anestesi-lainnya', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(111, 23, 'Sistem Tes Kimia Klinik', NULL, '', 'sistem-tes-kimia-klinik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(112, 23, 'Peralatan Laboratorium Klinik', NULL, '', 'peralatan-laboratorium-klinik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(113, 23, 'Sistem Tes Toksikologi Klinik', NULL, '', 'sistem-tes-toksikologi-klinik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(114, 24, 'Peralatan Kesehatan Fisik Diagnostik', NULL, '', 'peralatan-kesehatan-fisik-diagnostik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(115, 24, 'Peralatan Kesehatan Fisik Prostetik', NULL, '', 'peralatan-kesehatan-fisik-prostetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1),
(116, 24, 'Peratatan Kesehatan Fisik terapetik', NULL, '', 'peratatan-kesehatan-fisik-terapetik', 1, '2016-03-08 14:00:00.000000', 1, '2016-03-08 14:00:00.000000', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
