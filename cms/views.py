from models import Content
from django.shortcuts import render_to_response
from django.http import HttpResponse, Http404
from django.template import loader
from category.models import Category


def page(request, slug):
    categories = Category.objects.filter(is_active=1).order_by('sort_order')
    try:
        content = Content.get_content(slug)
    except Content.DoesNotExist:
        return render_to_response('404.html')

    print request

    context = {'slug': slug,
               'categories': categories,
               'content': content,
               'static_page': True}

    template = loader.get_template('web-v2/static.html') #('web/index.html')
    html = template.render(context, request)

    return HttpResponse(html)


def page_not_found(request):
    categories = Category.objects.filter(is_active=1).order_by('sort_order')
    content = Content.get_content('404')

    context = {'categories': categories,
               'content': content,
               'static_page': True,}

    template = loader.get_template('web-v2/home.html') #('web/index.html')
    html = template.render(context, request)

    return HttpResponse(html)
