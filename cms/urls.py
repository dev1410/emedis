from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^page/(?P<slug>[\w-]+)/$', views.page, name='static_page'),
    url(r'^404', views.page_not_found, name='static_page'),
]
