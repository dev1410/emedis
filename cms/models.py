from django.db import models
from django.contrib.auth.models import User
from django.core.cache import cache
from django.conf import settings
from ckeditor.fields import RichTextField


class Content(models.Model):
    title = models.CharField(max_length=55,
                             blank=False,
                             null=False,
                             help_text='Content Title')
    meta_description = models.CharField(max_length=160,
                                        blank=True,
                                        null=True,
                                        help_text='Content Meta Description')
    content = RichTextField(config_name='full',
                            blank=False,
                            null=False,
                            help_text='Content')
    slug = models.SlugField(unique=True)
    is_active = models.BooleanField(default=False,
                                    help_text='Content Active')
    created_date = models.DateTimeField(auto_now_add=True)
    created_user = models.ForeignKey(User, related_name='content_creator')
    updated_date = models.DateTimeField(auto_now=True)
    updated_user = models.ForeignKey(User, related_name='content_updater', null=True)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        cache.set(self.slug, self, 3600 * 24 * 30)

        super(Content, self).save(*args, **kwargs)

    @staticmethod
    def get_content(slug):
        content = cache.get(slug)

        if content is None:
            content = Content.objects.get(slug=slug)
            cache.set(slug, content, settings.CACHE_TIME)

        return content
