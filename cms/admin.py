from django.contrib import admin
from models import Content


class ContentAdmin(admin.ModelAdmin):
    list_display = ('title',
                    'meta_description',
                    'content',
                    'is_active',
                    'slug',)
    fieldsets = (
        ('Content', {'fields': ('title', 'meta_description', 'content',)}),
        ('Additional', {'fields': ('slug', 'is_active')})
    )
    prepopulated_fields = {'slug': ('title',)}

    def save_model(self, request, obj, form, change):
        if Content.objects.filter(slug=request.POST['slug']).exists():
            obj.updated_user = request.user
            obj.save()
        else :
            obj.created_user = request.user
            obj.save()

admin.site.register(Content, ContentAdmin)