# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Content',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('publish_date', models.DateField()),
                ('title', models.CharField(max_length=55)),
                ('meta_description', models.CharField(max_length=160, null=True, blank=True)),
                ('content', models.TextField()),
                ('slug', models.SlugField()),
                ('user', models.ForeignKey(related_name='content_creator', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
