# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cms', '0004_auto_20160315_1440'),
    ]

    operations = [
        migrations.RenameField(
            model_name='content',
            old_name='is_acvtive',
            new_name='is_active',
        ),
    ]
