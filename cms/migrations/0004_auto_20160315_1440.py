# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
import ckeditor.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('cms', '0003_auto_20160315_1331'),
    ]

    operations = [
        migrations.RenameField(
            model_name='content',
            old_name='user',
            new_name='created_user',
        ),
        migrations.RemoveField(
            model_name='content',
            name='publish_date',
        ),
        migrations.AddField(
            model_name='content',
            name='created_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 15, 14, 40, 18, 96000), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='content',
            name='is_acvtive',
            field=models.BooleanField(default=False, help_text=b'Content Active'),
        ),
        migrations.AddField(
            model_name='content',
            name='updated_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 3, 15, 14, 40, 28, 502000), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='content',
            name='updated_user',
            field=models.ForeignKey(related_name='content_updater', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='content',
            field=ckeditor.fields.RichTextField(help_text=b'Content'),
        ),
        migrations.AlterField(
            model_name='content',
            name='meta_description',
            field=models.CharField(help_text=b'Content Meta Description', max_length=160, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='content',
            name='title',
            field=models.CharField(help_text=b'Content Title', max_length=55),
        ),
    ]
