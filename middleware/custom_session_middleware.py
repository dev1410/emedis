from django.conf import settings
from django.contrib.sessions.middleware import SessionMiddleware
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from common.utils.authentication import Authentication


class CustomSessionMiddleware(SessionMiddleware):
    def process_request(self, request):
        if self.is_login_page(request=request) and Authentication.get_authentication_session(request=request) is not None:
            return HttpResponseRedirect(reverse('web-index', kwargs={}))

        if self.page_need_auth(request=request) and Authentication.get_authentication_session(request=request) is None:
            return HttpResponseRedirect(reverse('login', kwargs={}))

        print '{0} - {1}'.format(request.path, request.session.session_key)

    @staticmethod
    def page_need_auth(request):
        result = False
        paths = [x.lower() for x in request.path.split('/') if x.lower() != 'beta' and x != '']

        try:
            if 'admin' in paths:
                pass
            elif paths[0] in settings.PAGE_WITH_PERMISSIONS:
                result = True
        except IndexError:
            pass

        return result

    @staticmethod
    def is_login_page(request):
        result = False
        paths = [x.lower() for x in request.path.split('/') if x.lower() != 'beta' and x != '']

        try:
            if paths[0] == 'login':
                result = True
        except IndexError:
            pass

        return result
