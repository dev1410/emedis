from htmlmin.minify import html_minify
from django.conf import settings


class MinifyWithCaching:
    def process_response(self, request, response):
        try:
            if 'text/html' in response['Content-Type'] and settings.MINIFY_HTML:
                response.content = html_minify(response.content)
        except KeyError:
            pass

        return response
