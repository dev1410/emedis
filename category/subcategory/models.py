from django.db import models
from django.contrib.auth.models import User
from django.db.models import IntegerField, CharField, SlugField, TextField, DateTimeField, BooleanField, ImageField, ForeignKey
from ckeditor.fields import RichTextField
from ..models import Category


class Subcategory(models.Model):
    class Meta:
        verbose_name_plural = 'Subcategories'

    category = models.ForeignKey(Category, on_delete = models.CASCADE)
    name = CharField(
        max_length = 84,
        help_text = 'Product Subcategory name')
    subcategory_icon = ImageField(
        upload_to = 'images/subcategory/icon',
        null=True, blank=True)
    description = RichTextField(config_name='full', blank = True)
    slug = SlugField(
        max_length = 256,
        help_text = 'Subcategory Slug field')
    is_active = BooleanField(
        default = False,
        help_text = 'Subcategory active status')
    created_date =  DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='subcategory_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, related_name='subcategory_updated_user', null=True)

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):

        super(Subcategory, self).save(*args, **kwargs)

        # Elastic Integrate
        # from search.engine import Update, Create
        # from settings import ELASTIC_CONNECTION
        #
        # index_exists = ELASTIC_CONNECTION.exists(index='subcategory', doc_type='subcategory', id=self.id)
        #
        # if index_exists is True:
        #     Update(index='subcategory', doc_type='subcategory', doc_id=self.id)
        # else:
        #     Create(index='subcategory', doc_type='subcategory', doc_id=self.id)

    def delete(self, *args, **kwargs):

        super(Subcategory, self).delete(*args, **kwargs)

        # Elastic Integrate
        from search.engine import Delete

        Delete(index='subcategory', doc_type='subcategory', doc_id=self.id)