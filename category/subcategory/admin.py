from django.contrib import admin

from product.library.image import AdminImageWidget
from .models import Subcategory

class SubcategoryAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'description', 'slug', 'subcategory_icon', 'is_active',
        'created_date', 'updated_date')

    list_filter = ('created_date', 'updated_date')

    search_fields = ['name']

    fieldsets = [
        ('General Information',
         {'fields':
          ['name', 'subcategory_icon', 'category', 'description', 'slug', 'is_active']
          })
    ]

    prepopulated_fields = {'slug': ('name',)}

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'subcategory_icon':
            kwargs['widget'] = AdminImageWidget

            try:
                del kwargs['request']
            except KeyError:
                pass

            return db_field.formfield(**kwargs)
        return super(SubcategoryAdmin, self).formfield_for_dbfield(db_field, **kwargs)

    def save_model(self, request, obj, form, change):
        if Subcategory.objects.filter(slug=request.POST['slug']).exists():
            obj.updated_user = request.user
            obj.save()
        else :
            obj.created_user = request.user
            obj.save()
