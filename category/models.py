from django.contrib.auth.models import User
from django.db.models import IntegerField, CharField, SlugField, TextField, DateTimeField, BooleanField, ImageField, ForeignKey
from django.db import models
from ckeditor.fields import RichTextField

from unspsc.family.models import Family


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Categories'

    id = models.AutoField(primary_key=True)
    name = CharField(
        max_length=84,
        help_text='Product Category name'
    )
    # family = models.ForeignKey(
    #     Family,
    #     null=True,
    #     blank=True
    # )
    sort_order = models.SmallIntegerField(
        default=1,
        help_text='Category sort order'
    )
    category_help_text = CharField(
        max_length=100,
        blank=True,
        null=True,
        help_text='Category help text, if this field empty the help text will be "name" field'
    )
    description = RichTextField(config_name='full', blank = True)
    category_icon = ImageField(upload_to = 'images/category/icon', null=True, blank=True)
    # unpsc_family
    slug = SlugField(
        max_length=256,
        help_text='Category Slug field')
    is_active = BooleanField(
        default=False,
        help_text='Category active status')
    created_date = DateTimeField(auto_now_add=True)
    created_user = ForeignKey(User, related_name='category_created_user')
    updated_date = DateTimeField(auto_now=True)
    updated_user = ForeignKey(User, related_name='category_updated_user', null=True)

    def __unicode__(self):
        return self.name

    def save(self,*args, **kwargs):

        super(Category, self).save(*args, **kwargs)

        # Elastic Integrate
        # from search.engine import Update, Create
        # from settings import ELASTIC_CONNECTION
        #
        # index_exists = ELASTIC_CONNECTION.exists(index='category', doc_type='category', id=self.id)
        #
        # if index_exists is True:
        #     Update(index='category', doc_type='category', doc_id=self.id)
        # else:
        #     Create(index='category', doc_type='category', doc_id=self.id)

    def delete(self, *args, **kwargs):

        super(Category, self).delete(*args, **kwargs)

        # Elastic Integrate
        from search.engine import Delete
        Delete(index='category', doc_type='category', doc_id=self.id)

    def create_category_menu(self):
        categories = self.objects.filter(is_active=1)

        return categories
