# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import ckeditor.fields
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('family', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(help_text=b'Product Category name', max_length=84)),
                ('sort_order', models.SmallIntegerField(default=1, help_text=b'Category sort order')),
                ('category_help_text', models.CharField(help_text=b'Category help text, if this field empty the help text will be "name" field', max_length=100, null=True, blank=True)),
                ('description', ckeditor.fields.RichTextField(blank=True)),
                ('category_icon', models.ImageField(null=True, upload_to=b'images/category/icon', blank=True)),
                ('slug', models.SlugField(help_text=b'Category Slug field', max_length=256)),
                ('is_active', models.BooleanField(default=False, help_text=b'Category active status')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('created_user', models.ForeignKey(related_name='category_created_user', to=settings.AUTH_USER_MODEL)),
                ('family', models.ForeignKey(blank=True, to='family.Family', null=True)),
                ('updated_user', models.ForeignKey(related_name='category_updated_user', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Categories',
            },
        ),
        migrations.CreateModel(
            name='Subcategory',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True)),
                ('name', models.CharField(help_text=b'Product Subcategory name', max_length=84)),
                ('subcategory_icon', models.ImageField(null=True, upload_to=b'images/subcategory/icon', blank=True)),
                ('description', ckeditor.fields.RichTextField(blank=True)),
                ('slug', models.SlugField(help_text=b'Subcategory Slug field', max_length=256)),
                ('is_active', models.BooleanField(default=False, help_text=b'Subcategory active status')),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('updated_date', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(to='category.Category')),
                ('created_user', models.ForeignKey(related_name='subcategory_created_user', to=settings.AUTH_USER_MODEL)),
                ('updated_user', models.ForeignKey(related_name='subcategory_updated_user', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name_plural': 'Subcategories',
            },
        ),
    ]
