/**
 * Created by dev1410 on 18/10/16.
 */
$(document).ready(function () {

    var getCookie = function (name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== ''){
            var cookies = document.cookie.split(';');
            for (var i=0; i<cookies.length; i++){
                var cookie = jQuery.trim(cookies[i]);

                if (cookie.substring(0, name.length + 1) === (name + '=')){
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

    var csrfSafeMethod = function (method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    };

    var sameOrigin = function (url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    };

    var CSRFToken = getCookie('csrftoken');
    var FORM = '#subscribe-form';
    var INPUT_FIELD = '#subscribe_email_input';
    var AJAX_URL = '/subscribe/';
    var AJAX_METHOD = 'POST';
    var CSRF_HTTP_HEADER = 'X-CSRFToken';
    var SUBMIT_BUTTON = '#ses';
    var DEBUG = true;
    var THANKS_MESSAGE = 'Thank you for your subscription.';
    var ALREADY_REGISTERED = 'Your email already subscribed.';
    var dataType = 'json';

    var after_ajax = function (response) {
        $(INPUT_FIELD).prop('disabled', true);

        if (response.status == 201){
            $(INPUT_FIELD).val(THANKS_MESSAGE);
        } else {
            $(INPUT_FIELD).val(ALREADY_REGISTERED);
        }

        $(SUBMIT_BUTTON).prop('disabled', true);
    };

    $(SUBMIT_BUTTON).click(function () {
        $(FORM).submit(function (event) {
            event.preventDefault();
            var email_value = $(INPUT_FIELD).val();
            $.ajax({
                type: AJAX_METHOD,
                url: AJAX_URL,
                dataType: dataType,
                data: {'email': email_value},
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                        // Send the token to same-origin, relative URLs only.
                        // Send the token only if the method warrants CSRF protection
                        // Using the CSRFToken value acquired earlier
                        xhr.setRequestHeader(CSRF_HTTP_HEADER, CSRFToken);
                    }
                },
                success: function (response) {

                    after_ajax(response);

                    if (DEBUG == true){
                        console.log(response);
                        console.log('success');
                    }
                },
                error : function (jqXHR, textStatus, errorThrown) {

                    if (DEBUG == true){
                        console.log(textStatus);
                        console.log(errorThrown);
                    }
                }
            });
            return false;
        });
    });
});