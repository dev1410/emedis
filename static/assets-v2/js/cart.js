/**
 * Created by santo on 4/4/2016.
 */
$(document).ready(function (){
    update_shopping_cart(true);
});
$(document).on('click','.minus-cart-qty', function(e){
    e.stopPropagation();
    e.preventDefault();
    update_qty_on_event($(this), '-');
});
$(document).on('click','.plus-cart-qty', function(e){
    e.stopPropagation();
    e.preventDefault();
    update_qty_on_event($(this), '+');
});
$(document).on('change', '.number-only.form-control.input-sm', function (e) {
    this.value = this.value.replace(/[^0-9]/g,'');
    var element = this;
    var e = $(this);

    if (this.value.trim() != '') {
        var timeout = setTimeout(function() {
            update_qty_on_event(e, '');
            clearTimeout(timeout);
        }, 1000);
    }
});
$(document).on('click','#checkout_confirmation_button', function(e){
    if ($('#checkout_cart_total').data('checkout-total') <= 0) {
        e.preventDefault();
        if ( !$('#cart_update_fail_notification').is( '.in' ) ) {
            $('#cart_update_fail_notification').addClass('in');
            setTimeout(function() {
                $('#cart_update_fail_notification').removeClass('in');
            }, 10000);
        };
    }
});
$(document).on('click','.checkout-cart-row', function(e){
    e.preventDefault();
    var row = $(this);
    $.ajax({
            method: 'GET',
            url: '/cart/delete/',
            data: { 'product_id': row.data('product-id') }
        })
        .done(function (response) {
            $('tr#checkout-cart-' + row.data('product-id')).remove();
            if ($('#miyazaki tbody tr').length == 1) {
                checkout_page_hide(true);
            }
            update_shopping_cart(false);
        })
});
function update_checkout_cart_table(cart_details) {
    var shopping_cart_table_list_items = [];
    var total_qty = 0;
    var total = 0;

    $('#checkout_cart_table_notification').hide();

    $('.hasil-pencarian2').show();
    $('.barang-ditemukan').text('(' + cart_details.length + ' produk)');

    $.each(cart_details, function (key, value) {
        var json_cart_detail = cart_details[key];
        var product_number_link = '';
        var subtotal = cart_details[key]['product_qty'] * cart_details[key]['private_price'];
        shopping_cart_table_list_items.push('<tr id="checkout-cart-' + cart_details[key]['product_id'] + '" >' +
            '<td class="closeicon-table">' +
            '<a class="checkout-cart-row" data-product-id="' + cart_details[key]['product_id'] + '" href="#">' +
            '<img src="/static/assets-v2/images/close-icon.png"/>' +
            '</a>' +
            '</td>' +
            '<td height="100" width="100">' +
            '<center>' +
            '<a target="_blank" href="' + cart_details[key]['product_slug'] + '/">' +
            '<img class="productimg" src="' + cart_details[key]['product_image_url'] + '" /></a>' +
            '</center>' +
            '</td>' +
            '<td>' +
            '<center class="productname-cart"><a target="_blank" href="' + cart_details[key]['product_slug'] + '/">' + cart_details[key]["product_name"] + '</a></center>' +
            '<br />' + cart_details[key]['product_number_link'] +
            '</td>' +
            '<td>' +
            '<center>' + ((cart_details[key]['private_price'] == 0) ? 'n/a' : $.number(cart_details[key]['private_price'], 0)) + '</center>' +
            '</td>' +
            '<td>' +
            '<center>' +
            '<form>' +
            '<div data-product-id="' + cart_details[key]['product_id'] + '" class="value-button minus-cart-qty" id="decrease" value="Decrease Value">-</div>' +
            '<input type="number" id="product-id-' + cart_details[key]['product_id'] + '" class="number" value="' + cart_details[key]['product_qty'] + '"/>' +
            '<div data-product-id="' + cart_details[key]['product_id'] + '" class="value-button plus-cart-qty" id="increase" value="Increase Value">+</div>' +
            '</form>' +
            '</center>' +
            '</td>' +
            '<td>' +
            '<center class="subharga">' + ((subtotal == 0) ? 'n/a' : $.number(subtotal, 0)) + '</center>' +
            '</td>' +
            '</tr>'
        );
        total_qty += cart_details[key]['product_qty'];
        total += (cart_details[key]['private_price'].replace(',', '') * cart_details[key]['product_qty']);
    });
    shopping_cart_table_list_items.push('<tr>' +
        '<td></td>' +
        '<td class="hargatotal" height="100" width="100">Harga Total: </td>' +
        '<td></td><td></td><td></td>' +
        '<td class="hargatotal">' +
        '<center>' +
        '<span class="price old-price-emedis"><span id="checkout_cart_total" class=""></span></span>' +
        '</center>' +
        '</td>' +
        '</tr>' +
        '<tr></tr>');
    $('#miyazaki tbody').append(shopping_cart_table_list_items.join(''));
    $('#checkout_cart_total').text('IDR ' + $.number(total, 0));
    $('#checkout_cart_total2').text('IDR ' + $.number(total, 0));
    $('#checkout_cart_total').data('checkout-total', total);
}
function update_shopping_cart(is_update_cart) {
    $.ajax({
            method: 'GET',
            url: '/cart/get/',
            data: { }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var product_count = json_data['count'];
            var total_qty = 0;
            var cart_session = json_data['cart_session'];
            var cart_details = json_data['cart_details'];
            var shopping_cart_list_items = [];
            var total = 0;

            $.each(cart_details, function (key, value) {
                var json_cart_detail = cart_details[key];

                shopping_cart_list_items.push('<li class="product-info" id="row-' + cart_details[key]['product_id'] + '">' +
                    '<div class="p-left">' +
                    '<a href="#" class="checkout-cart-row" data-product-id="' + cart_details[key]['product_id'] + '"><img src="/static/assets-v2/images/close-icon.png" class="closeemedis"></a>' +
                    '<a target="_blank" href="' + cart_details[key]["product_slug"] + '/"><img class="productimg" src="' + cart_details[key]['product_image_url'] + '">' + cart_details[key]['product_name'] + '</a>' +
                    '</div>' +
                    '<div class="p-right">' +
                    '<p class="productname-cart p-name"><a href="' + cart_details[key]["product_slug"] + '/">' + cart_details[key]['product_name'] + '</a></p>' +
                    '<p class="p-rice">' + ((cart_details[key]['private_price'] == 0) ? 'N/A' : $.number(cart_details[key]['private_price'], 0)) + '</p>' +
                    '<p class="quantity">Jumlah X ' + cart_details[key]['product_qty'] + '</p>' +
                    '</div>' +
                    '</li>');

                total_qty += cart_details[key]['product_qty'];
                total += (cart_details[key]['private_price'].replace(',', '') * cart_details[key]['product_qty']);
            });
            if (total_qty > 0) {
                checkout_page_hide(false);
                $('.total-cart').show();
                $('#no_item_on_cart').hide();
                if (is_update_cart == true) {
                    checkout_page_hide(false);
                    update_checkout_cart_table(cart_details);
                }
                $('#cart-checkout-button').show();
                $('#total-cart-message').show();
            } else {
                $('.cart-title').text('Anda belum melakukan pembelian');
                checkout_page_hide(true);
                $('#cart-checkout-button').hide();
                $('.total-cart').hide();
                $('#no_item_on_cart').show();
            }
            $('#cart_total_qty_badge').text(total_qty).show();
            $('#total-qty-box').text(total_qty + ' barang - ');
            $('#shopping_cart_list').html(shopping_cart_list_items.join(''));
            $('#shopping_cart_total').text($.number(total, 0));
            $('#total-price').text('IDR ' + $.number(total, 0));
        })
        .fail(function (response) {
            $('.cart-title').text('Anda belum melakukan pembelian');
            $('#cart-checkout-button').hide();
            $('.order-detail-content').hide();
            $('#total-cart-message').hide();
            $('#checkout_cart_table_notification').show();
            $('#cart_navigation').hide();
            $('.total-cart').hide();
            $('#no_item_on_cart').show();
        });
}
$('#cart_success_continue_button').click(function(e) {
    e.preventDefault();
    $('#cart_success_notification').toggleClass('in');
});
$('#cart_success_checkout_button').click(function(e) {
    window.location.replace($(this).data('url'));
});
$('.add-product-to-cart').click(function(e) {
    var qty = 1;
    var type = $(this).data('type');
    e.preventDefault();
    if (is_numeric($('#number').val())) {
        qty = $("#number").val();
    }
    $.ajax({
            method: 'GET',
            url: '/cart/add/',
            data: { product_id: $(this).data('product-id'), qty: qty }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var status = json_data['status'];
            var message = json_data['message'];
            $('#cart_success_message').text('');
            $('#cart_success_notification').toggleClass('in');

            if ($('.cart-block-content').length > 0) {
                update_shopping_cart(true);
            }
            if (type == 'buy') {
                location.href = '/checkout/';
            } else if (type == 'list-buy') {
                confirmation_box();
            }
        })
        .fail(function (jqXHR, response) {
            var json_data = $.parseJSON(jqXHR.responseText);
            var json_data = jqXHR.responseText;
            if ( !$('#cart_update_fail_notification').is( '.in' ) ) {
                $('#cart_update_fail_message').text('Coba ulangi kembali atau segera hubungi call center kami');
                $('#cart_update_fail_notification').addClass('in');
                setTimeout(function() {
                    $('#cart_update_fail_notification').removeClass('in');
                }, 3200);
            };
        });
});
function checkout_page_hide(is_hide) {
    if (is_hide == true) {
        $('.hasil-pencarian2').hide();
        $('.product-cart').hide();
        $('#checkout_cart_table_notification').show();
    } else {
        $('.barang-ditemukan').text('(' + ($('#miyazaki tbody tr').length - 1) + ' produk)');
        $('.hasil-pencarian2').show();
        $('.product-cart').show();
        $('#checkout_cart_table_notification').hide();
    }
}
function update_cart_qty(product_id, qty) {
    $.ajax({
            method: 'GET',
            url: '/cart/add/',
            data: { product_id: product_id, qty: qty }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var status = json_data['status'];
            var message = json_data['message'];

            update_shopping_cart(false);
        })
}
function is_numeric(number) {
    var RE = /^\d*\d+$/;
    return (RE.test(number));
}
function update_qty_on_event(e, update_sign) {
    var number_id = 'product-id-' + e.data('product-id');
    var number_element = $('#' + number_id);
    if (is_numeric(number_element.val())) {
        if ((update_sign == '-') && (number_element.val() > 1)) {
            number_element.val(parseInt(number_element.val()) - 1);
        } else if (update_sign == '+') {
            number_element.val(parseInt(number_element.val()) + 1);
        }
    }
    update_cart_qty(e.data('product-id'), number_element.val());
}
function confirmation_box() {
    $( function() {
        $( "#dialog-confirm" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Lanjutkan Belanja": function() {
                    $(this).dialog("close");
                },
                "Teruskan ke Checkout": function() {
                    $(this).dialog("close");
                    location.href = '/checkout/';
                }
            }
        });
    } );
}