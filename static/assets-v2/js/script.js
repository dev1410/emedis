/**
 * Created by santo on 26/10/2016.
 */
$("#product_search_submit").click(function(e){
    e.preventDefault();
    product_search_submit_text = $("input[name='search-input']").val().replace(' ', '-').toLowerCase();
    if (product_search_submit_text != '') {
        window.location="/search/" + product_search_submit_text + "/";
    } else {
        $("input[name='search-input']").focus();
    }
});