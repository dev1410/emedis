/**
 * Created by santo on 05/09/2016.
 */
$(document).ready(function (){
    update_best_seller_list();
    update_brand_product_related();
});

function update_best_seller_list() {
    $.ajax({
            method: 'GET',
            url: '/best_seller/',
            data: { }
        })
        .done(function (response) {
            var best_seller_item_list = [];
            var json_data = response;
            var product_count = json_data['count'];
            var result = json_data['result'];
            var code = json_data['code'];
            if (code == 200) {
                $.each(result, function (key, value) {
                    var product = result[key];

                    best_seller_item_list.push('<center>' +
                        '<div class="col-md-12 productbox">' +
                        '<div id="button-hover">' +
                        '<button class="btn color-4 material-design button-payment" data-color="#d2181c"><span class="button-text">Beli</span>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="250"></canvas>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="250"></canvas>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="167"></canvas>' +
                        '<canvas width="30" height="38" style="width: 100%; height: 100%;"></canvas>' +
                        '</button>' +
                        '</div>' +
                        '<div class="product-container">' +
                        '<div class="left-block">' +
                        '<a href="' + product['product_slug'] + '"><img alt="' + product['product_name'] + '" title="' + product['product_name'] + '"class="img-responsive" src="' + product['image_url'] + '"></a>' +
                        '<div class="quick-view">' +
                        '<a class="heart" href="#" title="Add to my wishlist"></a>' +
                        '<a class="compare" href="#" title="Add to compare"></a>' +
                        '<a class="search" href="#" title="Quick view"></a>' +
                        '</div>' +
                        '</div>' +
                        '<div class="right-block">' +
                        '<h5 class="product-name"><a href="' + product['product_slug'] + '">' + product['product_name'] + '</a></h5>' +
                        '<div class="content-price-emedis">' +
                        '<div class="price product-price-emedis">emedis</div><span class="price old-price-emedis"><span class="">' + product['emedis_price_currency'] + ' ' + product['emedis_price'] + '</span>' + product['emedis_price_thousands'] + '</span>' +
                        '</div>' +

                        '</div>' +
                        '</div>' +
                        '</center>')
                });
                        //'<div class="content-price-lkpp">' +
                        //'<div class="price product-price-lkpp">lkpp</div><span class="price old-price-lkpp">' + product['ekatalog_price_currency'] + ' ' + product['ekatalog_price'] + '</span></div>' +
                        //'</div>' +
                $("#product_best_seller_list").html($("#product_best_seller_list").html() + best_seller_item_list.join(''));
                $("#product_best_seller_list").show();
            }
        })
        .fail(function (response) {
            $("#product_best_seller_list").hide();
        });
}

function update_brand_product_related() {
    $.ajax({
            method: 'GET',
            url: '/brand_product_related/hitachi/',
            data: { }
        })
        .done(function (response) {
            var brand_product_related_list = [];
            var json_data = response;
            var product_count = json_data['count'];
            var result = json_data['result'];
            var code = json_data['code'];

            if (code == 200) {
                brand_product_related_list.push('<ul class="product-list owl-carousel owl-theme owl-loaded" data-autoplayhoverpause="true" data-autoplaytimeout="1000" data-dots="false" data-loop="true" data-margin="30" data-nav="true" data-responsive="{\'0\':{\'items\':1},\'600\':{\'items\':3},\'1000\':{\'items\':3}}">' +
                    '<div class="owl-stage-outer">' +
                    '<div class="owl-stage" style="transform: translate3d(-1210.67px, 0px, 0px); transition: 0.3s; width: 3026.67px;">');
                $.each(result, function (key, value) {
                    var product = result[key];

                    brand_product_related_list.push('<div class="owl-item active" style="width: 272.667px; margin-right: 30px;">' +
                        '<li>' +
                        '<div class="product-container">' +
                        '<center>' +
                        '<div class="col-md-12 productbox">' +
                        '<div id="button-hover">' +
                        '<button class="btn color-4 material-design button-payment" data-color="#d2181c"><span class="button-text">Beli</span>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="250"></canvas>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="250"></canvas>' +
                        '<canvas height="38" style="width: 100%; height: 100%;" width="167"></canvas>' +
                        '<canvas width="0" height="0" style="width: 100%; height: 100%;"></canvas>' +
                        '</button>' +
                        '</div>' +
                        '<div class="product-container">' +
                        '<div class="left-block">' +
                        '<a href="' + product['product_slug'] + '"><img alt="' + product['product_name'] + '" class="img-alt" title="' + product['product_name'] + '" src="' + product['image_url'] + '">' + '</a>' +
                        '</div>' +
                        '<div class="right-block">' +
                        '<h5 class="product-name"><a href="' + product['product_slug'] + '">' + product['product_name'] + '</a></h5>' +
                        '<div class="content-price-emedis">' +
                        '<div class="price product-price-emedis">emedis</div><span class="price old-price-emedis"><span>' + product['emedis_price_currency'] + ' ' + product['emedis_price'] + '</span>' + product['emedis_price_thousands'] + '</span>' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        '</center>' +
                        '</div>' +
                        '</li>' +
                        '</div>');
                });
                        //'<div class="content-price-lkpp">' +
                        //'<div class="price product-price-lkpp">lkpp</div><span class="price old-price-lkpp">' + product['ekatalog_price_currency'] + ' ' + product['ekatalog_price'] + '</span></div>' +
                        //'</div>' +
                brand_product_related_list.push('</div>' +
                    '</div>' +
                    '<div class="owl-controls">' +
                    '<div class="owl-nav">' +
                    '<div class="owl-prev" style=""><i class="fa fa-angle-left"></i></div>' +
                    '<div class="owl-next" style=""><i class="fa fa-angle-right"></i></div>' +
                    '</div>' +
                    '<div class="owl-dots" style="display: none;"></div>' +
                    '</div>' +
                    '</ul>');
                $("#brand_product_related_list").html($("#brand_product_related_list").html() + brand_product_related_list.join(''));
                $("#brand_product_related_list").show();
            }
        })
        .fail(function (response) {
            $("#brand_product_related_list").hide();
        });
}
