/**
 * Created by dev1410 on 28/12/15.
 */
(function($) {
    $.fn.custom_prepopulate = function(
        field_id, reference_field_id, reference_field, max_length)
    {
        var field;
        var i;

        field = {
            id: field_id,
            dependency_ids: [],
            dependency_list: [],
            maxLength: max_length
        };

        field['dependency_ids'].push(reference_field_id);
        reference_field = reference_field;

        if ($.isArray(reference_field))
        {
            for (i=0; i<reference_field.length; i++)
            {
                field['dependency_list'].push(reference_field[i]);
            }
        }else{
            field['dependency_list'].push(reference_field);
        }

        $('.empty-form .form-row .field-slug, .empty-form.form-row .field-slug')
            .addClass('prepopulated_field');
        $(field.id).data('dependency_list', field['dependency_list'])
               .prepopulate(field['dependency_ids'], field.maxLength);
    }
})(django.jQuery);
