/**
 * Created by santo on 4/4/2016.
 */
$(document).ready(function (){
    update_shopping_cart(true);
});
$(document).on('click','.minus-cart-qty', function(e){
    e.stopPropagation();
    e.preventDefault();
    update_qty_on_event($(this), '-', 0);
});
$(document).on('click','.plus-cart-qty', function(e){
    e.stopPropagation();
    e.preventDefault();
    update_qty_on_event($(this), '+', 0);
});
$(document).on('change', '.number-only.form-control.input-sm', function (e) {
    this.value = this.value.replace(/[^0-9]/g,'');
    var element = this;
    var e = $(this);

    if (this.value.trim() != '') {
        var timeout = setTimeout(function() {
            update_qty_on_event(e, '');
            clearTimeout(timeout);
        }, 1000);
    }
});
$(document).on('click','#checkout_confirmation_button', function(e){
    if ($('#checkout_cart_total').data('checkout-total') <= 0) {
        e.preventDefault();
        if ( !$('#cart_update_fail_notification').is( '.in' ) ) {
            $('#cart_update_fail_notification').addClass('in');
            setTimeout(function() {
                $('#cart_update_fail_notification').removeClass('in');
            }, 10000);
        };
    }
});
$(document).on('click','.checkout-cart-row', function(e){
    e.preventDefault();
    var row = $(this);
    $.ajax({
            method: 'GET',
            url: '/cart/delete/',
            data: { 'product_id': row.data('product-id') }
        })
        .done(function (response) {
            $('tr#checkout-cart-' + row.data('product-id')).remove();
            if ($('#checkout_cart_table tbody tr').length == 0) {
                checkout_page_hide(true);
            }
            update_shopping_cart(false);
        })
});
function update_checkout_cart_table(cart_details) {
    var shopping_cart_table_list_items = [];
    var total_qty = 0;
    var total = 0;

    $('#checkout_cart_table_notification').hide();

    $.each(cart_details, function (key, value) {
        var json_cart_detail = cart_details[key];
        var product_number_link = '';

        shopping_cart_table_list_items.push( '<tr id="checkout-cart-' + cart_details[key]['product_id'] + '" data-product-id="' + cart_details[key]['product_id'] + '">' +
            '<td class="cart_product">' +
            '<a target="_blank" href="' + cart_details[key]["product_slug"] + '"><img src="' + cart_details[key]['product_image_url'] + '" alt="Product"></a>' +
            '</td>' +
            '<td class="cart_description">' +
            '<p class="product-name"><a target="_blank"  href="' + cart_details[key]['product_slug'] + '">' + cart_details[key]["product_name"] + '</a></p>' + '<p>' + cart_details[key]['product_number_link'] + '</p>' +
            '</td>' +
            '<td class="price"><span>' + ((cart_details[key]['product_price_primary'] == 0) ? 'N/A' : $.number(cart_details[key]['product_price_primary'], 2)) + '</span><br />' +
            '<span>' + ((cart_details[key]['e_catalogue_price'] == 0) ? 'N/A' : $.number(cart_details[key]['e_catalogue_price'], 2)) + '</span>' +
            '</td>' +
            '<td class="qty">' +
            '<input id="input-' + cart_details[key]['product_id'] + '" data-product-id="' + cart_details[key]['product_id'] + '"class="number-only form-control input-sm" type="text" value="' + cart_details[key]['product_qty'] + '">' +
            '<a data-product-id="' + cart_details[key]['product_id'] + '" href="#" class="minus-cart-qty"><i class="fa fa-minus"></i></a>' +
            '<a data-product-id="' + cart_details[key]['product_id'] + '" href="#" class="plus-cart-qty"><i class="fa fa-plus"></i></a>' +
            '</td>' +
            '<td class="price">' +
            '<span>' + ((cart_details[key]['private_price'] == 0) ? 'N/A' : $.number(cart_details[key]['private_price'], 2)) + '</span><br />' +
            '<span>' + ((cart_details[key]['e_catalogue_price'] == 0) ? 'N/A' : $.number(cart_details[key]['e_catalogue_price'], 2)) + '</span>' +
            '</td>' +
            '<td class="action">' +
            '<a href="#" class="checkout-cart-row" data-product-id="' + cart_details[key]['product_id'] + '">Delete item</a>' +
            '</td>' +
            '</tr>'
        );
        total_qty += cart_details[key]['product_qty'];
        total += (cart_details[key]['product_price_primary'].replace(',', '') * cart_details[key]['product_qty']);
    });
    $('#checkout_cart_table tbody').append(shopping_cart_table_list_items.join(''));
    $('#checkout_cart_total').text('IDR ' + $.number(total, 2));
    $('#checkout_cart_total').data('checkout-total', total);
}
function update_shopping_cart(is_update_cart) {
    $.ajax({
            method: 'GET',
            url: '/cart/get/',
            data: { }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var product_count = json_data['count'];
            var total_qty = 0;
            var cart_session = json_data['cart_session'];
            var cart_details = json_data['cart_details'];
            var shopping_cart_list_items = [];
            var total = 0;

            $.each(cart_details, function (key, value) {
                var json_cart_detail = cart_details[key];

                shopping_cart_list_items.push( '<li class="product-info" id="row-' + cart_details[key]['product_id'] + '">' +
                    '<a href="' + cart_details[key]["product_slug"] + '" class="remove_link" alt="' + cart_details[key]['product_name'] + '">' +
                    '<div class="p-left">' +
                    '<img class="img-responsive" src="' + cart_details[key]['product_image_url'] + '" alt="' + cart_details[key]['product_name'] + '">' +
                    '</div>' +
                    '<div class="p-right">' +
                    '<p class="p-name">' + cart_details[key]['product_name'] + '</p>' +
                    '<p class="p-rice">' + ((cart_details[key]['product_price_primary'] == 0) ? 'N/A' : $.number(cart_details[key]['product_price_primary'], 2)) + '</p>' +
                    '<p class="p-rice">' + ((cart_details[key]['product_price_secondary'] == 0) ? 'N/A' : $.number(cart_details[key]['product_price_secondary'], 2)) + '</p>' +
                    '<p>Qty: ' + cart_details[key]['product_qty'] + '</p>' +
                    '</div>' +
                    '</a>' +
                    '</li>'
                );
                total_qty += cart_details[key]['product_qty'];
                total += (cart_details[key]['product_price_primary'].replace(',', '') * cart_details[key]['product_qty']);
            });
            if (total_qty > 0) {
                $('.cart-title').text(product_count + ' produk di kotak belanja');
                checkout_page_hide(false);
                $('.total-cart').show();
                $('#no_item_on_cart').hide();
                if (is_update_cart == true) {
                    checkout_page_hide(false);
                    update_checkout_cart_table(cart_details);
                }
            } else {
                $('.cart-title').text('');
                checkout_page_hide(true);
                $('.total-cart').hide();
                $('#no_item_on_cart').show();
            }
            $('#cart_total_qty_badge').text(total_qty);
            $('#shopping_cart_list').html(shopping_cart_list_items.join(''));
            $('#shopping_cart_total').text('IDR ' + $.number(total, 2));
            $('.total').text('IDR ' + $.number(total, 2));
        })
        .fail(function (response) {
            $('.order-detail-content').hide();
            $('#checkout_cart_table_notification').show();
            $('#cart_navigation').hide();
            $('.total-cart').hide();
            $('#no_item_on_cart').show();
        });
}
$('#cart_success_continue_button').click(function(e) {
    e.preventDefault();
    $('#cart_success_notification').toggleClass('in');
});
$('#cart_success_checkout_button').click(function(e) {
    window.location.replace($(this).data('url'));
});
$('.add-product-to-cart').click(function(e) {
    var qty = 0;
    e.preventDefault();
    if ($('#option-product-qty').data('qty') == undefined)
    {
        qty = $(this).data('qty');
    }
    else
    {
        qty = $('#option-product-qty').data('qty');
    }
    $.ajax({
            method: 'GET',
            url: '/cart/add/',
            data: { product_id: $(this).data('product-id'), qty: qty }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var status = json_data['status'];
            var message = json_data['message'];
            $('#cart_success_message').text('');
            $('#cart_success_notification').toggleClass('in');

            if ($('.cart-block-content').length > 0) {
                update_shopping_cart(true);
            }
        })
        .fail(function (jqXHR, response) {
            var json_data = $.parseJSON(jqXHR.responseText);
            var json_data = jqXHR.responseText;
            if ( !$('#cart_update_fail_notification').is( '.in' ) ) {
                $('#cart_update_fail_message').text('Coba ulangi kembali atau segera hubungi call center kami');
                $('#cart_update_fail_notification').addClass('in');
                setTimeout(function() {
                    $('#cart_update_fail_notification').removeClass('in');
                }, 3200);
            };
        });
});
function checkout_page_hide(is_hide) {
    if (is_hide == true) {
        $('.order-detail-content').hide();
        $('#checkout_cart_table_notification').show();
        $('#cart_navigation').hide();
    } else {
        $('.order-detail-content').show();
        $('#checkout_cart_table_notification').hide();
        $('.cart_navigation').show();
    }
}
function update_cart_qty(product_id, qty) {
    $.ajax({
            method: 'GET',
            url: '/cart/add/',
            data: { product_id: product_id, qty: qty }
        })
        .done(function (response) {
            //var json_data = $.parseJSON(response);
            var json_data = response;
            var status = json_data['status'];
            var message = json_data['message'];

            update_shopping_cart(false);
        })
}
function is_numeric(number) {
    var RE = /^\d*\d+$/;
    return (RE.test(number));
}
function update_qty_on_event(e, update_sign) {
    var input_id = 'input-' + e.data('product-id');
    var input_value = $('#' + input_id);
    if (is_numeric(input_value.val())) {
        if ((update_sign == '-') && (input_value.val() > 1)) {
            input_value.val(parseInt(input_value.val()) - 1);
        } else if (update_sign == '+') {
            input_value.val(parseInt(input_value.val()) + 1);
        }
    }
    update_cart_qty(e.data('product-id'), input_value.val());
}