# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AuthenticationSessions',
            fields=[
                ('session_key', models.CharField(max_length=40, serialize=False, primary_key=True)),
                ('ip_address', models.CharField(max_length=45)),
                ('session_data', models.CharField(max_length=100)),
                ('session_date', models.DateTimeField(auto_now_add=True)),
                ('expire_date', models.DateTimeField(db_index=True)),
                ('last_login', models.DateTimeField(null=True)),
            ],
            options={
                'db_table': 'authentication_sessions',
                'verbose_name': 'Authentication Session',
                'verbose_name_plural': 'Authentication Sessions',
            },
        ),
    ]
