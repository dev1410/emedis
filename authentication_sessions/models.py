from django.db import models
from django.db.models import CharField, DateTimeField
from customers.models import Customer
from cart.models import Cart


class AuthenticationSessions(models.Model):
    class Meta:
        verbose_name = 'Authentication Session'
        verbose_name_plural = 'Authentication Sessions'
        db_table = 'authentication_sessions'

    session_key = CharField(max_length=40,
                            primary_key=True,)
    ip_address = CharField(max_length=45, )
    session_data = CharField(max_length=100)
    session_date = DateTimeField(auto_now_add=True)
    expire_date = DateTimeField(db_index=True)
    last_login = DateTimeField(null=True)
