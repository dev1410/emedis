import settings
import requests
import json
from . import response, request as vt_request
from common.utils.object import AutoAssignObject


class VTWeb(object):
    """
    Gateway to comunicate with veritrans API with method VTWeb.
    see: http://docs.veritrans.co.id/en/api/methods.html#vtwebcharge
    :py:class:VTWeb:
    """

    def __init__(self, sandbox_mode=False):
        self.sandbox_mode = sandbox_mode

    @property
    def base_url(self):
        """
        Returned base API URL regarding to the sandbox_mode value
        :rtype base_url: :py:class:`str`
        """
        return settings.VT_SANDBOX_API_URL if self.sandbox_mode \
            else settings.VT_LIVE_API_URL

    @property
    def __server_key__(self):
        """
        Returned VT Server Key regarding to the sandbox_mode
        :rtype __server_key__: :py:class:`str`
        """
        return settings.VT_DEV_API_KEY if self.sandbox_mode \
            else settings.VT_API_KEY

    def charge_request(self, req):
        """
        Submit charge request to the VT API.
        :param req:
        :type req: :py:class:ChargeRequest. And returned a
        :rtype: :py:class:`payments.veritrans.response.ChargeResponseBase`
        """
        # run validation againts our charge request before submitting
        req.validate_all()

        # build up our application payload and manually specify the header type.
        payload = json.dumps(req.serialize())
        headers = {'Content-Type': 'application/json',
                   'accept': 'application/json'}

        # Only for debugging purpose
        print 'request: \r\n{}\r\n'.format(req)
        print 'payload: \r\n{}\r\n'.format(payload)
        print 'headers: \r\n{}\r\n'.format(headers)

        # Submitting charge request
        http_response = requests.post(
            url='{base_url}/charge'.format(base_url=self.base_url),
            auth=(self.__server_key__, ''),
            headers=headers,
            data=payload)

        json_response = http_response.json()

        # Only for debugging purpose
        print 'json_response : \r\n{json_res}\r\n'.format(json_res=json_response)

        # build up pythonic response from VT after submit charge request
        vt_response = response.build_charge_response(
            request=req,
            **json_response)

        return vt_response

    def status_request(self, req):
        """
        Retrieve informations from Veritrans about a single transaction
        :param req: Data about a transaction to retrieve of
        :type req: :py:class:`payments.veritrans.requests.StatusRequest` **or**
            any response class that has an order_id attribute,
            such as :py:class:`payments.veritrans.response.ChargeResponseBase
        :rtype: :py:class:`payments.veritrans.response.StatusResponse`
        """
        # Specifically skip if it's a response type we don't have a
        # good reason to validate those.
        if not isinstance(req, response.ResponseBase):
            req.validate_all()

        request_url_format = '{base_url}/{order_id}/status'

        headers = {'accept': 'application/json'}

        http_response = requests.get(
            request_url_format.format(base_url=self.base_url,
                                      order_id=req.order_id),
            auth=(self.__server_key__, ''),
            headers=headers)
        json_response = http_response.json()

        vt_response = response.StatusResponse(**json_response)

        return vt_response

    def capture_finish_redirect_url(self, request):
        """
        A method to capture response after user finish transaction on the VTWeb,
         and redirected back to our site.
        :param request:
        :type request: :py:class:`django.http.request.HttpRequest`
        """
        # Order model imported locally to avoid ImportError
        from product.order.models import Order

        ro = dict()
        ro['order_id'] = request.GET.get('order_id', '')
        ro['status_code'] = request.GET.get('status_code', '')
        ro['transaction_status'] = request.GET.get('transaction_status', '')

        # build up object from finish redirect url after Veritrans VTWeb completed
        finish_status = AutoAssignObject(**ro)

        order = Order.objects.get(order_number=ro['order_id'])

        # make sure the finish transaction status
        if int(finish_status.status_code) == 200 \
                and finish_status.transaction_status == 'capture':

            request_status = vt_request.StatusRequest(order_id=str(finish_status.order_id))

            # request order status from Veritrans
            vt_status_response = self.status_request(req=request_status)

            # sync transaction status from GET url on our finish_redirect_url
            # and from Veritrans status request to avoid fraud captured status
            if vt_status_response.transaction_status == 'capture' and \
                            order.total == vt_status_response.gross_amount:
                # trigger the order data on our database to mark the processed order
                # and sending both user and vendor an invoice and delivery order email
                return Order.mark_as_paid_orders(finish_status.order_id)

        return False

    @staticmethod
    def create_item(product):
        """
        Returned a request.ItemDetails `object`.
        :param product:
        :type product: :py:class:`product.order.models.OrderDetail`
        :rtype: :py:class:`payments.veritrans.request.ItemDetails`
        """
        item = vt_request.ItemDetails(
            item_id=product.product_number,
            price=product.product_price,
            quantity=product.product_quantity,
            name=product.product_name)

        return item

    @staticmethod
    def capture_unfinish_redirect_url(*args, **kwargs):
        pass

    @staticmethod
    def capture_error_redirect_url(*args, **kwargs):
        pass

    def __repr__(self):
        """
        Representation of this class
        :return Object:
        :rtype Object: :py:class:VTWeb
        """
        return (
            "<VTWeb("
            "server_key: '{server_key}', "
            "sandbox_mode: {sandbox_mode})"
            .format(server_key=settings.VT_API_KEY,
                    sandbox_mode=self.sandbox_mode))
