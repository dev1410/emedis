from . import mixins
import json


class PaymentTypeBase(mixins.SerializableMixin):
    '''
    Base type for all payment types.  Not usable by itself.
    '''
    def serialize(self):
        return {"payment_type": self.PAYMENT_TYPE_KEY,
                self.PAYMENT_TYPE_KEY: {},
                }


class CreditCard(PaymentTypeBase):
    '''
    A payment made with a credit card.
    http://docs.veritranspay.co.id/sandbox/charge.html#vtdirect-cc
    '''
    PAYMENT_TYPE_KEY = 'credit_card'

    def __init__(self, bank, token_id, save_token_id=False):
        '''
        :param bank: Represents the acquiring bank.
        :type bank: :py:class:`str`
        :param token_id: A token retrieved from the Veritrans
            JavaScript library, after submitting the credit card details.
        :type token_id: :py:class:`str`
        :param bool save_token_id: Used in conjunction with a 2-click to
            indicate whether or not this token is to be made reusable.
        '''
        self.bank = bank
        self.token_id = token_id
        self.save_token_id = save_token_id

    def serialize(self):

        rv = super(CreditCard, self).serialize()

        rv[self.PAYMENT_TYPE_KEY] = {'bank': self.bank,
                                     'token_id': self.token_id,
                                     }

        # append save_token_id to the request, only if it's set to True
        if self.save_token_id:
            rv[self.PAYMENT_TYPE_KEY].update(
                {'save_token_id': self.save_token_id})

        return rv


class VTWeb(PaymentTypeBase):
    """
    Payment method by redirect user to the VT payments website.
    see:
    """
    PAYMENT_TYPE_KEY = 'vtweb'

    def __init__(self, enabled_payments=list(), credit_card_3d_secure=True,
                 finish_redirect_url=None, unfinish_redirect_url=None,
                 error_redirect_url=None):

        self.enabled_payments = enabled_payments
        self.credit_card_3d_secure = credit_card_3d_secure
        self.finish_redirect_url = finish_redirect_url
        self.unfinish_redirect_url = unfinish_redirect_url
        self.error_redirect_url = error_redirect_url

    def serialize(self):
        rv = super(VTWeb, self).serialize()
        rv[self.PAYMENT_TYPE_KEY] = {'credit_card_3d_secure': self.credit_card_3d_secure,}

        if self.enabled_payments.__len__() > 0:
            rv[self.PAYMENT_TYPE_KEY].update(
                {'enabled_payments': self.enabled_payments})

        if self.finish_redirect_url is not None:
            rv.update(
                {'finish_redirect_url': self.finish_redirect_url})

        if self.unfinish_redirect_url is not None:
            rv.update(
                {'unfinish_redirect_url': self.unfinish_redirect_url})

        if self.error_redirect_url is not None:
            rv.update({'error_redirect_url': self.error_redirect_url})

        return rv


class Indomaret(PaymentTypeBase):
    PAYMENT_TYPE_KEY = 'cstore'

    def __init__(self, message):
        self.store = 'Indomaret'
        self.message = message

    def serialize(self):
        rv = super(Indomaret, self).serialize()
        rv[self.PAYMENT_TYPE_KEY].update({
            'store': self.store,
            'message': self.message
        })
        return rv

#
# NOTE:
# The following types are not yet supported!
# They will be added to the documentation as support is added
#

class MandiriClickpay(PaymentTypeBase):
    # http://docs.veritranspay.co.id/sandbox/charge.html#vtdirect-mandiri
    PAYMENT_TYPE_KEY = 'mandiri_clickpay'

    def __init__(self, card_number, input1, input2, input3):
        raise NotImplementedError("Only CreditCard is implemented.")

class CimbClicks(PaymentTypeBase):
    # http://docs.veritranspay.co.id/sandbox/charge.html#vtdirect-cimb
    PAYMENT_TYPE_KEY = 'cimb_clicks'

    def __init__(self, description):
        raise NotImplementedError("Only CreditCard is implemented.")
