from . import vtweb, request, payment_types, validators
from django.http import HttpResponse


def test_charge_vt_production(customer, transaction):
    """
    This method handle charge request test from veritrans for production purpose.
    :param customer:
    :type customer: :py:class:`payments.veritrans.request.CustomerDetails`
    :param transaction:
    :type transaction: :py:class:`payments.veritrans.request.TransactionDetails`
    :rtype: :py:class:`str`

    customer = request.CustomerDetails(first_name="John",
                                       last_name="Doe",
                                       email="john.doe@mail.com",
                                       phone="0000000000")

    transaction = request.TransactionDetails(order_id="ORDID00001",
                                             gross_amount=10000)

    """

    link_direct_to_vt = None

    gateway = vtweb.VTWeb()

    payment_type = payment_types.VTWeb()

    charge_request = request.ChargeRequest(charge_type=payment_type,
                                           transaction_details=transaction,
                                           customer_details=customer)

    try:
        response = gateway.charge_request(req=charge_request)
        link_direct_to_vt = response.redirect_url
    except validators.ValidationError as e:
        raise validators.ValidationError('Validation Error : %s\n\r' % e.message)
    except AttributeError:
        pass

    return link_direct_to_vt


def test_request_status_vt_production(order_id):
    """
    This method handle transaction status requested from veritrans.

    In case customer has been finished paying on veritrans web,
    finish_redirect_url to our website, given GET & POST data,
    we just need order_id from GET querystring then request back
    the status to veritrans to make sure the given data is valid.

    :param order_id:
    :type order_id: :py:class:`str`
    :rtype: :py:class:`object`
    """
    try:
        status_req = request.StatusRequest(order_id=order_id)
        return vtweb.VTWeb().status_request(req=status_req)

    except validators.ValidationError as e:
        raise validators.ValidationError("Validation Error : %s" % e.message)


def charge_veritrans(order_id):
    """
    A method to demonstrate VTWeb payment gateway
    :param order_id:
    :type order_id: :py:class:`str`
    """
    gateway = vtweb.VTWeb(sandbox_mode=True)

    cust = request.CustomerDetails(
        first_name='Rijal',
        last_name='Tanjung',
        email='ical@mail.com',
        phone='0888988748')

    trans = request.TransactionDetails(
        order_id=str(order_id),
        gross_amount=9000)

    item_1 = request.ItemDetails(
        item_id='201', price=9000, quantity=2,
        name='Saturn Select 3.02 Operating Table')

    item_2 = request.ItemDetails(
        item_id='202', price=9100, quantity=3,
        name='Bipolar Dissection Electrode')

    payment_type = payment_types.VTWeb()

    charge_req = request.ChargeRequest(charge_type=payment_type,
                                       transaction_details=trans,
                                       customer_details=cust)

    try:
        resp = gateway.charge_request(charge_req)
        print resp.redirect_url

        status_req = request.StatusRequest(order_id=order_id)
        status_resp = gateway.status_request(req=status_req)
        print status_resp

    except validators.ValidationError as e:
        print ('Oops.. {e}'.format(e=e.message))


def status():
    gateway = vtweb.VTWeb(sandbox_mode=True)

    status_req = request.StatusRequest(order_id='1234')

    print gateway.status_request(req=status_req).serialize()


def capture_finish_redirect_url_view(request):
    html = '<html><body><p>Order ID : {order_id}</p><br/>' \
           '<p>Status Code : {status_code}</p><br/>' \
           '<p>Status message : {status_message}</p><br/>' \
           'It is test page</body></html>'.format(order_id=request.GET.get('order_id', ''),
                                                  status_code=int(request.GET.get('status_code', '')),
                                                  status_message=request.GET.get('transaction_status', ''))
    vtweb.VTWeb(sandbox_mode=True).capture_finish_redirect_url(request=request)
    return HttpResponse(html, content_type='text/html')