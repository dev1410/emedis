# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.http import HttpResponse
from django.template import loader


def index(request):
    context = {'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id: #1 Toko Alat Kesehatan Online Terbesar & Terlengkap se-Indonesia',
               'logo-alt': 'Emedis.id: #1 Toko Alat Kesehatan Online Terbesar & Terlengkap se-Indonesia',
               'logo-title': 'Emedis.id: #1 Toko Alat Kesehatan Online Terbesar & Terlengkap se-Indonesia', }
    template = loader.get_template('web-v2/landing-page.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def submit(request):
    context = {'response': ''}
    template = loader.get_template('web-v2/submit.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)
