from django.conf.urls import url
from landing_page import views

urlpatterns = [
    url(r'^$', views.index, name='landing_page'),
    url(r'^submit/', views.submit, name='submit'),
]
