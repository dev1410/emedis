from django.conf import settings
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from payments.veritrans.vtweb import VTWeb
from common.utils.authentication import Authentication
import json


@csrf_exempt
def payment_verification(request):
    result = dict()

    if Authentication.get_client_ip_address(request) in settings.VERITRANS_LIST_IP:
        vt_web = VTWeb(sandbox_mode=True)
        vt_web.capture_finish_redirect_url(request=request)

        result['message'] = 'Payment Notification successfully update.'
        result['status'] = 200
    else:
        result['message'] = 'Internal Server Error'
        result['status'] = 500

    response = json.dumps(result)

    http_response = HttpResponse(response, status=result['status'], content_type='application/json')
    return HttpResponse(http_response)

# {
#   "status_code": "200",
#   "status_message": "Midtrans payment notification",
#   "transaction_id": "ac4a95ea-8cd8-4594-9887-39e8f5133227",
#   "order_id": "Sample Order-1",
#   "payment_type": "credit_card",
#   "transaction_time": "2016-10-24 13:46:21 +0700",
#   "transaction_status": "capture",
#   "fraud_status": "ACCEPT",
#   "masked_card": "548336-1111",
#   "gross_amount": "2040000.00"
# }