from django.http import JsonResponse
from product.models import Product
from product.models import Brand
from common.utils.text_formatting import TextFormatting
from elastic.views import index as elastic_index


def best_seller(request):
    result = dict()
    http_response_code_success = 200

    products = Product.objects.filter(is_bestseller=1)[:3]

    product_data = list()

    for product in products:
        emedis_price = TextFormatting.price_formatting(product.get_product_detail().private_price)
        e_catalogue_price = TextFormatting.int_price_formatting(product.get_product_detail().e_catalogue_price)

        product_detail = dict()
        product_detail['product_name'] = product.product_name
        product_detail['product_id'] = product.id
        product_detail['image_url'] = product.get_first_image()
        product_detail['emedis_price'] = emedis_price['price']
        product_detail['emedis_price_thousands'] = emedis_price['thousands']
        product_detail['emedis_price_currency'] = '' if product_detail['emedis_price'] == 'n/a' else product.get_product_detail().private_price_currency.short_label
        product_detail['ekatalog_price'] = e_catalogue_price if e_catalogue_price >= 0 else 'n/a'
        product_detail['ekatalog_price_currency'] = '' if product_detail['ekatalog_price'] == 'n/a' else product.get_product_detail().e_catalogue_price_currency.short_label
        product_detail['product_slug'] = product.productmetadata.slug

        product_data.append(product_detail)

    result['count'] = products.__len__()
    result['code'] = http_response_code_success if result['count'] > 0 else 204
    result['result'] = product_data

    return JsonResponse(result,
                        content_type='application/json',
                        status=http_response_code_success,
                        safe=False,)


def brand_product_related(request, slug):
    result = dict()
    http_response_code_success = 200

    brand = Brand.objects.get(slug=slug)
    products = Product.objects.filter(brand=brand)[:5]

    product_data = list()

    for product in products:
        emedis_price = TextFormatting.price_formatting(product.get_product_detail().private_price)
        e_catalogue_price = product.get_product_detail().e_catalogue_price

        product_detail = dict()
        product_detail['product_name'] = product.product_name
        product_detail['product_id'] = product.id
        product_detail['image_url'] = product.get_first_image()
        product_detail['emedis_price'] = emedis_price['price']
        product_detail['emedis_price_thousands'] = emedis_price['thousands']
        product_detail['emedis_price_currency'] = '' if product_detail['emedis_price'] == 'n/a' else product.get_product_detail().private_price_currency.short_label
        product_detail['ekatalog_price'] = e_catalogue_price if e_catalogue_price >= 0 else 'n/a'
        product_detail['ekatalog_price_currency'] = '' if product_detail['ekatalog_price'] == 'n/a' else product.get_product_detail().e_catalogue_price_currency.short_label
        product_detail['product_slug'] = product.productmetadata.slug

        product_data.append(product_detail)

    result['count'] = products.__len__()
    result['code'] = http_response_code_success if result['count'] > 0 else 204
    result['result'] = product_data

    return JsonResponse(result,
                        content_type='application/json',
                        status=http_response_code_success,
                        safe=False,)


def suggestion(request):
    result = list()
    status = 200
    q = ''

    if 'q' in request.GET and len(str(request.GET['q']).strip()) > 2:
        q = request.GET['q']

        # suggestions = search.views.index(request)
        suggestions = elastic_index(request)

        for suggestion_data in suggestions['hits']['hits']:
            selection = dict()
            try:
                selection['value'] = str(suggestion_data['fields']['product_with_brand_name'][0])
                selection['data'] = str(suggestion_data['fields']['metadata.slug'][0])
            except:
                pass

            if 'value' in selection:
                result.append(selection)

    temp = dict()

    temp['suggestions'] = result

    return JsonResponse(temp,
                        content_type='application/json',
                        status=status,
                        safe=False,)
