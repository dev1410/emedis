import json
from decimal import Decimal, getcontext
from django.core import exceptions
from django.http import HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
from cart.models import Cart
from cart.cart_details.models import CartDetail
from website.cart_data import CartData
from product.models import Product
from common.utils.authentication import Authentication


def cart(request):
    getcontext().prec = 2

    result = dict()
    result['message'] = ''
    result['status'] = 400
    cart_session = None
    product_id = None
    qty = 1
    remark = ''
    product_object = None

    if 'product_id' in request.GET:
        product_id = request.GET['product_id']

    if 'qty' in request.GET:
        qty = request.GET['qty']

        qty = int(qty)
        print '{0} - {1}'.format(qty, type(qty))

    if 'remark' in request.GET:
        remark = request.GET['remark']

    if product_id is not None:
        try:
            product_object = Product.objects.get(id=product_id)
        except exceptions.ObjectDoesNotExist:
            pass

    cart_session = Authentication.get_cart_session(request=request)

    if product_object is not None:
        try:
            cart_object = Cart.objects.get(cart_session=cart_session)
        except exceptions.ObjectDoesNotExist:
            cart_object = Cart()
            cart_object.cart_session = cart_session

        cart_object.cart_status = 1
        cart_object.save()

        try:
            cart_detail = CartDetail.objects.get(cart=cart_object, product=product_object)
        except CartDetail.DoesNotExist:
            cart_detail = CartDetail()

        cart_detail.product = product_object
        cart_detail.product_qty = qty

        if product_object.get_product_detail().private_price is None:
            cart_detail.private_price = Decimal(0.00)
        else:
            cart_detail.private_price = product_object.get_product_detail().private_price

        if product_object.get_product_detail().e_catalogue_price is None:
            cart_detail.e_catalogue_price = Decimal(0)
        else:
            cart_detail.e_catalogue_price = product_object.get_product_detail().e_catalogue_price

        cart_detail.private_price_currency = product_object.get_product_detail().private_price_currency
        cart_detail.e_catalogue_price_currency = product_object.get_product_detail().e_catalogue_price_currency

        cart_detail.product_remark = remark
        cart_detail.cart = cart_object
        cart_detail.save()

        result['message'] = 'Produk telah ditambahkan kekeranjang belanja'
        result['status'] = 200
    else:
        result['message'] = 'Produk tidak dapat ditemukan'
        result['status'] = 400

    response = json.dumps(result)

    http_response = HttpResponse(response, status=result['status'], content_type='application/json')
    http_response.set_cookie('cart_session', cart_session)

    return http_response


def get_cart(request):
    result = dict()
    result['status'] = 400
    result['message'] = 'No Cart data'
    cart_session = None
    cart = dict()
    cart_details = list()

    if 'cart_session' in request.COOKIES:
        cart_session = request.COOKIES.get('cart_session')

    cart_object = CartData.get_cart_object(cart_session=cart_session)

    if cart_object is not None:
        cart = CartData.cart_object_to_dictionary(cart=cart_object)
        cart_detail_object = CartData.get_cart_detail_object(cart_object=cart_object)
        cart_details = CartData.cart_detail_object_to_dictionary(cart_detail_object=cart_detail_object)

        result.update(cart)
        result['cart_details'] = cart_details
        result['count'] = len(cart_details)

        result['status'] = 200
        result['message'] = 'Cart data'

    response = json.dumps(result, cls=DjangoJSONEncoder)

    http_response = HttpResponse(response, status=result['status'], content_type='application/json')

    return http_response


def delete_cart(request):
    result = {'status': 400, 'message': 'Tidak ada produk dalam keranjang belanja', }
    cart_session = None
    product_id = None

    if 'cart_session' in request.COOKIES:
        cart_session = request.COOKIES.get('cart_session')

    if 'product_id' in request.GET and str(request.GET['product_id']).isdigit():
        product_id = request.GET['product_id']

    if product_id is not None and cart_session is not None:
        cart_object = CartData.get_cart_object(cart_session=cart_session)

        if cart_object is not None:
            cart_detail_object = CartData.get_cart_detail_object(cart_object=cart_object)
            for cart_detail in cart_detail_object:
                if cart_detail.product_id == long(product_id):
                    result['status'] = 200
                    result['message'] = 'Produk telah dihapus dari kelanjang belanja'
                    cart_detail.delete()
                    break

    response = json.dumps(result, cls=DjangoJSONEncoder)

    return HttpResponse(response, status=result['status'], content_type='application/json')
