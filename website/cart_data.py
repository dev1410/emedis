from cart.models import Cart
from cart.cart_details.models import CartDetail
from product.models import Product
from product.models import ProductMetadata
from common.utils.authentication import Authentication
import sys


class CartData():
    def __init__(self):
        pass

    @staticmethod
    def create_new_cart(request):
        new_cart_session_id = Authentication.get_new_cart_session()

        cart = Cart()
        cart.cart_status = 1
        cart.cart_session = new_cart_session_id
        cart.save()

        return cart

    @staticmethod
    def get_cart_object(cart_session):
        result = None

        try:
            if cart_session is not None:
                result = Cart.objects.get(cart_session=cart_session)
        except Cart.DoesNotExist:
            pass

        return result

    @staticmethod
    def cart_object_to_dictionary(cart):
        result = dict()

        # result['id'] = cart.id
        result['cart_created_time'] = cart.cart_created_time
        result['cart_session'] = cart.cart_session

        return result

    @staticmethod
    def get_cart_detail_object(cart_object):
        result = None

        try:
            if cart_object is not None:
                result = CartDetail.objects.filter(cart=cart_object)
        except CartDetail.DoesNotExist:
            pass

        return result

    @staticmethod
    def cart_detail_object_to_dictionary(cart_detail_object, price_formatting=False):
        result = list()

        try:
            for cart_detail_object_row in cart_detail_object:
                cart_detail = dict()
                cart_detail['product_id'] = cart_detail_object_row.product_id
                cart_detail['product_qty'] = cart_detail_object_row.product_qty

                cart_detail['private_price'] = (cart_detail_object_row.private_price if cart_detail_object_row.private_price > 0 else 'Request for quote')
                cart_detail['e_catalogue_price'] = (cart_detail_object_row.e_catalogue_price if cart_detail_object_row.e_catalogue_price > 0 else 'Request for quote')
                cart_detail['private_price_currency'] = (cart_detail_object_row.private_price_currency.short_label if cart_detail_object_row.private_price_currency is not None else '')
                cart_detail['e_catalogue_price_currency'] = (cart_detail_object_row.e_catalogue_price_currency.short_label if cart_detail_object_row.e_catalogue_price_currency is not None else '')
                cart_detail['product_remark'] = cart_detail_object_row.product_remark
                cart_detail['product_added_time'] = cart_detail_object_row.product_added_time

                try:
                    product = Product.objects.get(id=cart_detail['product_id'])
                    try:
                        cart_detail['product_image_url'] = product.get_product_image().image_media_url()
                    except:
                        cart_detail['product_image_url'] = '/static/assets/images/no-image-300x366.jpg'
                    cart_detail['product_slug'] = '{0}/{1}'.format('/product_detail', product.get_product_metadata().slug)
                    cart_detail['product_name'] = product.product_name

                    if product.product_number is not None and len(str(product.product_number).strip()) > 0:
                        cart_detail['product_number_link'] = '<sub>Klik <a href="https://e-katalog.lkpp.go.id/backend/katalog/list_produk/24/?isSubmitted=1&kategoriProdukId=&kabupatenId=302&searchIn=CONCAT(m.nama_manufaktur%2C+%27+%27%2C+p.nama_produk%2C+%27+%27%2C+p.no_produk%2C+%27+%27%2C+p.no_produk_penyedia)&keyword={0}&penyediaId=all&manufakturId=all&orderBy=harga+ASC&list=20" target="_blank"><img src="/static/assets/images/lkpp-connect.png" alt="{1}" /></a> untuk memesan produk ini secara langsung di situs resmi eCatalogue</sub>'.format(str(product.product_number)[:22], product.product_name)
                    else:
                        cart_detail['product_number_link'] = ''

                    cart_detail['product_number'] = product.product_number
                except ProductMetadata.DoesNotExist:
                    print sys.exc_info()[1]

                result.append(cart_detail)
        except:
            print sys.exc_info()[1]

        return result

    @staticmethod
    def get_product_object(product_id):
        result = None

        try:
            result = Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            pass

        return result
