from django.conf.urls import url, include
from . import views, cart_view, ajax
from .auth import views as auth_views
import payment


urlpatterns = [
    url(r'^$', views.index, name='web-index'),
    url(r'^signin/$', auth_views.signin, name='login'),
    url(r'^signup/$', auth_views.signup, name='signup'),
    url(r'^signout/$', auth_views.signout, name='signout'),
    url(r'^account_activation/$', auth_views.account_activation, name='activation'),
    url(r'^category/(?P<slug>[\w-]+)/$', views.category, name='category'),
    url(r'^search/(?P<slug>[\w-]+)/$', views.search, name='category'),
    url(r'^payment_verification/$', payment.payment_verification, name='payment-verification'),
    url(r'^product_detail/(?P<slug>[\w-]+)/$', views.product_detail, name='detail'),
    url(r'^cart/add/$', cart_view.cart, name='add-to-cart'),
    url(r'cart/get/$', cart_view.get_cart, name="get-cart"),
    url(r'cart/delete/$', cart_view.delete_cart, name="delete-product-on-cart"),
    url(r'^suggestion/$', ajax.suggestion, name='search-suggestion'),
    url(r'^checkout/$', views.checkout, name='Checkout'),
    url(r'^confirmation/$', views.confirmation, name='confirmation-page'),
    url(r'^direct_payment/$', views.direct_payment, name='direct-payment'),
    url(r'^best_seller/$', ajax.best_seller, name='best-seller'),
    url(r'^brand_product_related/(?P<slug>[\w-]+)/$', ajax.brand_product_related, name='brand-product-related'),
    url(r'^subscribe/$', include('subscribe.urls')),
]
