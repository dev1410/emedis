import hashlib
import urllib
import uuid
from django.conf import settings
from django.core.context_processors import csrf
from django.http import HttpResponse, HttpResponseRedirect, HttpResponsePermanentRedirect
from django.template import loader
from common.utils.guid import create_guid
from customers.models import Customer
from common.utils.transaction_email import TransactionEmail
from common.utils.authentication import Authentication


def signin(request):
    data = {}

    if 'login_email' in request.POST and 'login_password' in request.POST:
        email = request.POST.get('login_email', '')
        password = request.POST.get('login_password', '')

        data['login_email'] = email

        if email != '' and password != '':
            authentication = Authentication(request=request)
            customer = authentication.login(email_address=email, password=password)

            if customer is not None:
                request.session['session'] = create_guid()
                return HttpResponsePermanentRedirect('/' + settings.ADDITIONAL_URL_PATH)

    context = {'data': data,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format('Halaman Masuk / Bergabung', settings.META_TITLE),
               'meta_description': settings.META_DESCRIPTION, }
    context.update(csrf(request))

    template = loader.get_template('web-v2/sign.html')
    html = template.render(context, request)

    http_response = HttpResponse(html)

    return http_response


def signout(request):
    cart_session = Authentication.signout_customer(request)

    http_response = HttpResponseRedirect('/')
    http_response.set_cookie('cart_session', cart_session)

    return http_response


def signup(request):
    error_message = 'Field yang masih belum lengkap:<br />'
    success = True
    redirect_url = '/'
    data = {}

    full_name = request.POST.get('full_name', '')
    phone_number = request.POST.get('phone_number', '')
    email_address = request.POST.get('email_address', '')
    user_password = request.POST.get('user_password', '')
    user_confirm_password = request.POST.get('user_confirm_password', '')

    data['full_name'] = full_name
    data['phone_number'] = phone_number
    data['email_address'] = email_address

    if full_name.strip() == '':
        error_message += 'Nama lengkap<br />'
        success = False

    if phone_number.strip() == '':
        error_message += 'Nomor telepon<br />'
        success = False

    if email_address.strip() == '':
        error_message += 'Alamat email<br />'
        success = False

    if user_password.strip() == '':
        error_message += 'Password<br />'
        success = False
    elif user_password != '' and (user_password != user_confirm_password):
        error_message += 'Verifikasi password tidak sama<br />'
        success = False

    if email_address != '' and Customer.availability(email=email_address):
        error_message = 'Alamat telah terdaftar, klik <a href="#">disini</a> untuk merubah password Anda<br />'
        success = False

    if success:
        password_hash = hashlib.sha1(settings.SECRET_KEY + ' ' + user_password)

        customer = Customer.objects.create(pic_name=full_name,
                                           email=email_address,
                                           phone=phone_number,
                                           password=password_hash.hexdigest(),
                                           activation_code=str(uuid.uuid4()).replace('-', '')+str(uuid.uuid1()).replace('-', ''),)

        context = {'message': 'Anda telah berhasil melakukan signup, silahkan cek email Anda untuk mengaktifasi.  Klik <a href="/beta/" id="redirect_to_home">disini</a> untuk kembali kehalaman utama.',
                   'customer': customer,
                   'data': {'activation_link': 'https://www.emedis.id/account_activation/?activation_code={0}&email={1}'.format(customer.activation_code, customer.email)},
                   'subject': 'Selamat Datang di Emedis.id',
                   'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
                   'meta_title':  '{0} | {1}'.format('Halaman Masuk / Bergabung', settings.META_TITLE),
                   'meta_description': settings.META_DESCRIPTION, }

        transaction_email = TransactionEmail(to=customer.email,
                                             email_template='user_activation')
        transaction_email.send_email(context=context)

        template = loader.get_template('web-v2/sign_up_success.html')
        html = template.render(context, request)
    else:
        context = {'error_message': error_message,
                   'data': data,
                   'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
                   'meta_title':  '{0} | {1}'.format('Halaman Masuk / Begabung', settings.META_TITLE),
                   'meta_description': settings.META_DESCRIPTION, }

        template = loader.get_template('web-v2/sign.html')
        html = template.render(context, request)

    http_response = HttpResponse(html)

    return http_response


def account_activation(request):
    activation_code = request.GET.get('activation_code', '')
    email = request.GET.get('email', '')

    if activation_code != '' and email != '':
        if Customer.account_activation(email=email, activation_code=urllib.unquote(activation_code)):
            context = {'message': 'Akun Anda sudah aktif.  Klik <a href="/signin/" id="redirect_to_home">disini</a> untuk login.',
                       'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
                       'meta_title':  '{0} | {1}'.format('Halaman Aktifasi Akun', settings.META_TITLE),
                       'meta_description': settings.META_DESCRIPTION, }

            template = loader.get_template('web-v2/sign_up_success.html')
            html = template.render(context, request)

            http_response = HttpResponse(html)

            return http_response

    return HttpResponsePermanentRedirect('/')


def forget_password(request):
    pass


def change_password(request):
    pass
