# -*- coding: utf-8 -*-
from __future__ import absolute_import

import os
import sys
import math
from django.conf import settings
from django.db import transaction
from django.db.models import F, FloatField, Sum
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from category.models import Category
from product.models import Product
from product.models import ProductDetail
from product.models import ProductImage, ProductMetadata
from search.views import products_by_category
from website.cart_data import CartData
from product.library.generator import Generator
from operator import itemgetter
from product.order.models import TempOrder, TempOrderDetail
from customers.models import Customer
from customers.detail.models import CustomerDetail
from common.utils.text_formatting import TextFormatting
from common.utils.authentication import Authentication
from payments.veritrans import tests
from payments.veritrans import request as veritrans_request
from cart.models import Cart
from cart.cart_details.models import CartDetail
from elastic import search as elastic_search


def index(request):
    context = {'homepage': True,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title': settings.META_TITLE,
               'meta_description': settings.META_DESCRIPTION, }
    template = loader.get_template('web-v2/home.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def category(request, slug):
    products = []
    page = 1
    limit = 20

    category_object = Category.objects.get(slug=slug)

    page = int(request.GET.get('page', 1))
    limit = int(request.GET.get('limit', 20)) if 1 <= limit <= 20 else 20

    if category_object is not None:
        categories = Category.objects.filter(is_active=1).order_by('sort_order')
        data = products_by_category(category_name=category_object.name,
                                    offset=((page - 1)*limit),
                                    limit=limit)
        # '{:20,.2f}'.format(18446744073709551616.0)
        pagination = dict()
        pagination['total_row'] = data['hits']['total']
        pagination['current_page'] = page
        pagination['limit_per_page'] = limit
        pagination['total_page'] = int(math.ceil(pagination['total_row'] / (pagination['limit_per_page'] * 1.0)))

        pagination['paginators'] = create_pagination(pagination=pagination)
        if pagination['total_row'] > 0:
            for item in data['hits']['hits']:
                product = {}
                try:
                    # print item['_source']['price']['e_catalogue']
                    # item['_source']['price']['e_catalogue'] = TextFormatting.price_formatting(item['_source']['price']['e_catalogue'])
                    # print item['_source']['price']['e_catalogue']

                    item['_source']['price']['private'] = TextFormatting.price_formatting(item['_source']['price']['private'])
                    product['price'] = item['_source']['price']
                except:
                    None

                try:
                    image_url = '/static/assets/images/no-image-300x366.jpg'

                    if len(item['_source']['image']) > 0 and os.path.isfile(os.path.join(settings.BASE_DIR, 'media/{0}'.format(item['_source']['image'][0]['url']))):
                        image_url = os.path.join(settings.MEDIA_URL, item['_source']['image'][0]['url'])

                    image = dict()
                    image['url'] = image_url
                    product['image'] = dict()
                    product['image'].update(image)
                except:
                    sys.exc_info()[1]

                try:
                    product['product_name'] = TextFormatting.text_ellipsis(text=item['_source']['product_name'].strip(), max_length=50, ellipsis_chars='...')
                except:
                    pass
                try:
                    product['slug'] = item['_source']['metadata']['slug']
                    product['id'] = item['_source']['id']
                    pm = ProductMetadata.objects.get(slug=item['_source']['metadata']['slug']).product
                    pd = Product.objects.get(id=pm.id)
                except:
                    print sys.exc_info()[1]

                products.append(product)

    context = {'categories': categories,
               'products': products,
               'pagination': pagination,
               'current_category': category_object,
               'category_page': True,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format(category_object.name.encode('ascii', 'ignore').decode('ascii'), settings.META_TITLE),
               'meta_description': 'Belanja {0} di {1}'.format(category_object.name.encode('ascii', 'ignore').decode('ascii'), settings.META_DESCRIPTION), }

    template = loader.get_template('web-v2/list.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def search(request, slug):
    products = []
    page = 1
    limit = 20

    page = int(request.GET.get('page', 1))
    limit = int(request.GET.get('limit', 20)) if 1 <= limit <= 20 else 20

    data = elastic_search(keyword=slug.replace('-', ' '),
                          offset=((page - 1)*limit),
                          limit=limit)

    pagination = dict()
    pagination['total_row'] = data['hits']['total']
    pagination['current_page'] = page
    pagination['limit_per_page'] = limit
    pagination['total_page'] = int(math.ceil(pagination['total_row'] / (pagination['limit_per_page'] * 1.0)))

    pagination['paginators'] = create_pagination(pagination=pagination)
    if pagination['total_row'] > 0:
        for item in data['hits']['hits']:
            product = {}
            try:
                item['_source']['price']['private'] = TextFormatting.price_formatting(item['_source']['price']['private'])
                product['price'] = item['_source']['price']
            except:
                None

            try:
                image_url = '/static/assets/images/no-image-300x366.jpg'

                if len(item['_source']['image']) > 0 and os.path.isfile(os.path.join(settings.BASE_DIR, 'media/{0}'.format(item['_source']['image'][0]['url']))):
                    image_url = os.path.join(settings.MEDIA_URL, item['_source']['image'][0]['url'])

                image = dict()
                image['url'] = image_url
                product['image'] = dict()
                product['image'].update(image)
            except:
                sys.exc_info()[1]

            try:
                product['product_name'] = TextFormatting.text_ellipsis(text=item['_source']['product_name'].strip(), max_length=50, ellipsis_chars='...')
            except:
                pass
            try:
                product['slug'] = item['_source']['metadata']['slug']
                product['id'] = item['_source']['id']
                pm = ProductMetadata.objects.get(slug=item['_source']['metadata']['slug']).product
                pd = Product.objects.get(id=pm.id)
            except:
                print sys.exc_info()[1]

            products.append(product)

    context = {'products': products,
               'pagination': pagination,
               'current_category': '',
               'category_page': False,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  'Jual {0} | {1}'.format(slug, settings.META_TITLE),
               'meta_description': 'Belanja {0} di {1}'.format(slug, settings.META_DESCRIPTION), }

    template = loader.get_template('web-v2/list.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def product_detail(request, slug):
    context = None
    categories = None
    product = None
    image_url = '/static/assets/images/no-image-300x366.jpg'
    try:
        categories = Category.objects.filter(is_active=1).order_by('sort_order')
        product_metadata = ProductMetadata.objects.get(slug=slug)
        product = Product.objects.get(id=product_metadata.product_id)
        private_price = TextFormatting.price_formatting(product.get_product_detail().private_price)
    except:
        return HttpResponseRedirect('/404')

    try:
        image_url = product.get_product_image().image_media_url()
    except:
        pass

    context = {'categories': categories,
               'product': product,
               'private_price': private_price,
               'image_url': image_url,
               'detail_page': True,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format(product.product_name, settings.META_TITLE),
               'meta_description': 'Belanja {0} di {1}'.format(product.product_name, settings.META_DESCRIPTION), }

    template = loader.get_template('web-v2/detail.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def checkout(request):
    categories = Category.objects.filter(is_active=1).order_by('sort_order')

    context = {'categories': categories,
               'category_page': True,
               'checkout_page': True,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format('Periksa Keranjang Belanja', settings.META_TITLE),
               'meta_description': settings.META_DESCRIPTION, }

    template = loader.get_template('web-v2/cart.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


@transaction.atomic
def final_checkout_process(request, slug):
    cart_session = None
    temp_orders = list()

    transaction_checkpoint = transaction.savepoint()

    if 'cart_session' in request.COOKIES:
        cart_session = request.COOKIES.get('cart_session')

    cart_object = CartData.get_cart_object(cart_session=cart_session)

    if cart_object is not None:
        cart_detail_object = CartData.get_cart_detail_object(cart_object=cart_object)

        for cart_detail in cart_detail_object:
            try:
                product_detail_object = ProductDetail.objects.get(product=cart_detail.product)
                product = product_detail_object.product

                product_name = product.product_name
                product_number = Generator.sku_number(product_id=product.id)
                if product.productdetail.private_price is None:
                    product_price = 0
                else:
                    product_price = int(product.productdetail.private_price)
                product_quantity = cart_detail.product_qty
                subtotal = product_price * product_quantity
                owner = product.main_dealer
                brand = product.brand

                temp_order_detail = dict()
                temp_order_detail['product_id'] = product.id
                temp_order_detail['product_name'] = product_name
                temp_order_detail['product_number'] = product_number
                temp_order_detail['product_price'] = product_price
                temp_order_detail['product_quantity'] = product_quantity
                temp_order_detail['subtotal'] = subtotal
                temp_order_detail['owner'] = owner
                temp_order_detail['owner_id'] = owner.id
                temp_order_detail['brand'] = brand.name

                temp_orders.append(temp_order_detail)

            except ProductDetail.DoesNotExist:
                pass

        if len(temp_orders):
            sorted_temp_orders = sorted(temp_orders, key=itemgetter('owner_id'))

        owner_id = 0
        temp_order = None
        temp_order_detail = None
        order_number = "%s" % Generator.order_number()
        vendor_order_number_postfix = 0

        save_success = True
        for sorted_temp_order in sorted_temp_orders:
            customer_id = Authentication.get_customer_id(request)
            if owner_id != sorted_temp_order['owner_id']:
                vendor_order_number_postfix += 1
                temp_order = TempOrder()
                temp_order.order_number = order_number
                temp_order.vendor_order_number = temp_order.order_number + str(vendor_order_number_postfix).zfill(2)
                temp_order.owner = sorted_temp_order['owner']
                temp_order.brand = sorted_temp_order['brand']
                temp_order.is_processed = False
                print customer_id
                temp_order.created_user = Customer.objects.get(id=customer_id)
                temp_order.save()
                owner_id = sorted_temp_order['owner_id']
            try:
                temp_order_detail = TempOrderDetail()
                temp_order_detail.temporder = temp_order
                temp_order_detail.product = Product.objects.get(id=sorted_temp_order['product_id'])
                temp_order_detail.product_name = sorted_temp_order['product_name']
                temp_order_detail.brand = sorted_temp_order['brand']
                temp_order_detail.product_number = sorted_temp_order['product_number']
                temp_order_detail.product_price = sorted_temp_order['product_price']
                temp_order_detail.product_quantity = sorted_temp_order['product_quantity']
                temp_order_detail.subtotal = sorted_temp_order['subtotal']
                temp_order_detail.product_availability = False
                temp_order_detail.save()
            except:
                save_success = False

        if save_success:
            cart_detail_object.delete()
            cart_object.delete()
            transaction.savepoint_commit(transaction_checkpoint)
    else:
        return HttpResponseRedirect('/')

    categories = Category.objects.filter(is_active=1).order_by('sort_order')
    context = {'slug': slug,
               'categories': categories,
               'static_page': True,
               'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format('Proses Pembayaran', settings.META_TITLE),
               'meta_description': settings.META_DESCRIPTION, }

    template = loader.get_template('web-v2/home.html')
    html = template.render(context, request)

    # cart_session = create_guid()

    http_response = HttpResponse(html)
    # http_response.set_cookie('cart_session', cart_session)

    # Send sales confirmation email to merchant, user
    TempOrder.send_order_notification_email(order_number)

    return http_response


def confirmation(request):
    cart_session = None
    cart_total = 0

    if 'cart_session' in request.COOKIES:
        cart_session = request.COOKIES.get('cart_session')

    cart_object = CartData.get_cart_object(cart_session=cart_session)
    cart_detail_object = CartData.get_cart_detail_object(cart_object=cart_object)
    if cart_detail_object is not None:
        cart_detail_list = CartData.cart_detail_object_to_dictionary(cart_detail_object=cart_detail_object, )
        for cart_detail_index in range(len(cart_detail_list)):
            try:
                cart_total += cart_detail_list[cart_detail_index]['private_price']
            except TypeError:
                pass
            finally:
                cart_detail_list[cart_detail_index]['private_price'] = TextFormatting.price_formatting(cart_detail_list[cart_detail_index]['private_price'])
    else:
        return HttpResponseRedirect('/')

    customer = Customer.objects.get(id=Authentication.get_customer_id(request))
    try:
        customer_detail = CustomerDetail.objects.get(customer=customer)
    except CustomerDetail.DoesNotExist:
        customer_detail = None

    context = {'customer': customer,
               'customer_detail': customer_detail,
               'cart_detail_list': cart_detail_list,
               'cart_total': TextFormatting.price_formatting(cart_total),
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format('Konfirmasi Belanja', settings.META_TITLE),
               'meta_description': settings.META_DESCRIPTION, }

    template = loader.get_template('web-v2/final_checkout.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)


def create_pagination(pagination):
    result = dict()
    start_page = 0
    last_page = 0

    if pagination['current_page'] > 1:
        result['previous_page'] = pagination['current_page'] - 1
    else:
        result['previous_page'] = 0

    if pagination['current_page'] < pagination['total_page']:
        result['next_page'] = pagination['current_page'] + 1
    else:
        result['next_page'] = 0

    if pagination['total_page'] == 1:
        result['first_page'] = 0
        result['last_page'] = 0
    elif pagination['current_page'] == 1:
        result['first_page'] = 0
        result['last_page'] = pagination['total_page']
    elif pagination['current_page'] == pagination['total_page']:
        result['first_page'] = 1
        result['last_page'] = 0
    else:
        result['first_page'] = 1
        result['last_page'] = pagination['total_page']

    paginators = list()
    if pagination['total_page'] > 5:
        if pagination['current_page'] > 2 and pagination['current_page'] + 3 < pagination['total_page']:
            start_page = pagination['current_page'] - 2
            last_page = pagination['current_page'] + 3
        elif pagination['current_page'] > 2 and pagination['current_page'] + 3 > pagination['total_page']:
            start_page = pagination['total_page'] - 6
            last_page = pagination['total_page'] + 1
        else:
            start_page = 1
            last_page = start_page + 5
    else:
        start_page = 1
        last_page = pagination['total_page']

    for page in range(start_page, last_page):
        paginators.append(page)

    result['page_number'] = paginators

    return result


def direct_payment(request):
    url = '/'
    http_response = HttpResponseRedirect(url)

    cart_session = request.COOKIES.get('cart_session')

    cart = None
    cart_detail_sum = 0

    try:
        cart = Cart.objects.get(cart_session=cart_session)
        cart_detail_sum = (CartDetail.objects.filter(cart=cart).aggregate(total=Sum(F('private_price') * F('product_qty'), output_field=FloatField())))['total']
    except Cart.DoesNotExist:
        pass
    except CartDetail.DoesNotExist:
        pass

    try:
        if cart is not None and cart_detail_sum is not None:
            customer = Customer.objects.get(cart_session=cart)

            customer_data = veritrans_request.CustomerDetails(first_name=customer.pic_name,
                                                              last_name='',
                                                              email=customer.email,
                                                              phone=customer.phone)

            transaction_to_pay = veritrans_request.TransactionDetails(order_id=str('{:05d}'.format(cart.id)),
                                                                      gross_amount=cart_detail_sum)

            url = tests.test_charge_vt_production(customer=customer_data,
                                                  transaction=transaction_to_pay)

            new_cart = CartData.create_new_cart(request=request)
            customer.cart_session = new_cart
            customer.save()

            request.COOKIES['cart_session'] = new_cart.cart_session

            http_response = HttpResponseRedirect(url)
            http_response.set_cookie('cart_session', new_cart.cart_session)
    except Customer.DoesNotExist:
        pass

    return http_response


def sign_in(request):
    context = {'additional_url_path': settings.ADDITIONAL_URL_PATH,
               'title': 'Belanja Alat Kesehatan Online Mudah Dan Terpercaya | Emedis.id #1 Toko Alat Kesehatan Online Terbesar dan Terlengkap di Indonesia',
               'meta_title':  '{0} | {1}'.format('Masuk atau Bergabung dengan Emedis', settings.META_TITLE),
               'meta_description': settings.META_DESCRIPTION, }
    template = loader.get_template('web-v2/sign.html')
    http_response = template.render(context, request)

    return HttpResponse(http_response)
