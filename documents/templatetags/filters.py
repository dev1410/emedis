from django import template
import locale

locale.setlocale(locale.LC_MONETARY, '')
register = template.Library()


@register.filter(name='make_currency')
def make_currency(value):
    return "%s" % locale.currency(value, grouping=True)

