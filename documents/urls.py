from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^sales_confirmation/$', views.sales_confirmation, name='Sales Confirmation'),
    url(r'^sales_confirmation/(?P<order_number>\w+)/$', views.sales_confirmation, name='Sales Confirmation'),
    url(r'^sales_confirmation/(?P<order_number>\w+)/download$', views.download_sales_confirmation, name='Download Sales Confirmation'),
]