# -*- coding: utf-8 -*-

from __future__ import absolute_import

import os

from django.http import HttpResponse
from django.template import loader
from django.conf import settings
from django.shortcuts import redirect

from product.order.models import TempOrder, TempOrderDetail, Order, OrderDetail
from product.models import ProductDetail

# from wkhtmltopdf.subprocess import CalledProcessError
# from wkhtmltopdf.utils import (_options_to_args, make_absolute_paths, wkhtmltopdf)
# from wkhtmltopdf.views import PDFResponse, PDFTemplateView, PDFTemplateResponse

from common.utils import file_operation


def index(request):
    return HttpResponse("Hello World")


def sales_confirmation(request, order_number=None):
    order_id = order_number
    dp_amount_percentage = 40

    if order_id is not None:
        order_object = Order.objects.get(order_number=order_id)
        order_details = OrderDetail.objects.filter(order=order_object, product_availability=True)

        product_objects = []
        product_details = []

        for counter in range(len(order_details)):
            product_objects.append(order_details[counter].product)

        for counter in range(len(product_objects)):
            product_details.append(ProductDetail.objects.get(product=product_objects[counter]))

        context = {
            'emedis_data': {
                'name': 'PT Medis Raya',
                'address_1': 'Taman Palem Lestari, Ruko Galaxy Blok P/28',
                'address_2': 'West Jakarta ID 11730',
                'phone': '021 - 12345678',
                'fax': '021 - 12345678',
                'email': 'am@emedis.id',
            },
            'customer_data': {
                'order_no': order_object.order_number,
                'order_date': order_object.created_date,
                'email': 'abc@example.com',
                'phone': '0812345678',
                'billing_address': 'Jl. Sudirman No 1 Kav 1 - 21, 11 Fl Suite Room Jakarta 1000',
                'shipping_address': 'Jl. Sudirman No 1 Jakarta',
            },
            'order_details': order_details,
            'order_id': order_id,
            'product_details': product_details,
            'grand_total': sum([order_details[i].product_price for i in range(len(order_details))])
        }

        if order_object.is_credit:
            context['dp'] = (context['grand_total'] * dp_amount_percentage)/100
            context['total_after_dp'] = context['grand_total'] - context['dp']

        pdf_response = PDFTemplateResponse(
            request = request,
            context = context,
            template = "documents/sales_confirmation.html",
            cmd_options = {
                'encoding': 'utf8',
                'quiet': True,
            }
        )

        template = loader.get_template('documents/order_notification.html')
        http_response = template.render(context, request)

        file_save_response = file_operation.save(http_response, os.path.join(os.path.join(settings.BASE_DIR, settings.TMP_FOLDER), order_id + ".html"))

        if file_save_response is None:
            None    #add logic here, either send email to admin or put it on log
        else:
            wkhtmltopdf(pages=[file_save_response], output=file_save_response.replace(".html", ".pdf"))
            file_operation.delete(file_save_response)

        return HttpResponse(http_response)
    else:
        return HttpResponse("Please input the order id, or contact admin.")


def download_sales_confirmation(request, order_number=None):
    print request.user.is_authenticated()
    if request.user.is_authenticated():
        order_id = order_number

        if order_id is None:
            None    #adding no file logic, eg: redirect to other page
        else:
            path = os.path.join(settings.BASE_DIR, settings.TMP_FOLDER)

            with open(os.path.join(path, order_id + ".pdf"), 'rb') as pdf_file:
                response = HttpResponse(pdf_file.read(), content_type='application/pdf')
                response['Content-Disposition'] = 'filename=' + order_id + '.pdf'

                return response
    else:
        return redirect('/')







