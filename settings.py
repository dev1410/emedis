import os
from elasticsearch import Elasticsearch
from PIL import _imaging as core

try:
    import production as default_config
except ImportError:
    try:
        import staging as default_config
    except ImportError:
        import local as default_config


BASE_DIR = os.path.dirname(os.path.abspath(__file__))

SECRET_KEY = 't5meqc^$&yse%*eaxjcn=03+cropm8g58peo&v(e86=%f*$q8#'

DEBUG = default_config.DEBUG

ALLOWED_HOSTS = default_config.ALLOWED_HOSTS

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'product',
    'django.contrib.humanize',
    # 'product.library',
    'ckeditor',
    'ckeditor_uploader',
    'category',
    # 'unspsc',
    'unspsc.family',
    'smart_selects',
    'elasticsearch',
    'search',
    'djrill',
    'mailer',
    'product.order',
    'wkhtmltopdf',
    'documents',
    'cms',
    'email_template',
    'cart',
    'customers',
    'geographicals',
    'elastic',
    'payments',
    'common',
    'authentication_sessions',
    'subscribe',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'middleware.minify_with_caching.MinifyWithCaching',
    'middleware.custom_session_middleware.CustomSessionMiddleware',
    'django.middleware.common.CommonMiddleware',
)

ROOT_URLCONF = 'urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'html/templates'), os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'wsgi.application'

DATABASES = default_config.DATABASES

# django-wkhtmltopdf
# http://django-wkhtmltopdf.readthedocs.org/en/latest/index.html

WKHTMLTOPDF_CMD = default_config.WKHTMLTOPDF_CMD

DOMAIN_URL = default_config.DOMAIN_URL

#WKHTMLTOPDF_CMD = 'X:\\developers\\engine\\wkhtmltopdf\\bin'

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Jakarta'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATICFILES_FINDERS = [
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder"
]

# We better choose STATICFILES_DIRS to make easier when defining new static file,
# first step, use findstatic path/to/static.extension to define the file.

STATIC_ROOT = os.path.join(BASE_DIR, 'static_root')

STATICFILES_DIRS = [
    ("static", os.path.join(os.path.dirname(__file__), 'static/')),
    ("assets", os.path.join(os.path.dirname(__file__), 'static/assets')),
    ("assets-v2", os.path.join(os.path.dirname(__file__), 'static/assets-v2')),
    ("smart-selects", os.path.join(os.path.dirname(__file__), 'static/smart-selects')),
    ("foundation", os.path.join(os.path.dirname(__file__), 'static/foundation')),
]

STATIC_URL = '/static/'

# Media files (images, docs, etc)

MEDIA_ROOT = os.path.join(os.path.dirname(__file__), 'media/')
MEDIA_URL = '/media/'

STATIC_ASSETS_ROOT = os.path.join(os.path.dirname(__file__), 'static/assets/')
STATIC_ASSETS_URL = '/assets/'

GOOGLE_APPS_VERIFY_ROOT = os.path.join(os.path.dirname(__file__), 'static/file/')
GOOGLE_APPS_VERIFY_URL = '/'

# Config for django-ckeditor
# https://github.com/django-ckeditor/django-ckeditor

CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_BASEPATH = os.path.join(STATIC_URL, "ckeditor/")
CKEDITOR_UPLOAD_PATH = "images/ckeditor/"
CKEDITOR_IMAGE_BACKEND = "Pillow"
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [
            ['Undo', 'Redo',
             '-', 'Bold', 'Italic', 'Underline',
             '-', 'Link', 'Unlink', 'Anchor',
             '-', 'Format',
             '-', 'SpellChecker', 'Scayt',
             '-', 'Maximize',
             '-', 'Language',
            ],
        ],
        'height': '100%',
        'width': '100%',
        'toolbarCanCollapse': False,
    },
    'full': {
        'toolbar': [
            ['Undo', 'Redo',
             '-', 'Bold', 'Italic', 'Underline', 'NumberedList', 'BulletedList',
             '-', 'Outdent', 'Indent', 'Blockquote', 'CreateDiv',
             '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock',
             '-', 'TextColor', 'BGColor',
             '-', 'Maximize', 'ShowBlocks',  'Image' ,
             '-', 'Cut', 'Copy', 'Paste', 'PasteText',
            ],
            ['-', 'SpecialChar',
             '-', 'Source',
            ],
            [
                '-', 'Styles', 'Format', 'Font', 'FontSize'
            ],
            [
                '-', 'BidiLtr', 'BidiRtl'
            ]
        ],
        'width': '100%',
        'height': '300px',
        'toolbarCanCollapse': False,
    },
    'disable': {
        'toolbar': [],
        'width': '100%',
        'height': '600px',
        'toolbarCanCollapse': False,
    },
}

# django-smart-selects configuration
USE_DJANGO_JQUERY = False

#Const
ADMIN_SITE_HEADER = 'Emedis Site Administration'
ADMIN_SITE_TITLE = 'Emedis Site Admin'

# Elasticsearch configurations
ELASTIC_CONNECTION = Elasticsearch()


# Mandrill Configuration
MANDRILL_API_KEY = 'z3LzBVMKVDY79y7Whe1B_A'
EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
DEFAULT_FROM_EMAIL = 'rijal@emedis.id'
MANDRILL_API_URL = "https://mandrillapp.com/api/1.0"

# Django email
# https://docs.djangoproject.com/en/1.9/ref/settings/#default-from-email
DEFAULT_FROM_EMAIL = 'admin@emedis.id'

# django-debug-toolbar
#
INTERNAL_IPS = '127.0.0.1'

TMP_FOLDER = 'temp'

# Format template tags & Filters
#
FORMAT_MODULE_PATH = ['emedis.formats']

"""
'loaders': [
                ('django.template.loaders.cached.Loader',
                 (
                     'django.template.loaders.filesystem.Loader',
                     'django.template.loaders.app_directories.Loader',)),
            ],
"""

# ORDER PDF DOCUMENTS PATH
BASE_NEWORDER_PDF_PATH = os.path.join(os.path.join(BASE_DIR, MEDIA_ROOT), 'temp/neworder/')
BASE_SALES_CONFIRMATION_PDF_PATH = os.path.join(BASE_DIR, 'temp/sales_confirmation/')

CACHE_TIME = 3600 * 24 * 30

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'content-cache',
        'TIMEOUT': CACHE_TIME,
        'MAX_ENTRIES': 300,
    }
}

MINIFY_HTML = True

DEFAULT_NO_IMAGE_PRODUCT_URL = 'assets/images/no-image-300x366.jpg'

ADDITIONAL_URL_PATH = default_config.ADDITIONAL_URL_PATH

APPEND_SLASH = True


# Veritrans API URL
# http://docs.veritrans.co.id/en/api/introduction.html
VT_API_KEY = default_config.VERITRANS_API_KEY
VT_DEV_API_KEY = default_config.VERITRANS_DEVELOPMENT_API_KEY
VT_SANDBOX_API_URL = 'https://api.sandbox.veritrans.co.id/v2'
VT_LIVE_API_URL = 'https://api.veritrans.co.id/v2'

PAGE_WITH_PERMISSIONS = ['checkout', 'payment_page', 'confirmation', 'direct_payment', ]

VERITRANS_LIST_IP = ['127.0.0.1', ]

META_TITLE = 'Your Medical Megastore - Emedis'
META_DESCRIPTION = 'Mal online terbesar Indonesia, tempat berkumpulnya vendor / distributor / pabrikan Alat Kesehatan / Medis / Alkes terpercaya se Indonesia. Belanja Alkes online semakin aman dan menguntungkan di Emedis.id.'