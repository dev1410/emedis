from django.contrib import admin


class CartDetailAdmin(admin.ModelAdmin):
    fieldsets = (
        ('Cart Detail', {'fields': ['cart', 'product_qty', 'private_price', 'e_catalogue_price', 'product_remark', 'product_added_time', ]}),
    )
