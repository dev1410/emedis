from django.db import models
from cart.models import Cart
from product.currency.models import ProductCurrency
from product.models import Product


class CartDetail(models.Model):
    product = models.ForeignKey(Product,
                                related_name='cart_product',
                                help_text='Product ID')
    product_qty = models.IntegerField(help_text='Product Qty')
    private_price = models.DecimalField(help_text='Emedis Price / Primary',
                                        decimal_places=2,
                                        max_digits=18)
    e_catalogue_price = models.DecimalField(help_text='Ekatalog Price',
                                            decimal_places=2,
                                            max_digits=18)
    private_price_currency = models.ForeignKey(ProductCurrency,
                                               null=True,
                                               related_name='cart_detail_private_price_currency')
    e_catalogue_price_currency = models.ForeignKey(ProductCurrency, null=True)
    product_remark = models.CharField(max_length=160,
                                      help_text='Product Remark')
    product_added_time = models.DateField(help_text='Product Added To Cart Time',
                                          auto_now_add=True,)
    cart = models.ForeignKey(Cart,
                             on_delete=models.CASCADE,
                             help_text='Cart')

    def product_id(self):
        return unicode(self.product.id)

    @property
    def total_price(self):
        return self.private_price * self.product_qty
