from django.contrib import admin
from cart.models import Cart
from cart.cart_details.models import CartDetail
from cart.cart_details.admin import CartDetailAdmin


class CartAdmin(admin.ModelAdmin):
    list_display = ('cart_created_time',
                    'cart_status', )

    fieldsets = (
        ('Cart Data', {'fields': ['cart_created_time', 'cart_status',]}),
    )

    # inlines = [CartDetailAdmin]

admin.site.register(Cart, CartAdmin)
admin.site.register(CartDetail, CartDetailAdmin)
