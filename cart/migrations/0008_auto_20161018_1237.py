# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0023_merge'),
        ('cart', '0007_merge'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartdetail',
            name='product_price_primary',
        ),
        migrations.RemoveField(
            model_name='cartdetail',
            name='product_price_secondary',
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='e_catalogue_price',
            field=models.DecimalField(default=0, help_text=b'Ekatalog Price', max_digits=18, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='e_catalogue_price_currency',
            field=models.ForeignKey(to='product.ProductCurrency', null=True),
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='private_price',
            field=models.DecimalField(default=0, help_text=b'Emedis Price / Primary', max_digits=18, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='private_price_currency',
            field=models.ForeignKey(related_name='cart_detail_private_price_currency', to='product.ProductCurrency', null=True),
        ),
    ]
