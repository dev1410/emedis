# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0005_remove_cart_customer'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cartdetail',
            name='product_price_primary',
            field=models.DecimalField(help_text=b'Product Price Primary', max_digits=18, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='cartdetail',
            name='product_price_secondary',
            field=models.DecimalField(help_text=b'Product Price Secondary / Ekatalog', max_digits=18, decimal_places=2),
        ),
    ]
