# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0003_auto_20160318_0935'),
        ('cart', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='cartdetail',
            name='product_id',
        ),
        migrations.AddField(
            model_name='cartdetail',
            name='product',
            field=models.ForeignKey(related_name='cart_product', default='', to='product.Product', help_text=b'Product ID'),
            preserve_default=False,
        ),
    ]
