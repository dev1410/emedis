# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('cart_created_time', models.DateField(help_text=b'Cart Created Time')),
                ('cart_status', models.PositiveSmallIntegerField(help_text=b'Cart Status')),
                ('customer', models.ForeignKey(related_name='customer_cart', to='customers.Customer')),
            ],
            options={
                'verbose_name': 'Cart',
                'verbose_name_plural': 'Carts',
            },
        ),
        migrations.CreateModel(
            name='CartDetail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('product_id', models.IntegerField(help_text=b'Product ID')),
                ('product_qty', models.IntegerField(help_text=b'Product Qty')),
                ('product_price_primary', models.DecimalField(help_text=b'Product Price Primary', max_digits=12, decimal_places=2)),
                ('product_price_secondary', models.DecimalField(help_text=b'Product Price Secondary / Ekatalog', max_digits=12, decimal_places=2)),
                ('product_remark', models.CharField(help_text=b'Product Remark', max_length=160)),
                ('product_added_time', models.DateField(help_text=b'Product Added To Cart Time')),
                ('cart', models.ForeignKey(help_text=b'Cart', to='cart.Cart')),
            ],
        ),
    ]
