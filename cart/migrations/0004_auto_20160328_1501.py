# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0003_auto_20160328_1451'),
    ]

    operations = [
        migrations.AlterField(
            model_name='cart',
            name='cart_session',
            field=models.CharField(help_text=b'Cart Session', unique=True, max_length=100),
        ),
    ]
