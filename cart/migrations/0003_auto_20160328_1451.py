# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0002_auto_20160326_1727'),
    ]

    operations = [
        migrations.AddField(
            model_name='cart',
            name='cart_session',
            field=models.UUIDField(help_text=b'Cart Session', unique=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='cart',
            name='cart_created_time',
            field=models.DateField(help_text=b'Cart Created Time', auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='cartdetail',
            name='product_added_time',
            field=models.DateField(help_text=b'Product Added To Cart Time', auto_now_add=True),
        ),
    ]
