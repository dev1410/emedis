from django.db import models


class Cart(models.Model):
    class Meta:
        verbose_name_plural = 'Carts'
        verbose_name = 'Cart'

    cart_created_time = models.DateField(help_text='Cart Created Time',
                                         auto_now_add=True)
    cart_status = models.PositiveSmallIntegerField(help_text='Cart Status',)
    cart_session = models.CharField(help_text='Cart Session',
                                    max_length=100,
                                    blank=False,
                                    unique=True,)

    def __unicode__(self):
        return self.cart_session
