import json

from django.db import models, IntegrityError


class Subscribe(models.Model):
    email = models.EmailField(unique=True)
    created_date = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        response = dict()

        try:
            super(Subscribe, self).save(*args, **kwargs)
            response['status'] = 201
            response['message'] = 'Email address saved successfully!'

        except IntegrityError as e:
            response['status'] = 409
            response['message'] = '%s' % e.__cause__

        return json.dumps(response)

    def __unicode__(self):
        return self.email
