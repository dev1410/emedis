from django.http import HttpResponse
from .models import Subscribe
from django.views.decorators.http import require_POST


@require_POST
def subscribe_ajax(request):
    message = None
    if request.is_ajax():
        create_object = Subscribe(email=request.POST.get('email'))
        message = create_object.save()

    return HttpResponse(message, content_type='application/json')
