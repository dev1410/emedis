from django.db import models


class Subscriber(models.Model):
    email = models.EmailField(max_length=84)
    created_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.email
