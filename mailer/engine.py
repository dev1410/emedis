from django.core.mail import EmailMultiAlternatives
from email_template.views import purchase_request, quotation, buyer_invoice, request_order


class Send(object):

    def __init__(self):
        pass

    @classmethod
    def request_order(cls, data):
        html_content = request_order(data=data)

        msg = EmailMultiAlternatives(
            subject='Delivery Order #{}'.format(data['order'].order_number),
            from_email='admin@emedis.id',
            to=data['recipient_list']
        )
        msg.attach_alternative(html_content, 'text/html')
        msg.attach_file(data['attachment'])
        return msg.send()

    @classmethod
    def buyer_invoice_order(cls, data):
        html_content = buyer_invoice(data)

        msg = EmailMultiAlternatives(
            subject='Purchase Invoice #%s' % data['order'].order_number,
            from_email='admin@emedis.id',
            to=[data['customer'].email])

        msg.attach_alternative(html_content, 'text/html')
        msg.attach_file(data['attachment'])
        return msg.send()

    @classmethod
    def order_notification(cls, data):
        """
        New order notification for seller
        :param data:
        :return:
        """
        html_content = purchase_request(data, is_customer=False)

        msg = EmailMultiAlternatives(
            subject='{0} #{1}'.format(data['subject'], data['order'].order_number),
            from_email='admin@emedis.id',
            to=data['recipient_list'])
        msg.attach_alternative(html_content, 'text/html')
        return msg.send()

    @classmethod
    def buyer_order_notification(cls, data):
        """
        New order notification for buyer
        :param data:
        :return:
        """
        html_content = purchase_request(data, is_customer=True)

        msg = EmailMultiAlternatives(
            subject='{0} #{1}'.format(data['subject'], data['order'].order_number),
            from_email='admin@emedis.id',
            to=[data['customer'].email])
        msg.attach_alternative(html_content, 'text/html')
        return msg.send()

    @classmethod
    def quotation(cls, data, is_customer=True):

        html_content = quotation(data=data, is_customer=is_customer)

        msg = EmailMultiAlternatives(
            subject='{0} #{1}'.format(data['subject'], data['order'].order_number),
            from_email='admin@emedis.id',
            to=[data['customer'].email])

        if not is_customer:
            msg = EmailMultiAlternatives(
                subject='{0} #{1}'.format(data['subject'], data['order'].order_number),
                from_email='admin@emedis.id',
                to=data['recipient_list'])

        msg.attach_alternative(html_content, 'text/html')

        if is_customer:
            msg.attach_file(data['attachment'])

        return msg.send()
