from django.shortcuts import render
from django.core.mail import send_mail, EmailMessage
from django.http import HttpResponse


def index(request):
    return render(
        request, 'mailer/index.html',
        {"foo": 'Bar'},
        content_type='text/html')


def request_order(request):

    return render(request, 'mailer/request_order.html', content_type='text/html')


def send(request):
    msg = EmailMessage(
        subject='TESTING DJANGO MANDRILL',
        from_email='rijal@emedis.id',
        to=['dev1410shop@gmail.com', 'rijalt1410@gmail.com']
    )
    msg.template_name = 'default'
    msg.template_content = {
        'ACTIVATION_LINK': "<a href='*|LINK|*'> Activate my Account!</a>"
    }
    msg.global_merge_vars = {
        'LINK': "https://www.emedis.id/",
        "NAME": "Rijal Tanjung!"
    }
    msg.merge_vars = {
        "rijal@emedis.id": {"NAME": "Rijal Tanjung"},
    }

    return HttpResponse(msg.send(), content_type='application/javascript')


def sendemail(source=None):
    send_mail(
        "Test Django Mandrill Email",
        "Here is the message",
        "rijal@emedis.id",
        ['dev1410shop@gmail.com'])


def subscriber(request):
    from django.http import HttpResponseRedirect

    if request.method == 'POST':

        from .models import Subscriber

        Subscriber.objects.create(email=request.POST.get('email', ''))

        HttpResponseRedirect('https://www.emedis.id')
    else:
        HttpResponseRedirect('https://www.emedis.id')
